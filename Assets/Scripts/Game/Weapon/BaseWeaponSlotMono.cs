using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class BaseWeaponSlotMono : MonoBehaviour{
    public ElementData Data;
    public WeaponGoods CurrentWeapon;

    public virtual void Init(ElementData data){
        Data = data;
    }
    public virtual void ClearWeapon(){
        if (CurrentWeapon != null){
            CurrentWeapon.DisequitEff();
            CurrentWeapon.Disequip();
        }
        CurrentWeapon = null;
    }

    public virtual void SwitchWeapon(Goods newWeapon){
        ClearWeapon();
        CurrentWeapon = newWeapon as WeaponGoods;
        CurrentWeapon.Equip(transform);
        CurrentWeapon.EquitEff();
    }
}
