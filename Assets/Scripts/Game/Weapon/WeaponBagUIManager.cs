using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class WeaponBagUIManager : SingletonMono<WeaponBagUIManager>{
    public BagItem BagItemHead;     //头盔
    public BagItem BagItemEar;      //耳坠
    public BagItem BagItemNecklace; //项链
    public BagItem BagItemMask;     //面具
    public BagItem BagItemCloak;    //披风
    public BagItem BagItemHandLeft; //左手武器
    public BagItem BagItemHandRight;//右手武器
    public BagItem BagItemHand;     //手套
    public BagItem BagItemJecked;   //上衣
    public BagItem BagItemBelt;     //腰带
    public BagItem BagItemTrouser;  //裤子
    public BagItem BagItemShoe;     //鞋子

    private List<BagItem> weaponBagItems = new List<BagItem>();
    protected override void Awake(){
        base.Awake();
        //初始化武器格子
        BagItemHead.Init((int) WeaponKindType.Head,BagItemType.Weapon);             //头盔
        BagItemEar.Init((int) WeaponKindType.Ear,BagItemType.Weapon);               //耳坠        
        BagItemNecklace.Init((int) WeaponKindType.Necklace,BagItemType.Weapon);     //项链
        BagItemMask.Init((int) WeaponKindType.Mask,BagItemType.Weapon);             //面具
        BagItemCloak.Init((int) WeaponKindType.Cloak,BagItemType.Weapon);           //披风
        BagItemHandLeft.Init((int) WeaponKindType.HandLeft,BagItemType.Weapon);     //左手武器
        BagItemHandRight.Init((int) WeaponKindType.HandRight,BagItemType.Weapon);   //披风
        BagItemHand.Init((int) WeaponKindType.Hand,BagItemType.Weapon);             //手套
        BagItemJecked.Init((int) WeaponKindType.Jecked,BagItemType.Weapon);         //上衣
        BagItemBelt.Init((int) WeaponKindType.Belt,BagItemType.Weapon);             //腰带
        BagItemTrouser.Init((int) WeaponKindType.Trouser,BagItemType.Weapon);       //裤子
        BagItemShoe.Init((int) WeaponKindType.Shoe,BagItemType.Weapon);             //鞋子
        weaponBagItems.Add(BagItemHead);
        weaponBagItems.Add(BagItemEar);
        weaponBagItems.Add(BagItemNecklace);
        weaponBagItems.Add(BagItemMask);
        weaponBagItems.Add(BagItemCloak);
        weaponBagItems.Add(BagItemHandLeft);
        weaponBagItems.Add(BagItemHandRight);
        weaponBagItems.Add(BagItemHand);
        weaponBagItems.Add(BagItemJecked);
        weaponBagItems.Add(BagItemBelt);
        weaponBagItems.Add(BagItemTrouser);
        weaponBagItems.Add(BagItemShoe);
    }

    public void RefreshUI(){
        foreach (var item in BagManager.Ins.GetWeaponGoods()){
            if (item.Value.id >= 0 && item.Value.num > 0)
                weaponBagItems[(int)item.Key].BindData(GoodsManager.Ins.GetNewGoods(item.Value.id));
            else  weaponBagItems[(int)item.Key].Refresh(true);
        }
    }
}
