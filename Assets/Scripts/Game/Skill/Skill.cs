using System.Collections;
using System.Collections.Generic;
using Game;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// 技能范围类型
/// </summary>
public enum SkillRangeType
{
    /// <summary>
    /// 半圆
    /// </summary>
    Semicircle,
    /// <summary>
    /// 圆形
    /// </summary>
    Roundness,
    /// <summary>
    /// 正方形
    /// </summary>
    Square,
    /// <summary>
    /// 单体自身
    /// </summary>
    MonomerOwner,
    /// <summary>
    /// 单体别人
    /// </summary>
    MonomerOwnerOther,
    /// <summary>
    /// 全屏
    /// </summary>
    FullScreen,
}
/// <summary>
/// 消耗类型
/// </summary>
public enum SkillCastType
{
    /// <summary>
    /// 血量
    /// </summary>
    HP,
    /// <summary>
    /// 蓝量
    /// </summary>
    MP,
    /// <summary>
    /// 能量条
    /// </summary>
    Enegy,
    /// <summary>
    /// 愤怒值
    /// </summary>
    Rage,
    /// <summary>
    /// 无消耗
    /// </summary>
    Null,
}
/// <summary>
/// 技能目标类型
/// </summary>
public enum SkillTargetType
{
    /// <summary>
    /// 自身
    /// </summary>
    Self,
    /// <summary>
    /// 单体非自身
    /// </summary>
    Single,
    /// <summary>
    /// 队友
    /// </summary>
    Friends,
    /// <summary>
    /// 宠物
    /// </summary>
    Pets,
    /// <summary>
    /// 中立单位
    /// </summary>
    Neutralitys,
    /// <summary>
    /// 敌人
    /// </summary>
    Enery,
    /// <summary>
    /// 物品
    /// </summary>
    Items,
}
/// <summary>
/// 能量机制
/// </summary>
public enum ChargeMechanicType
{
    /// <summary>
    /// 攻击次数
    /// </summary>
    Attacks,
    /// <summary>
    /// 消耗蓝量
    /// </summary>
    CastMP,
    /// <summary>
    /// 无充能机制
    /// </summary>
    Null,
}
/// <summary>
/// 技能
/// </summary>
public enum SkillType
{
    BaJiBeng,
    ShengXiangBengTianZhuang
}

/// <summary>
/// 技能基类
/// </summary>
public class SkillData
{
    #region 技能基础信息

    public int ID;//技能ID
    public SkillType SkillType;//技能类型
    public int Level;//技能等级
    public string Name;//技能名称
    public string Description;//技能描述
    public int ImgID;//图片ID
    public string EffectPath;//特效路径
    public string EffectID;//效果ID

    #endregion

    #region 技能消耗

    public float Value;//影响的值
    public float Cost;//消耗的能量值
    public SkillCastType SkillCastType;//消耗能量类型
    public float MaxValue;//最大的值

    #endregion

    #region 技能时间相关

    public float RockTime;//前摇时间
    public float RemainRockTime;//剩余前摇时间
    public float Time;//技能的持续时间
    public float RemainTime;//剩余时间
    public float CastTimes;//施放次数
    public float CD;//技能CD
    public float RemainCD;//技能剩余CD

    #endregion

    #region 技能范围相关

    public SkillRangeType SkillRangeType;//范围类型
    public float Range;//范围大小,比如半圆多少度
    public float DischargeDistance;//施放距离
    public float DischargeRadius;//施放法术半径

    #endregion

    #region 技能目标

    public SkillTargetType SkillTargetType;//作用目标
    public int MaxAffectSeveralPeople;//最多影响几个人 

    #endregion

    #region 其他技能调教

    public int ComboNext;//下一个技能
    public bool TriggerConditions;//是否达到触发条件
    public int ComboEffects;//技能组合
    public ChargeMechanicType ChargeMechanicType;//充能机制

    #endregion

    public SkillData(SkillJsonData data)
    {
        ID = data.ID;
        SkillType = data.SkillType;
        Level = data.Level;
        Name = data.Name;
        Description = data.Description;
        ImgID = data.ImgID;
        EffectPath = data.EffectPath;
        EffectID = data.EffectID;
        Value = data.Value;
        Cost = data.Cost;
        SkillCastType = data.SkillCastType;
        MaxValue = data.MaxValue;
        RockTime = data.RockTime;
        Time = data.Time;
        RemainTime = data.RemainTime;
        CastTimes = data.CastTimes;
        CD = data.CD;
        RemainCD = data.RemainCD;
        SkillTargetType = data.SkillTargetType;
        MaxAffectSeveralPeople = data.MaxAffectSeveralPeople;
        DischargeDistance = data.DischargeDistance;
        DischargeRadius = data.DischargeRadius;
        ComboNext = data.ComboNext;
        ComboEffects = data.ComboEffects;
        ChargeMechanicType = data.ChargeMechanicType;
        EffectID = data.EffectID;
    }
}

public enum SkillState
{
    /// <summary>
    /// 技能初始化
    /// </summary>
    Init,
    /// <summary>
    /// 技能准备状态
    /// </summary>
    Ready,
    /// <summary>
    /// 技能读条中
    /// </summary>
    OnCast,
    /// <summary>
    /// 技能释放
    /// </summary>
    Release,
    /// <summary>
    /// 技能循环结束
    /// </summary>
    End,
}
public class Skill
{
    public int ID;//技能ID
    public int Level;//技能等级
    public SkillData Data;
    /// <summary>
    /// 效果buff  根据id来获取效果并添加
    /// </summary>
    public List<Goods> SkillGoodsBuff = new();

    #region 技能信息

    public int IgnoreMask = ~(1 << 8 | 1 << 9 | 1 << 15);//自身 敌人 忽略单位 
    public Vector3 Position;//释放位置
    public Vector3 DirPosition;//技能释放方向
    public bool IsDir;//是否只是改变方向
    public ElementData Element;//释放单位
    public ElementData EffectElement;//释放单位
    public List<ElementData> EffectElementList = new();//受影响的单位
    public bool CantPut;//是否可以释放技能
    public SkillState SkillState;

    #endregion

    #region 技能私有变量

    private const float WaitForTime = 0.05f;
    private WaitForSeconds WaitRockTime = new WaitForSeconds(WaitForTime);

    //技能准备释放时,区域选择检测时间
    private WaitForSeconds WaitDetection = new WaitForSeconds(WaitForTime);
    #endregion

    public Skill(Skill data) : this(data.Data) { }
    public Skill(SkillData data)
    {
        Data = data;
        ID = data.ID;
        Level = data.Level;
        var effect = System.Array.ConvertAll(data.EffectID.Split("|"), int.Parse);
        foreach (var i in effect)
        {
            //fix 修复数据共用bug
            SkillGoodsBuff.Add(GoodsManager.Ins.GetNewGoods((DrugType)i));
        }

        Debug.Log($"技能{data.Name} 初始化完成");
    }

    #region 技能释放流程
    // 准备释放 选择位置/或者敌人 释放技能 消耗能量 技能效果

    #region 技能准备

    /// <summary>
    /// 准备释放,选取自身或者敌人
    /// </summary>
    public IEnumerator ReadyPutSkill(ElementData self, ElementData element = null)
    {
        yield return null;
        Element = self;
    }
    /// <summary>
    /// 释放技能,选取范围
    /// </summary>
    public IEnumerator ReadyPutSkill()
    {
        switch (Data.SkillRangeType)
        {
            case SkillRangeType.Roundness:
            case SkillRangeType.Semicircle:
            case SkillRangeType.Square:
            case SkillRangeType.FullScreen:
                //todo 选取位置
                break;
            case SkillRangeType.MonomerOwner://单体自身
                                             //todo 鼠标选人
                break;
            case SkillRangeType.MonomerOwnerOther://单体别人
                                                  //todo 鼠标选人
                Position = Vector3.zero;
                break;
        }
        yield return null;
        //测试代码
        EventManager.Ins.AddMouseContinuouseDetection(OnEventMouseDetection);
        EventManager.Ins.AddMouseLeftDownClick(OnEventMouseLeftClick);
    }

    private bool BeGeneratingSkillFlag = false;
    private void OnEventMouseDetection(IEventMessage e, object[] o = null)
    {
        var hp = Utility.Ins.MouseMove(null, 100, IgnoreMask);
        if (!BeGeneratingSkillFlag)
        {
            if (SkillCross.Ins == null)
            {
                BeGeneratingSkillFlag = true;

                var obj = PoolManager.Ins.Release("Effect/Skill/SkillCross", null, 1);
                BeGeneratingSkillFlag = false;
                Debug.Log("技能预览生成");
                obj.GetComponent<SkillCross>().RefreshPos(hp.hit.point);
            }
            else SkillCross.Ins.RefreshPos(hp.hit.point);
        }
    }

    private void OnEventMouseLeftClick(IEventMessage e, object[] o = null)
    {
        Position = Utility.Ins.MouseMove(null, 100, IgnoreMask).hit.point;
        DirPosition = Vector3.zero;
        IsDir = false;
        EventManager.Ins.RemoveMouseLeftDownClick(OnEventMouseLeftClick);
        EventManager.Ins.RemoveMouseContinuouseDetection(OnEventMouseDetection);
        SkillCross.Ins?.Hide();
        SkillState = SkillState.Ready;
    }
    /// <summary>
    /// 技能预览
    /// </summary>
    public void PreviewSkill()
    {

    }

    #endregion

    /// <summary>
    /// 释放技能
    /// </summary>
    public IEnumerator Put(Vector3 pos, Vector3 dir = default, bool isDir = false)
    {
        Data.RemainRockTime = Data.RockTime;
        yield return Utility.Ins.StartCoroutine(ReduceRockTimeCoroutine());

        SkillState = SkillState.Release;
        //读条完毕
        if (CantPut)
        {//如果被打断,直接退出技能释放
         //直接结束
            SkillState = SkillState.End;
        }
        else
        {
            var obj = PoolManager.Ins.Release(Data.EffectPath);

            obj.AddComponent<SkillMiddleware>().Init(this, Element, ID);
            if (isDir)
            {
                obj.transform.position = pos;
                obj.transform.rotation = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.z));
                // obj.transform.LookAt(new Vector3(0,dir.y,0));
            }
            else
            {
                obj.transform.position = pos;
            }

            Data.RemainCD = Data.CD;//技能冷却
                                    //todo 耗能
                                    // 测试 人类释放技能耗蓝10滴
            Element.MP_pm(-10);
            //todo 上buff
            SkillEffectSingle();

            SkillState = SkillState.End;
        }

    }

    public IEnumerator End()
    {
        yield return null;
    }

    #endregion

    #region 技能效果

    /// <summary>
    /// 检测范围的受影响的元素
    /// </summary>
    public void DetectionRangeEffect(List<ElementData> datas)
    {
        EffectElementList.Clear();
        EffectElementList.AddRange(datas);
    }

    public void AddElementEffect(ElementData data)
    {
        if (data == null) return;
        foreach (var effect in SkillGoodsBuff)
        {
            data.UseGoods(effect);
        }

        EffectElementList.Add(data);
    }

    public void RemoveElemetEffect(ElementData data)
    {
        if (data == null) return;
        foreach (var effect in SkillGoodsBuff)
        {
            data.RemoveGoods(effect);
        }
        EffectElementList.Remove(data);
    }
    /// <summary>
    /// 上buff
    /// </summary>
    public void SkillEffect()
    {
        switch (Data.SkillTargetType)
        {
            case SkillTargetType.Self:
                foreach (var effect in SkillGoodsBuff)
                {
                    Element.UseGoods(effect);
                }
                break;
            case SkillTargetType.Enery:
            case SkillTargetType.Friends:
            case SkillTargetType.Pets:
                foreach (var effect in SkillGoodsBuff)
                {
                    foreach (var data in EffectElementList)
                    {
                        data.UseGoods(effect);
                    }
                }
                break;
        }

    }

    public void SkillEffectSingle()
    {
        if (Data.SkillTargetType == SkillTargetType.Self)
            foreach (var effect in SkillGoodsBuff)
                Element?.UseGoods(effect);
        else if (Data.SkillTargetType == SkillTargetType.Single)
            foreach (var effect in SkillGoodsBuff)
                EffectElement?.UseGoods(effect);
    }

    #endregion


    /// <summary>
    /// 打断技能
    /// </summary>
    public void BreakSkill()
    {
        CantPut = true;
    }


    IEnumerator ReduceRockTimeCoroutine()
    {
        //读条中
        SkillState = SkillState.OnCast;

        while (Data.RemainRockTime >= 0)
        {
            Data.RemainRockTime -= 0.2f;
            yield return WaitRockTime;
        }

        Debug.Log("技能" + Data.Name + "读条结束");
    }
}
