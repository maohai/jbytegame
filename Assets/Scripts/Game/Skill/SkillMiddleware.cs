using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class SkillMiddleware : MonoBehaviour{
    public ElementData Data;
    public Skill Skill;
    public int SkillID;
    public void Init(Skill skill,ElementData data,int id){
        Skill = skill;
        Data = data;
        SkillID = id;
    }
    
    public void OnTriggerEnter(Collider other){
        if (other.TryGetComponent(out BaseElementMono element)){
            Skill.AddElementEffect(element.Data);
        }
    }

    public void OnTriggerExit(Collider other){
        if (other.TryGetComponent(out BaseElementMono element)){
            Skill.RemoveElemetEffect(element.Data);
        }
    }
}
