using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public class SkillCross : SingletonMono<SkillCross> {
        public void RefreshPos(Vector3 pos) {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            transform.position = pos;
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }

}