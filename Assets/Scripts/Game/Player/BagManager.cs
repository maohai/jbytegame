using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.Serialization;

public class BagManager : SingletonAutoMono<BagManager> {
    public override void Init() {
        base.Init();
        InitBagGoods();
        InitWeaponGoods();
        InitQuickBagGoods();
        FastPickUp = true;
    }

    #region 总背包物体
    /// <summary>
    /// 默认开启快捷拾取
    /// </summary>
    public bool FastPickUp = true;

    public const int DefaultIndex = -1;
    public const int DefaultID = -1;
    public const int DefaultNum = -1;
    /// <summary>
    /// 是否已经加入快捷背包
    /// </summary>
    /// 等bagGoods背包刷新之后更新为false
    public BagItemType RefreshBagType = BagItemType.All;
    /// <summary>
    /// 所拥有的物品及其数量   类型,物品ID,物品数量
    /// </summary>
    private Dictionary<GoodsType,Dictionary<int, int>> dicGoods = new();
    // Consumables,//消耗品
    // Weapon,//装备
    // Biology,//生物相关
    // Materials,//材料
    // Special,//特殊
    // 物品ID 物品数量
    private Dictionary<int, int> dicConsumablesGoods = new();
    private Dictionary<int, int> dicBiologyGoods = new();
    /// <summary>
    /// 背包添加物品 总共的物品
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    public void AddGoods(int id, int num = 1) {
        AddGoods(GoodsManager.Ins.GetGoodsData(id), num);
    }
    public void AddGoods(Goods goods, int num = 1) {
        int id = goods.ID;
        if (dicGoods.ContainsKey(goods.GoodsType)) {
            if (dicGoods[goods.GoodsType].ContainsKey(id)) {
                dicGoods[goods.GoodsType][id] += num;
            } else {
                dicGoods[goods.GoodsType].Add(id, num);
            }
        } else {
            Dictionary<int, int> temp = new();
            temp.Add(id, num);
            dicGoods.Add(goods.GoodsType, temp);
        }
        AddGoodsGroupType(goods, num);

        //先检测背包里是否有这个id 且没满
        bool hasID = false;
        foreach (var item in bagGoods) {
            if (item.Value.id == goods.ID) {
                hasID = true;
                break;
            }
        }
        //如果有 跳过快捷键检查 看是否能添加到背包
        if (!hasID) {
            foreach (var item in QuickBagGoods) {
                if (item.Value.id == -1 || item.Value.id == goods.ID && item.Value.num < goods.BagLimitNum) {
                    RefreshBagType = BagItemType.QuickBag;
                    AddQuickBagGoods(goods, num);
                    num = 1;
                    break;
                }
            }
        }
        //如果没有添加进入,则添加到背包
        if (RefreshBagType == BagItemType.All) {
            RefreshBagType = BagItemType.Bag;
            AddBagGoods(goods, num);
        }

        RefreshAllBag(goods);
    }
    /// <summary>
    /// 分类
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    public void AddGoodsGroupType(Goods goods, int num) {
        int id = goods.ID;
        switch (goods.GoodsType) {
            case GoodsType.Consumables: {
                    if (dicConsumablesGoods.ContainsKey(id)) {
                        dicConsumablesGoods[id] += num;
                    } else {
                        dicConsumablesGoods.TryAdd(id, num);
                    }
                    break;
                }
            case GoodsType.Weapon: break;
            case GoodsType.Biology: {
                    if (dicBiologyGoods.ContainsKey(id)) {
                        dicBiologyGoods[id] += num;
                    } else {
                        dicBiologyGoods.TryAdd(id, num);
                    }
                    break;
                }
            case GoodsType.Materials: break;
            case GoodsType.Special: break;
        }
    }
    /// <summary>
    /// 移除总背包的物品
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    public void ReduceGoods(Goods goods, int num = 1) {
        int id = goods.ID;
        if (ContainsGoods(goods)) {
            var current = dicGoods[goods.GoodsType][id];
            Utility.NumClam(ref current, num, 0, false);
            dicGoods[goods.GoodsType][id] = current;
            RemoveGoodsGroupType(goods, current);
        }
    }
    public void ReduceGoods(int id, int num = 1) {
        ReduceGoods(GoodsManager.Ins.GetGoodsData(id), num);
    }
    /// <summary>
    /// 移除对应的类型
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    private void RemoveGoodsGroupType(Goods goods, int num) {
        int id = goods.ID;
        switch (goods.GoodsType) {
            case GoodsType.Consumables: {
                    if (dicConsumablesGoods.ContainsKey(id)) {
                        dicConsumablesGoods[id] = num;
                    }
                    break;
                }
            case GoodsType.Weapon: break;
            case GoodsType.Biology: {
                    if (dicBiologyGoods.ContainsKey(id)) {
                        dicBiologyGoods[id] = num;
                    }
                    break;
                }
            case GoodsType.Materials: break;
            case GoodsType.Special: break;
        }
    }
    /// <summary>
    /// 是否包含此物品
    /// </summary>
    /// <param name="good"></param>
    /// <returns></returns>
    private bool ContainsGoods(Goods good) => good != null && dicGoods.ContainsKey(good.GoodsType) && dicGoods[good.GoodsType].ContainsKey(good.ID);
    private bool AddDicData(Goods goods, int num, int size, Dictionary<int, (int id, int num)> dic) {
        if (goods == null || dic == null) return false;
        int id = goods.ID;
        bool addSuccess = false;
        int firstEmptyIndex = DefaultIndex;
        for (int i = 0; i < size; i++) {
            int bagIndexID = dic[i].id;
            int bagNum = dic[i].num;
            if (bagIndexID == goods.ID && bagNum < goods.BagLimitNum) {
                num += dic[i].num;
                int remainNum = num - goods.BagLimitNum;
                if (remainNum > 0) {
                    dic[i] = (id, goods.BagLimitNum);
                    AddGoods(goods, remainNum);
                } else {
                    dic[i] = (id, num);
                }
                addSuccess = true;
                break;
            } else if (bagIndexID == DefaultID && firstEmptyIndex == -1) {
                //找到第一个空格子
                firstEmptyIndex = i;
            }
        }
        if (!addSuccess) {
            int remainNum = num - goods.BagLimitNum;
            if (remainNum > 0) {
                dic[firstEmptyIndex] = (id, goods.BagLimitNum);
                AddGoods(goods, remainNum);
            } else {
                dic[firstEmptyIndex] = (id, num);
            }
        }

        return true;
    }
    /// <summary>
    /// 移除使用的物品  或者移除当做材料的物品
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    public void RemoveUsedGoods(int id, int num) {
        RemoveUsedGoods(GoodsManager.Ins.GetGoodsData(id), num);
    }
    public void RemoveUsedGoods(Goods goods, int num) {
        List<(int index, int num)> goodsIndexList = new();
        int remainNum = num;
        RefreshBagType = BagItemType.QuickBag;
        //查询快捷背包
        foreach (var item in QuickBagGoods)
            if (goods.ID == item.Value.id) {
                int index = item.Key;
                int temp = remainNum;
                temp -= item.Value.num;
                if (temp > 0)
                    goodsIndexList.Add((index, item.Value.num));
                else goodsIndexList.Add((index, remainNum));
                remainNum = temp;
                if (remainNum <= 0) break;
            }
        foreach (var item in goodsIndexList) RemoveQuickBagGoods(goods, item.index, item.num);
        //刷新背包
        RefreshAllBag(goods);
        if (remainNum <= 0) return;
        RefreshBagType = BagItemType.Bag;
        goodsIndexList.Clear();
        //查询背包
        foreach (var item in bagGoods)
            if (goods.ID == item.Value.id) {
                int index = item.Key.index;
                int temp = remainNum;
                temp -= item.Value.num;
                if (temp > 0)
                    goodsIndexList.Add((index, item.Value.num));
                else goodsIndexList.Add((index, remainNum));
                if (temp < 0) break;
                remainNum = temp;
            }
        foreach (var item in goodsIndexList) RemoveBagGoods(goods, item.index, item.num);
        RefreshAllBag(goods);
    }
    public void RemoveUsedGoods(BagItem bagItem, int n = 1) {
        RefreshBagType = bagItem.BagItemType;
        switch (bagItem.BagItemType) {
            case BagItemType.Bag:
                RemoveBagGoods(bagItem, n);
                break;
            case BagItemType.Box:
                break;
            case BagItemType.QuickBag:
                RemoveQuickBagGoods(bagItem, n);
                break;
        }
        RefreshAllBag(bagItem.Data);
    }
    /// <summary>
    /// 刷新所有背包
    /// </summary>
    /// <param name="goods"></param>
    public void RefreshAllBag(Goods goods) {
        RefreshBagMessage.Ins.SendMessage(goods);
        RefreshBagType = BagItemType.All;
    }
    /// <summary>
    /// 根据背包类型类型获取背包物品数量
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public int GetNumByGoods(Goods goods, BagItemType type = BagItemType.QuickBag) {
        switch (type) {
            case BagItemType.QuickBag: return GetQuickAllNumByGoods(goods);
            case BagItemType.Bag: return GetBagAllNumByGoods(goods);
        }
        return 0;
    }
    /// <summary>
    /// 获取背包和快捷键的所有物品
    /// </summary>
    /// <param name="goods"></param>
    /// <returns></returns>
    public int GetAllNumByGoods(Goods goods) {
        var num = 0;
        num += GetNumByGoods(goods, BagItemType.QuickBag);
        num += GetNumByGoods(goods, BagItemType.Bag);
        return num;
    }
    public int GetAllNumByGoods(int id) {
        return GetAllNumByGoods(GoodsManager.Ins.GetGoodsData(id));
    }
    #endregion

    #region 背包管理

    public GoodsType CurrentClickBagIndex = GoodsType.Consumables;//当前页面的Index
    /// <summary>
    /// 当前背包容量
    /// </summary>
    public const int BagItemTypeSize = 5;
    public const int BagItemSize = 64;
    /// <summary>
    /// 当前背包类型,背包格子下标,id,数量
    /// id为-1的时候当前格子为空
    /// </summary>
    private Dictionary<(GoodsType itemType,int index), (int id, int num)> bagGoods = new();
    /// <summary>
    /// 初始化背包物品
    /// </summary>
    private void InitBagGoods() {
        for (int i = 0; i < BagItemTypeSize; i++) {
            for (int j = 0; j < BagItemSize; j++) {
                if (bagGoods.ContainsKey(((GoodsType)i, j)))
                    bagGoods[((GoodsType)i, j)] = (DefaultID, DefaultNum);
                else bagGoods.Add(((GoodsType)i, j), (DefaultID, DefaultNum));
            }
        }
    }
    /// <summary>
    /// 重置背包物品
    /// </summary>
    /// <param name="type"></param>
    /// <param name="index"></param>
    public void ResetBagGoodsByIndex(GoodsType type, int index) {
        bagGoods[(type, index)] = (DefaultID, DefaultNum);
    }
    /// <summary>
    /// 添加背包物品
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public bool AddBagGoods(Goods goods, int num = 1) {
        int id = goods.ID;
        var type = goods.GoodsType;
        //todo 满背包判断

        //加入总物品中
        bool addSuccess = false;
        int firstEmptyIndex = DefaultIndex;
        for (int i = 0; i < BagItemSize; i++) {
            int bagIndexID = bagGoods[(type, i)].id;
            int bagNum = bagGoods[(type, i)].num;
            if (bagIndexID == goods.ID && bagNum < goods.BagLimitNum) {
                num += bagNum;
                int remainNum = num - goods.BagLimitNum;
                if (remainNum > 0) {
                    bagGoods[(type, i)] = (id, goods.BagLimitNum);
                    AddGoods(goods, remainNum);
                } else {
                    bagGoods[(type, i)] = (id, num);
                }
                addSuccess = true;
                break;
            } else if (bagIndexID == DefaultID && firstEmptyIndex == -1) {
                //找到第一个空格子
                firstEmptyIndex = i;
            }
        }

        //放入空格子
        if (!addSuccess) {
            int remainNum = num - goods.BagLimitNum;
            if (remainNum > 0) {
                bagGoods[(type, firstEmptyIndex)] = (id, goods.BagLimitNum);
                AddGoods(goods, remainNum);
            } else {
                bagGoods[(type, firstEmptyIndex)] = (id, num);
            }

        }

        return true;
    }
    /// <summary>
    /// 移除背包物品
    /// </summary>
    /// <param name="bagItem"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    private bool RemoveBagGoods(BagItem bagItem, int n = 1) => RemoveBagGoods(bagItem.Data, bagItem.BagItemIndex, n);
    private bool RemoveBagGoods(Goods goods, int index, int n) {
        ReduceGoods(goods, n);

        //查询第一个此类id下标
        var type = goods.GoodsType;
        int id = bagGoods[(type, index)].id;
        int num = bagGoods[(type, index)].num;
        if (num < n) Debug.LogError("超了,检查一下");
        Utility.NumClam(ref num, n, 0, false);
        bagGoods[(type, index)] = (id, num);
        return true;
    }
    /// <summary>
    /// 获取空格子位置
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private int GetEmptyBagIndex(GoodsType type) {
        foreach (var item in bagGoods) {
            if (item.Key.itemType == type && item.Value.id == DefaultID)
                return item.Key.index;
        }

        return DefaultID;
    }
    /// <summary>
    /// 根据类型获取背包格子数据
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public Dictionary<int, (int, int)> GetGoodsListByType(GoodsType type) {
        Dictionary<int, (int, int)> temp = new();
        foreach (var item in bagGoods) {
            if (item.Key.itemType == type) {
                temp.Add(item.Key.index, (item.Value.id, item.Value.num));
            }
        }
        return temp;
    }
    public int GetBagNumByGoods(GoodsType type, int index) => bagGoods[(type, index)].num;
    private int GetBagAllNumByGoods(Goods goods) {
        int num = 0;
        foreach (var item in bagGoods) {
            if (item.Value.id == goods.ID)
                num += item.Value.num;
        }
        return num;
    }
    public int GetBagAllNumByGoods(int id) {
        return GetBagAllNumByGoods(GoodsManager.Ins.GetGoodsData(id));
    }
    public void SortBagGoods() {
        RefreshBagType = BagItemType.Bag;
        Dictionary<(GoodsType,int), (int id, int num)> temp = new();
        var goods = new Dictionary<GoodsType, int>();//下标数据
        foreach (var item in bagGoods) {
            if (item.Value != (DefaultID, DefaultNum)) {
                var t = item.Key.itemType;
                if (goods.ContainsKey(t)) {
                    ++goods[t];
                } else goods.Add(t, 0);
                temp.Add((t, goods[t]), (item.Value));
            }
        }
        InitBagGoods();
        foreach (var item in temp) {
            bagGoods[item.Key] = item.Value;
        }
        RefreshAllBag(null);
    }

    #endregion

    #region 快捷键背包

    public const int QuickBagSize = 8;
    /// <summary>
    /// 背包数据 下标 ID 数量
    /// </summary>
    private Dictionary<int, (int id, int num)> QuickBagGoods = new();
    /// <summary>
    /// 初始化快捷背包
    /// </summary>
    private void InitQuickBagGoods() {
        for (int i = 0; i < QuickBagSize; i++) {
            QuickBagGoods.Add(i, (DefaultID, DefaultNum));
        }
    }

    /// <summary>
    /// 重置快捷背包数据
    /// </summary>
    /// <param name="index"></param>
    public void ResetQuickBagGoodsByIndex(int index) {
        QuickBagGoods[index] = (DefaultID, DefaultNum);
    }
    /// <summary>
    /// 添加快捷背包数据
    /// </summary>
    /// <param name="goods"></param>
    /// <param name="num"></param>
    /// <returns></returns>
    public bool AddQuickBagGoods(Goods goods, int num = 1) {
        //todo 无法加入判断
        return AddDicData(goods, num, QuickBagSize, QuickBagGoods);
    }
    /// <summary>
    /// 移除快捷背包数据
    /// </summary>
    /// <param name="bagItem"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    public bool RemoveQuickBagGoods(BagItem bagItem, int n = 1) =>
        RemoveQuickBagGoods(bagItem.Data, bagItem.BagItemIndex, n);
    public bool RemoveQuickBagGoods(Goods goods, int index, int n = 1) {

        ReduceGoods(goods, n);
        int id = QuickBagGoods[index].id;
        int num = QuickBagGoods[index].num;
        if (num < n) Debug.LogError("超了,检查一下");
        Utility.NumClam(ref num, n, 0, false);

        QuickBagGoods[index] = (id, num);

        return true;
    }
    /// <summary>
    /// 获取空格子位置
    /// </summary>
    /// <returns></returns>
    public int GetEmptyQuickIndex() {
        foreach (var item in QuickBagGoods) {
            if (item.Value.id == DefaultID)
                return item.Key;
        }
        return DefaultID;
    }
    public Dictionary<int, (int, int)> GetQuickBagData() => QuickBagGoods;
    public int GetQuickNumByIndex(int index) => QuickBagGoods[index].num;
    private int GetQuickAllNumByGoods(Goods goods) {
        int num = 0;
        foreach (var item in QuickBagGoods) {
            if (item.Value.id == goods.ID)
                num += item.Value.num;
        }
        return num;
    }
    #endregion

    #region 装备
    public const int WeaponBagSize = 12;
    private Dictionary<WeaponKindType, (int id, int num)> bagWeaponGoods = new();

    private void InitWeaponGoods() {
        for (int i = 0; i < WeaponBagSize; i++) {
            bagWeaponGoods.Add((WeaponKindType)i, (DefaultID, DefaultNum));
        }
    }
    /// <summary>
    /// 重置快捷背包数据
    /// </summary>
    /// <param name="index"></param>
    public void ResetWeaponBagGoodsByKindType(WeaponKindType type) {
        bagWeaponGoods[type] = (DefaultID, DefaultNum);
    }
    public Dictionary<WeaponKindType, (int id, int num)> GetWeaponGoods() => bagWeaponGoods;

    /// <summary>
    /// 检测武器是否装备上了
    /// </summary>
    public void CheckWeaponEquit(BagItemType type, Goods goods) {
        if (type == BagItemType.Weapon && goods != null) {
            goods.Used();
            PlayerController.Ins.rightWeaponSlotSlot.SwitchWeapon(goods);
        }
    }
    public void CheckWeaponDisequit(BagItemType type, Goods goods) {
        if (type == BagItemType.Weapon && goods != null) {
            PlayerController.Ins.rightWeaponSlotSlot.ClearWeapon();
            goods.Nonuse();
        }
    }
    #endregion

    #region 背包拖拽

    /// <summary>
    /// 当前拖拽的格子
    /// </summary>
    public BagItem CurrentDrapBagItem;
    public int CurrentDrapIndex = DefaultID;
    public void SwapCell(BagItemType type, int index) {
        //拖到背包上 但是物品类型当前页面类型 直接退出
        //快捷键 如果当前背包没打开 直接放入
        if (type is BagItemType.Bag
            && !(BagDialog.Ins && CurrentClickBagIndex == CurrentDrapBagItem.Data.GoodsType || !BagDialog.Ins)
        ) {
            CurrentDrapBagItem = null;
            CurrentDrapIndex = DefaultIndex;
            return;
        }
        //拖到装备格子上 但是当前物品类型不是装备
        if (type is BagItemType.Weapon
           && CurrentDrapBagItem.Data is not WeaponGoods
            ) {
            CurrentDrapBagItem = null;
            CurrentDrapIndex = DefaultIndex;
            return;
        }
        //交换格子
        (int id, int num) temp = (DefaultID,DefaultNum);
        switch (CurrentDrapBagItem.BagItemType) {
            case BagItemType.Bag:
                temp = bagGoods[(CurrentDrapBagItem.Data.GoodsType, CurrentDrapIndex)];
                break;
            case BagItemType.Weapon:
                CheckWeaponDisequit(BagItemType.Weapon, CurrentDrapBagItem.Data);
                temp = bagWeaponGoods[(CurrentDrapBagItem.Data as WeaponGoods).WeaponKindType];
                break;
            case BagItemType.Box:
                break;
            case BagItemType.QuickBag:
                temp = QuickBagGoods[CurrentDrapIndex];
                break;
        }
        (int id, int num) tempSwap = (DefaultID,DefaultNum);
        //被交换格子
        switch (type) {
            case BagItemType.Bag:
                tempSwap = bagGoods[(CurrentDrapBagItem.Data.GoodsType, index)];
                bagGoods[(CurrentDrapBagItem.Data.GoodsType, index)] = temp;
                break;
            case BagItemType.Weapon:
                CheckWeaponEquit(BagItemType.Weapon, CurrentDrapBagItem.Data);
                tempSwap = bagWeaponGoods[(CurrentDrapBagItem.Data as WeaponGoods).WeaponKindType];
                bagWeaponGoods[(CurrentDrapBagItem.Data as WeaponGoods).WeaponKindType] = temp;
                break;
            case BagItemType.Box:
                break;
            case BagItemType.QuickBag:
                tempSwap = QuickBagGoods[index];
                QuickBagGoods[index] = temp;
                break;
        }
        switch (CurrentDrapBagItem.BagItemType) {
            case BagItemType.Bag:
                bagGoods[(CurrentDrapBagItem.Data.GoodsType, CurrentDrapIndex)] = tempSwap;
                break;
            case BagItemType.Weapon:
                bagWeaponGoods[(CurrentDrapBagItem.Data as WeaponGoods).WeaponKindType] = tempSwap;
                break;
            case BagItemType.Box:
                break;
            case BagItemType.QuickBag:
                QuickBagGoods[CurrentDrapIndex] = tempSwap;
                break;
        }

        RefreshAllBag(null);
        CurrentDrapBagItem = null;
        CurrentDrapIndex = DefaultIndex;
    }
    public void SwapCell(BagItem swapBagItem) {
        SwapCell(swapBagItem.BagItemType, swapBagItem.BagItemIndex);
    }
    /// <summary>
    /// 如果是装备的话 在快捷背包 如果装备栏为空 直接替换否则按照以下转换规则
    /// 在背包里 则直接跟装备栏替换
    /// -------------------------
    /// 背包和快捷背包互换
    /// 如果有箱子的话先放箱子
    /// 如果从箱子拿的话先放背包 满就放快捷背包
    /// </summary>
    public void SwapCell() {
        switch (CurrentDrapBagItem.BagItemType) {
            case BagItemType.QuickBag: {
                    //todo 如果有箱子打开

                    //如果是装备且装备栏为空直接装备上 否则直接放到背包
                    if (CurrentDrapBagItem.Data is WeaponGoods weaponGoods && bagWeaponGoods[weaponGoods.WeaponKindType].id == DefaultID) {
                        SwapCell(BagItemType.Weapon, (int)weaponGoods.WeaponKindType);
                        break;
                    }
                    //不管背包有没有打开 直接放进去
                    var index = GetEmptyBagIndex(CurrentDrapBagItem.Data.GoodsType);
                    if (index != DefaultID) {
                        SwapCell(BagItemType.Bag, index);
                    }
                    break;
                }
            case BagItemType.Weapon: {
                    //直接放入背包
                    var index = GetEmptyBagIndex(CurrentDrapBagItem.Data.GoodsType);
                    if (index != DefaultID) {
                        SwapCell(BagItemType.Bag, index);
                    }
                    break;
                }
            case BagItemType.Bag: {
                    //如果是装备直接替换
                    if (CurrentDrapBagItem.Data is WeaponGoods weaponGoods) {
                        SwapCell(BagItemType.Weapon, (int)weaponGoods.WeaponKindType);
                        break;
                    }
                    //todo 如果有箱子打开
                    var index = GetEmptyQuickIndex();
                    if (index != DefaultID) {
                        SwapCell(BagItemType.QuickBag, index);
                    }
                    break;
                }
        }
    }
    #endregion
}
