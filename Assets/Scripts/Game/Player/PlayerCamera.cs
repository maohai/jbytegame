using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public class PlayerCamera : SingletonMono<PlayerCamera> {
        public void UpdateRotate(ref Vector3 dir) {
            transform.rotation = Quaternion.Euler(dir);
        }
        public void UpdatePos(Vector3 pos) {
            transform.position = pos;
        }
    }

}