using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace Game {
    public class PlayerMoveMessage : Singleton<PlayerMoveMessage>, IEventMessage {

        private object[] o = { Vector3.zero  };
        public void SendMessage(Vector3 move) {
            o[0] = move;
            UniEvent.SendMessage(Ins, o);
        }
    }

    public class KeyRClickDownMessage : Singleton<KeyRClickDownMessage>, IEventMessage {
        public void SendMessage() {
            UniEvent.SendMessage(Ins);
        }
    }

    public class PlayerController : SingletonMono<PlayerController>, IUIPos {
        public Role Data;

        [HideInInspector] public Animator PlayerAnimator;
        [HideInInspector] public LeftWeaponSlotMono leftWeaponSlotSlot;
        [HideInInspector] public RightWeaponSlotMono rightWeaponSlotSlot;
        [HideInInspector] public RigBuilder PlaterRigBuilder;
        private int rotateAngle = 0;//旋转的角度
        private int rotateEveryTimes = 45;//每次旋转的角度
        private Vector3 rotateAngleV3 = Vector3.zero;

        private PlayerInput input => PlayerInput.Ins;
        private BagManager bag => BagManager.Ins;
        private ElementPickUp pickUp => ElementPickUp.Ins;
        private PlayerStateManager stateManager => PlayerStateManager.Ins;
        public Transform SelfTransHP;
        public Transform SelfFloatText;
        public Transform SelfPickUp;
        public Vector3 PlayerRotateDir = Vector3.forward;

        #region mono周期
        protected override void Awake() {
            base.Awake();
            PlayerAnimator = GetComponentInChildren<Animator>();//自身找不到就找子
            leftWeaponSlotSlot = GetComponentInChildren<LeftWeaponSlotMono>();//自身找不到就找子
            rightWeaponSlotSlot = GetComponentInChildren<RightWeaponSlotMono>();//自身找不到就找子
            PlaterRigBuilder = GetComponentInChildren<RigBuilder>();
        }

        public IEnumerator Start() {
            stateManager.Init();
            input.Init();
            Init(RoleManager.Ins.GetLandData(LandRoleType.Main));
            yield return new WaitUntil(() => PlayerCamera.Ins != null);
            CinemachineManager.Ins.InitFollowCamSimpleFollow(PlayerCamera.Ins.transform, transform);
            //StartCoroutine(Utility.Ins.MonsterRefreshByPosRandom(transform.position, 10, "Item/Goods/Weapon/Cold/Club", obj => {
            //    if (obj.TryGetComponent(out BaseGoodsMono mono) || (mono = obj.AddComponent<BaseGoodsMono>())) {
            //        mono.Init(GoodsManager.Ins.GetNewGoods(ColdWeaponType.Club));
            //    }
            //}, 10));
            StartCoroutine(Utility.Ins.MonsterRefreshByPosRandom(transform.position, 10, "Item/Goods/Weapon/Gun/Gunak47", obj => {
                if (obj.TryGetComponent(out BaseGoodsMono mono) || (mono = obj.AddComponent<BaseGoodsMono>())) {
                    mono.Init(GoodsManager.Ins.GetNewGoods(GunWeaponType.Gunak47));
                }
            }, 10));

            //StartCoroutine(Utility.Ins.MonsterRefreshByPosRandom(transform.position, 10, "Item/Goods/Biology/PlantBiology/TreeWoodPlant", obj => {
            //    if (obj.TryGetComponent(out BaseGoodsMono mono) || (mono = obj.AddComponent<BaseGoodsMono>())) {
            //        mono.Init(GoodsManager.Ins.GetNewGoods(PlantBiologyType.TreeWood));
            //    }
            //}, 20));

            //关闭持枪IK动画
            CloseIK();

            //初始化武器
            leftWeaponSlotSlot.Init(Data);
            rightWeaponSlotSlot.Init(Data);

            //初始化事件
            InitEvent();

            //挂载ElementUI
            var elementPickUp = ResManager.Ins.Load<ElementPickUp>("UI/Common/PickUpTip");
            UIManager.Ins.AddElementUI(elementPickUp, Data);
            //挂载建筑网格
            BottomBuildBegin.Init();
        }

        private void Update() {
            input.DetetionLeftMouseClick(DetetionLeftMouseDownClick, DetetionLeftMouseUpClick);
            input.DetetionRightMouseClick(DetetionRightMouseDown, DetetionRightMouseUp);
            input.DetetionE();
            input.DetetionQ();
            input.DetetionR(DetetionRKeyClick);
            input.DetetionFClick(DetetionFKeyClick);
            input.DetetionTabClick(DetetionTabClick);
            input.DetetionBClick(DetetionBClick);
            input.DetetionNumKeyClick();
            //鼠标射线持续检测事件
            MouseContinueDetectionMessage.Ins.SendMessage();
            EventManager.OnUpdateRefresh.Trigger();
        }

        private void OnTriggerEnter(Collider other) {
            if (other.tag == "Goods") {
                var goods = other.GetComponentInParent<BaseGoodsMono>();
                if (goods != null) {
                    if (bag.FastPickUp) {
                        pickUp?.PickUpGoods(goods);
                    } else {
                        pickUp?.AddCount(goods);
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other) {
            if (other.tag == "Goods") {
                var goods = other.GetComponentInParent<BaseGoodsMono>();
                if (goods != null) {
                    pickUp?.ReduceCount(goods);
                }
            }
        }

        #endregion

        #region 初始化

        public void Init(Role data) {
            Data = data;
            Data.WalkMoveSpeed = 6;
            Data.RunMoveSpeed = 10;
        }
        private void InitEvent() {
            EventManager.Ins.AddMouseRightDownClick(EnterAim);
            EventManager.Ins.AddMouseRightUpClick(QuitAim);
            RegisterEClick();
            RegisterQClick();
        }

        #endregion

        #region 键盘相关事件

        private void DetetionLeftMouseDownClick() {
            MouseLeftClickDownMessage.Ins.SendMessage();
            if (rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null || leftWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null) {
                stateManager.SetArmContinueFire();
            } else if (rightWeaponSlotSlot.CurrentWeapon as ColdWeaponGoods != null || leftWeaponSlotSlot.CurrentWeapon as ColdWeaponGoods != null) {
                stateManager.SwitchArmAttack1();
            }
        }
        private void DetetionLeftMouseUpClick() {
            MouseLeftClickUpMessage.Ins.SendMessage();
            if (rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null || leftWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null) {
                stateManager.SetArmContinueFire(false);
            }
        }
        private void DetetionRightMouseDown() {
            MouseRightClickDownMessage.Ins.SendMessage();
        }
        private void DetetionRightMouseUp() {
            MouseRightClickUpMessage.Ins.SendMessage();
        }
        public void RegisterEClick() {
            EventManager.OnEventEClick.RemoveAll();
            EventManager.OnEventEClick.Register(EventEClick);
        }
        private void EventEClick(ClientEvent e = null, object[] o = null) {
            rotateAngle -= rotateEveryTimes;
            rotateAngleV3.y = rotateAngle;
            PlayerCamera.Ins.UpdateRotate(ref rotateAngleV3);
        }
        public void RegisterQClick() {
            EventManager.OnEventQClick.RemoveAll();
            EventManager.OnEventQClick.Register(EventQClick);
        }
        private void EventQClick(ClientEvent e = null, object[] o = null) {
            rotateAngle += rotateEveryTimes;
            rotateAngleV3.y = rotateAngle;
            PlayerCamera.Ins.UpdateRotate(ref rotateAngleV3);
        }
        private void DetetionRKeyClick() {
            var gunWeapon = rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods;
            if (gunWeapon != null) {
                //换弹动画
                //查询快捷背包子弹数量
                if (BagManager.Ins.GetNumByGoods(GoodsManager.Ins.GetGoodsData(gunWeapon.BulletType)) <= 0)
                    return;
                stateManager.ArmSwitchState(typeof(ShootReloadState));
            }
        }

        private void DetetionFKeyClick() {
            var goods = pickUp.GetPickUpGoods();
            if (goods != null) {
                pickUp.ReduceCount(goods);
                pickUp.PickUpGoods(goods);
            }
        }

        private void DetetionTabClick() {
            if (!BuildCreateDialog.Ins)
                BuildCreateDialog.Show();
            else
                BuildCreateDialog.Ins?.Close();
        }

        private void DetetionBClick() {
            if (!BagDialog.Ins)
                BagDialog.Show();
            else
                BagDialog.Ins.Close();
        }

        /// <summary>
        /// 旋转
        /// </summary>
        /// <param name="targetDir"></param>
        public void Rotation(Vector3 targetDir) {
            PlayerRotateDir = targetDir;
            transform.rotation = Quaternion.LookRotation(PlayerRotateDir);
            PlayerAnimator.transform.rotation = transform.rotation;
            transform.Rotate(rotateAngleV3);//在旋转rotateAngleV3角度
        }
        /// <summary>
        /// 模型旋转
        /// </summary>
        public void RotationModel(Vector3 targetDir) {
            Debug.Log("模型转向");
            PlayerAnimator.transform.rotation = Quaternion.LookRotation(targetDir);
        }
        /// <summary>
        /// 走路
        /// </summary>
        /// <param name="e"></param>
        /// <param name="o"></param>
        public void WalkMove(IEventMessage e, object[] o) {
            //速度
            Vector3 dir = (Vector3) o[0];
            Rotation(dir.normalized);
            if (stateManager.IsInAim) {
                //动画切换
                Vector3 playerDir = Utility.Ins.GetDirByMainCameraMousePoint();
                playerDir = new Vector3(playerDir.x, 0, playerDir.y);
                //转向
                RotationModel(playerDir);
            }
            //移动
            transform.Translate(Vector3.forward * Data.WalkMoveSpeed * Time.deltaTime);

            //更新摄像机位置
            PlayerCamera.Ins.UpdatePos(transform.position);
        }
        /// <summary>
        /// 跑步
        /// </summary>
        /// <param name="e"></param>
        /// <param name="o"></param>
        public void RunMove(IEventMessage e, object[] o) {
            Vector3 dir = (Vector3) o[0];
            Rotation(dir.normalized);
            if (stateManager.IsInAim) {
                //动画切换
                Vector3 playerDir = Utility.Ins.GetDirByMainCameraMousePoint();
                playerDir = new Vector3(playerDir.x, 0, playerDir.y);
                //转向
                RotationModel(playerDir);
            }
            transform.Translate(Vector3.forward * Data.RunMoveSpeed * Time.deltaTime);

            //更新摄像机位置
            PlayerCamera.Ins.UpdatePos(transform.position);
        }
        /// <summary>
        /// 进入瞄准
        /// </summary>
        /// <param name="e"></param>
        /// <param name="o"></param>
        private void EnterAim(IEventMessage e, object[] o = null) {
            if (rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null && !stateManager.IsInAim) {
                stateManager.IsInAim = true;
                SetIsCanHoldGun(false);
                stateManager.ArmSwitchState(typeof(ShootIdleState));
                EventManager.OnInArmEvent.Trigger();
            }
        }
        /// <summary>
        /// 退出瞄准
        /// </summary>
        /// <param name="e"></param>
        /// <param name="o"></param>
        private void QuitAim(IEventMessage e, object[] o = null) {
            if (rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods != null && stateManager.IsInAim) {
                stateManager.IsInAim = false;
                SetIsCanHoldGun(true);
                EventManager.OnOutArmEvent.Trigger();
            }
        }
        public void SetIsCanHoldGun(bool flag) {
            PlayerAnimator.SetBool(PlayerStateManager.Ins.AnimName_IsCanHoldGun, flag);
        }
        /// <summary>
        /// 打开持枪动画IK
        /// </summary>
        public void OpenIK() {
            PlaterRigBuilder.enabled = true;
        }
        /// <summary>
        /// 关闭持枪动画IK
        /// </summary>
        public void CloseIK() {
            PlaterRigBuilder.enabled = false;
        }

        public void SetAnimIsInAim(bool flag) {
            PlayerAnimator.SetBool(PlayerStateManager.Ins.AnimName_IsInAim, flag);
        }

        #endregion

        #region UI位置

        public Transform TransHP { get => SelfTransHP; set { } }
        public Transform TransFloatText { get => SelfFloatText; set { } }
        public Transform TransPickUp { get => SelfPickUp; set { } }

        #endregion

    }

}