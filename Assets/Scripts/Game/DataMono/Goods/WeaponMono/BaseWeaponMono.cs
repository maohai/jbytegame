using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class BaseWeaponMono : MonoBehaviour{
        public IAttack Attack;
        public Transform GunFire;

        public void BindData(IAttack data){
            Attack = data;
        }
    }


}