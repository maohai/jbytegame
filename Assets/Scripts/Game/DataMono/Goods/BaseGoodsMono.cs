using System;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityQuaternion;
using Game;
using TMPro;
using UnityEngine;

public class BaseGoodsMono : MonoBehaviour {
    public Goods GoodsData;
    private TextMeshPro txtName;

    protected Transform TransPivot;
    protected Vector3 Position;
    protected Quaternion Rotate;
    protected Vector3 Scale;

    protected virtual void Awake() { }
    protected virtual void Start() { }

    public virtual void Init(Goods goods) {
        txtName = transform.GetComponentInChildren<TextMeshPro>();
        TransPivot = transform.GetChild(0);
        GoodsData = goods;
        if (txtName != null) {
            txtName.text = GoodsData.Name;
        }
        Position = TransPivot.position;
        Rotate = TransPivot.rotation;
        Scale = TransPivot.localScale;
        TransPivot.position = Vector3.zero;
        TransPivot.rotation = Quaternion.identity;
        TransPivot.localScale = Vector3.one;
    }

    float currentEuler = 0;
    float currentRotateSpeed = 20f;
    public float currentFloat = 0;
    public float floatHeight = 0.2f;
    public float floatSpeed = 0.5f;
    Vector3 pos = new Vector3(0,0,0);
    private void Update() {
        if (currentEuler >= 360)
            currentEuler = 0;
        TransPivot.rotation = Quaternion.Euler(0, currentEuler += currentRotateSpeed * Time.deltaTime, 0);
        if (currentFloat >= Mathf.PI) currentFloat = 0;
        pos.y = Mathf.Sin(currentFloat += Time.deltaTime) * floatSpeed + floatHeight;
        TransPivot.localPosition = pos;
    }

    public void GoodsHide() {
        gameObject.SetActive(false);
        txtName?.gameObject.SetActive(false);
    }

    public virtual void OnDestory(Action cb = null) {
        cb?.Invoke();
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other) { }
    protected virtual void OnTriggerExit(Collider other) { }
}