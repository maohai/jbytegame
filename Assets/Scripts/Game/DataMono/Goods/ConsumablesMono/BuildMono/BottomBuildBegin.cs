using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public class BottomBuildBegin : SingletonMono<BottomBuildBegin> {
        public Transform TransParent;
        public Transform TransBuildBottom;
        public Transform TransBuildBottomGrid;

        private Material matBuildBottom;
        private Vector3 gridPos;
        protected override void Awake() {
            base.Awake();
            matBuildBottom = TransBuildBottom.GetComponent<MeshRenderer>().material;
        }
        public static void Init() {
            PoolManager.Ins.ReleaseNoPool("Item/Goods/Consumables/BuildConsumables/BottomBuildBegin", (obj) => {
                Ins.Hide();
            });
        }
        public void RefreshPos(Vector3 pos) {
            TransBuildBottom.position = new Vector3(pos.x, 0.02f, pos.z);
            TransBuildBottomGrid.position = new Vector3(pos.x, 0.01f, pos.z);
        }

        /// <summary>
        /// 刷新重新刷新位置
        /// </summary>
        /// <param name="pos"></param>
        public void BuildEnter(float width, float height) {
            TransParent.gameObject.SetActive(true);
            //设置底部宽高 /2是因为本身缩放到了0.2倍,网格大小跟世界坐标大小转换为10:1
            TransBuildBottom.localScale = new Vector3(width / 2, 1, height / 2);
            //设置线框位置
            SetGridShow(GoodsManager.Ins.FreeBuild);
        }

        private BuildShaderType? currentType = null;
        public void RefreshShader(BuildShaderType type) {
            if (currentType != null && currentType == type) return;
            currentType = type;
            switch (currentType) {
                case BuildShaderType.Can: {
                        matBuildBottom.SetColor("Trans Color", Color.green);
                        break;
                    }
                case BuildShaderType.Cant: {
                        matBuildBottom.SetColor("Trans Color", Color.red);
                        break;
                    }
                //如果处于建造状态,则直接隐藏
                case BuildShaderType.Buiding: {
                        Hide();
                        break;
                    }
            }
        }
        public void SetGridShow(bool hide) {
            TransBuildBottomGrid.gameObject.SetActive(!hide);
        }
        public void Hide() {
            TransParent.gameObject.SetActive(false);
        }
    }

}