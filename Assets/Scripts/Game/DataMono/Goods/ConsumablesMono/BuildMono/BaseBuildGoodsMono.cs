using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class BaseBuildGoodsMono : BaseGoodsMono {
    public BuildShaderType CurrentShaderState = BuildShaderType.Null;
    protected new BuildConsumablesGoods GoodsData => base.GoodsData as BuildConsumablesGoods;

    private Renderer shaderRender;
    private float buildTime = 0;
    private float currentTime = 0;

    private int rotateAngle = 0;//旋转的角度
    private int rotateEveryTimes = 45;//每次旋转的角度
    private Vector3 rotateAngleV3 = Vector3.zero;

    protected new Collider collider;
    protected Collider collider2Trigger; // 用于选取位置时候碰撞器改为触发器

    private void OnDisable() {
        //按键事件注销
        PlayerController.Ins.RegisterEClick();
        PlayerController.Ins.RegisterQClick();
    }

    public override void Init(Goods goods) {
        base.Init(goods);
        //添加刚体,用作检测是否能够建造
        Utility.Ins.AddRigibody(gameObject);

        collider = GetComponent<Collider>();
        collider2Trigger = transform.GetChild(0).GetComponentInChildren<Collider>();
        //设置collider
        if (collider2Trigger != null)
            collider2Trigger.isTrigger = true;
        //设置shader
        SetShader(BuildShaderType.Can);
        //设置地块
        BottomBuildBegin.Ins.BuildEnter(GoodsData.BuildingCircle, GoodsData.BuildingCircle);
        //注册事件
        RegisteBuild();
    }

    public virtual void Begin(float timer) {
        buildTime = timer;
        currentTime = 0f;
        //删除rigibody
        Utility.Ins.RemoveRigibody(gameObject);
        //设置shader
        SetShader(BuildShaderType.Buiding);
        shaderRender = GetComponentInChildren<Renderer>();
        //改变shader颜色
        shaderRender.material.SetColor("_Color", new Color(0.1f, 0.5f, 0.1f, 0.6f));
        shaderRender.material.SetFloat("_ClipHeight", 0);
        if (collider2Trigger != null)
            collider2Trigger.isTrigger = false;
        //按键事件注销
        PlayerController.Ins.RegisterEClick();
        PlayerController.Ins.RegisterQClick();
    }
    public void SelectPos(Vector3 pos) {
        pos = GoodsManager.Ins.FreeBuildPos(pos);
        transform.position = pos;
        BottomBuildBegin.Ins.RefreshPos(pos);
    }

    public virtual void EndBuild() {
        GoodsData.RemoveAllInfluenceData();
        SetShader(BuildShaderType.End);
        CurrentShaderState = BuildShaderType.Null;
    }

    private void Update() {
        if (shaderRender == null || currentTime >= buildTime || buildTime == 0) return;
        currentTime += Time.deltaTime;
        shaderRender.material.SetFloat("_ClipHeight", currentTime / buildTime);
    }

    protected override void OnTriggerEnter(Collider other) {
        if (GoodsData.GetStateType() == BuildStateType.Move) {
            if (other.tag is "Goods" or "Player")
                GoodsManager.Ins.AddCurrentOnTrigger();
        }
    }

    protected override void OnTriggerExit(Collider other) {
        if (GoodsData.GetStateType() == BuildStateType.Move) {
            if (other.tag is "Goods" or "Player")
                GoodsManager.Ins.ReduceCurrentOnTrigger();
        }
    }
    /// <summary>
    /// 设置shader
    /// </summary>
    /// <param name="type"></param>
    public virtual void SetShader(BuildShaderType type) {
        if (CurrentShaderState == type) return;
        CurrentShaderState = type;
        var render = GetComponentInChildren<Renderer>();

        switch (CurrentShaderState) {
            case BuildShaderType.Can: {
                    render.sharedMaterial = new Material(Shader.Find("Custom/Build/BuildConstructionCondition"));
                    render.material.SetColor("_Color", Color.green);
                    break;
                }
            case BuildShaderType.Cant: {
                    render.sharedMaterial = new Material(Shader.Find("Custom/Build/BuildConstructionCondition"));
                    render.material.SetColor("_Color", Color.red);
                    break;
                }
            case BuildShaderType.Buiding: {
                    render.sharedMaterial = new Material(Shader.Find("Custom/Build/BuildingScan"));
                    var texture = Resources.Load<Texture2D>("Textures/Armchair");
                    render.material.SetTexture("_MainTex", texture);
                    break;
                }
            case BuildShaderType.End: {
                    render.sharedMaterial = new Material(Shader.Find("Standard"));
                    var texture = Resources.Load<Texture2D>("Textures/Armchair");
                    render.material.SetTexture("_MainTex", texture);
                    break;
                }
        }
        BottomBuildBegin.Ins?.RefreshShader(type);
    }
    private void RegisteBuild() {
        RegisterEClick();
        RegisterQClick();
    }
    private void RegisterEClick() {
        EventManager.OnEventEClick.RemoveAll();
        EventManager.OnEventEClick.Register(EventEClick);
    }
    private void EventEClick(ClientEvent e = null, object[] o = null) {
        rotateAngle -= rotateEveryTimes;
        rotateAngleV3.y = rotateAngle;
        transform.rotation = Quaternion.Euler(rotateAngleV3);
    }
    private void RegisterQClick() {
        EventManager.OnEventQClick.RemoveAll();
        EventManager.OnEventQClick.Register(EventQClick);
    }
    private void EventQClick(ClientEvent e = null, object[] o = null) {
        rotateAngle += rotateEveryTimes;
        rotateAngleV3.y = rotateAngle;
        transform.rotation = Quaternion.Euler(rotateAngleV3);
    }
}