using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class BonfireLifeGoodsMono : BaseBuildGoodsMono
{
    private Light pointLight;
    public override void Init(Goods goods)
    {
        base.Init(goods);
        pointLight = GetComponentInChildren<Light>();
        if (pointLight != null)
            pointLight.enabled = false;
    }

    public override void Begin(float timer)
    {
        base.Begin(timer);
        (collider as BoxCollider).size = new Vector3(GoodsData.BuildingCircle, 2, GoodsData.BuildingCircle);
    }
    public override void EndBuild()
    {
        base.EndBuild();
        if (pointLight != null)
            pointLight.enabled = true;
        (collider as BoxCollider).size = new Vector3(GoodsData.EffCircle, 2, GoodsData.EffCircle);
    }
    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if (other.TryGetComponent(out PlayerController player))
        {
            //已经是完成状态下建筑
            if (GoodsData.GetStateType() == BuildStateType.Builded)
            {
                GoodsData.AddInfluenceData(PlayerController.Ins.Data);
            }
        }
    }
    protected override void OnTriggerExit(Collider other) {
        base.OnTriggerExit(other);
        if (other.TryGetComponent(out PlayerController player)){
            //已经是完成状态下建筑
            if (GoodsData.GetStateType() == BuildStateType.Builded){
                GoodsData.RemoveInfluenceData(PlayerController.Ins.Data);
            }
        }
    }
}
