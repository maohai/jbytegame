using UnityEngine;

public class BottomBuildBeginGrid : MonoBehaviour {
    public int xSize, ySize;
    public Vector3[] vertices;
    private Mesh mesh;

    public void Awake() {
        Generate();
    }

    private void Generate() {
        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        // mesh = new Mesh();
        mesh.name = "ProceduralGridPlane";
        for (int i = 0, y = 0; y <= ySize; y++) {
            for (int x = 0; x <= xSize; x++, i++) {
                vertices[i] = new Vector3(x, 0, y);
            }
        }
        mesh.vertices = vertices;
        //其他部分不变
        // 通过triangles数组,记录对应的的组成三角面的顶点顺序
        int[] triangles = new int[xSize * ySize * 6];
        int tag = 0;
        for (int i = 0; i < ySize; i++) {
            for (int j = 0; j < xSize; j++) {
                triangles[tag] = (xSize + 1) * (i + 1) + j;
                triangles[tag + 1] = (xSize + 1) * i + j + 1;
                triangles[tag + 2] = (xSize + 1) * i + j;
                triangles[tag + 3] = triangles[tag];
                triangles[tag + 4] = triangles[tag] + 1;
                triangles[tag + 5] = triangles[tag + 1];
                tag += 6;

                //使面建立也有渐进效果，这里把更新triangles的部分提前到循环里面
                mesh.triangles = triangles;
            }
        }

    }
    //private void OnDrawGizmos() {
    //    Gizmos.color = Color.black;

    //    if (vertices == null) {
    //        Debug.Log("vertices is null");
    //        return;
    //    }

    //    for (int i = 0; i < vertices.Length; i++) {
    //        Gizmos.DrawSphere(vertices[i], 0.1f);
    //    }
    //}
}
