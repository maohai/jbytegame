using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class DeferredDecal : MonoBehaviour {
    private void OnEnable() {
        Camera.main.depthTextureMode |= DepthTextureMode.Depth;
    }
}