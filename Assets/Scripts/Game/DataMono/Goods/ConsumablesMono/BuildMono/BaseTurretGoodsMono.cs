using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Game;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class BaseTurretGoodsMono : BaseBuildGoodsMono
{
    public override void Begin(float timer)
    {
        base.Begin(timer);
        (collider as BoxCollider).size = new Vector3(GoodsData.BuildingCircle, 2, GoodsData.BuildingCircle);
    }
    public override void EndBuild()
    {
        base.EndBuild();
        (collider as BoxCollider).size = new Vector3(GoodsData.EffCircle, 2, GoodsData.EffCircle);
        //开始射线检测
        RaycastHit[] hitsInfo = Physics.SphereCastAll(transform.position, 
            GoodsData.EffCircle, 
            Vector3.forward, 
            10f);
       foreach (var hit in hitsInfo){
           if (hit.transform.TryGetComponent(out BaseElementMono enemy)){
               Debug.Log("敌人UID:" + enemy.Data.UID);
               GoodsData.AddInfluenceData(enemy.Data);
           }
       }
    }

    public virtual void Fire() { }
    public virtual void StopFire() { }
}
