using Cysharp.Threading.Tasks;
using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteelTurretGoodsMono : BaseTurretGoodsMono
{
    public override void OnDestory(Action cb = null)
    {
        base.OnDestory(cb);
    }
    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.TryGetComponent(out BaseElementMono enemy))
        {
            //已经是完成状态下建筑
            if (GoodsData.GetStateType() == BuildStateType.Builded)
            {
                GoodsData.AddInfluenceData(enemy.Data);
                enemy.OnDestoryEvent.Register((e,o) => { GoodsData.RemoveInfluenceData(enemy.Data); });
            }
        }
    }
    
    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
        if (other.TryGetComponent(out BaseElementMono enemy))
        {
            //已经是完成状态下建筑
            if (GoodsData.GetStateType() == BuildStateType.Builded)
            {
                GoodsData.RemoveInfluenceData(enemy.Data);
            }
        }
    }
    private bool isFire = false;
    public async override void Fire()
    {
        if (isFire) return;
        isFire = true;
        base.Fire();
        while (true)
        {
            var bestNear = GoodsData.GetBestNear();
            if (bestNear == null){
                //重置开火
                isFire = false;
                return;
            }
            var dir = Vector3.Normalize(bestNear.ElementMono.transform.position - transform.position);
            PoolManager.Ins.ReleaseNoPool("Item/GoodsUse/Consumables/Bullet/MissileK99", 
                obj => {
                if (obj.TryGetComponent(out BulletMissileK99UseMono mono) 
                    ||(mono = obj.AddComponent<BulletMissileK99UseMono>())){
                    mono.transform.position = transform.position;
                    mono.BindData(GoodsManager.Ins.GetNewGoods(BulletType.MissileK99), 
                        new object[] { new Vector3(dir.x,0,dir.z) });
                    mono.GoodsData.BindWeapon(GoodsData);
                }
            });
            await UniTask.WaitForSeconds(2f);
        }
    }
    public override void StopFire()
    {
        base.StopFire();
        if (GoodsData.GetInfluenceData().Count != 0) { 
            
        }
        Debug.Log("停止发射");
    }
}
