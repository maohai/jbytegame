using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class BulletUseMono : BaseConsumablesUseMono{
    public new Bullet GoodsData => base.GoodsData as Bullet;
    protected Vector3 FireDir = Vector3.forward;

    protected override void Init(object[] o = null){
        base.Init(o);
        FireDir = (Vector3)o[0];
    }
}
