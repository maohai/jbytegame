using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class BaseConsumablesUseMono : MonoBehaviour{
    protected ConsumablesGoods GoodsData;

    public void BindData(Goods goods,object[]o=null){
        GoodsData = goods as ConsumablesGoods;
        Init(o);
    }

    protected virtual void Init(object[]o=null){ }
}
