using System;
using Game;
using TMPro;
using UnityEngine;

namespace Game
{
    public class BaseElementMono : MonoBehaviour
        , IAIEnemyCapsule
        , IUIPos
    {
        protected PlayerController player => PlayerController.Ins;
        protected AIStateManager stateManager => AIStateManager.Ins;
        protected Animator anim;
        protected new Collider collider;
        public ElementData Data;
        public LeftWeaponSlotMono leftWeaponSlotSlot;
        public RightWeaponSlotMono rightWeaponSlotSlot;
        public Transform SelfTransHP;

        public ClientEvent OnInitEvent = new ClientEvent();
        public ClientEvent OnDestoryEvent = new ClientEvent();
        protected virtual void Start()
        {
            anim = GetComponentInChildren<Animator>();
            collider = GetComponentInChildren<Collider>();
            leftWeaponSlotSlot = GetComponentInChildren<LeftWeaponSlotMono>();
            rightWeaponSlotSlot = GetComponentInChildren<RightWeaponSlotMono>();
            SelfTransHP = transform.GetChild(0).Find("TransHP");
            //初始化状态机
            InitState();
        }

        protected void Init(ElementData data)
        {
            Data = data;
            OnInitEvent.Trigger();
            //绑定数据
            Data.BindMono(this);
            // 使用状态机 而非用 行为树框架
            // BTManager.Ins.AddTree(transform,"AICommonBehavior",new []{role});
            stateManager.AddAIState(this);
        }

        public virtual void OnDestory()
        {
            OnDestoryEvent.Trigger();
            OnDestoryEvent.RemoveAll();
            Destroy(gameObject);
        }


        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.GetComponentInChildren<BaseWeaponMono>() is var baseWeapon
                && baseWeapon != null
                && Data != null)
            {
                //武器打人
                baseWeapon.Attack.InflictOther(Data);
            }
            if (other.GetComponentInParent<BulletUseMono>() is var baseBullet
                && baseBullet != null
                && Data != null)
            {
                //武器打人
                baseBullet.GoodsData.InflictOther(Data);
                PoolManager.Ins.Recycle(baseBullet.gameObject);
            }
        }

        protected virtual void OnTriggerStay(Collider collider) { }

        #region 接口
        #region 状态切换全局变量

        private Type[] IgnoreSwitch = new[] { typeof(AIDeathState) };
        private Type[] IgnoreArmSwitch = new[] { typeof(AIReactState) };
        public bool CanSwitchState = true;
        public bool CanSwitchArmState = true;

        protected virtual void InitState()
        {
            CanSwitchState = true;
            CanSwitchArmState = true;
        }

        public bool CheckIgnore(Type t)
        {
            foreach (var type in IgnoreSwitch)
            {
                if (type == t)
                    return true;
            }

            return false;
        }
        public bool CheckArmIgnore(Type t)
        {
            foreach (var type in IgnoreArmSwitch)
            {
                if (type == t)
                    return true;
            }

            return false;
        }

        #endregion

        public virtual void EnterAIIdle() { }
        public virtual void ExitAIIdle() { }
        public virtual void LogicUpdateAIIdle() { }
        public virtual void PhysicUpdateAIIdle() { }
        public virtual void EnterAIAttack() { }
        public virtual void ExitAIAttack() { }
        public virtual void LogicUpdateAIAttack() { }
        public virtual void PhysicUpdateAIAttack() { }
        public virtual void EnterAIMove() { }
        public virtual void ExitAIMove() { }
        public virtual void LogicUpdateAIMove() { }
        public virtual void PhysicUpdateAIMove() { }
        public virtual void EnterAIFollow() { }
        public virtual void ExitAIFollow() { }
        public virtual void LogicUpdateAIFollow() { }
        public virtual void PhysicUpdateAIFollow() { }
        public virtual void EnterAIReaction() { }
        public virtual void ExitAIReaction() { }
        public virtual void LogicUpdateAIReaction() { }
        public virtual void PhysicUpdateAIReaction() { }

        public virtual void EnterAIDeath()
        {
            collider.enabled = false;
            //禁用脚本
            enabled = false;
            OnDestory();
        }
        public virtual void ExitAIDeath() { }
        public virtual void LogicUpdateAIDeath() { }
        public virtual void PhysicUpdateAIDeath() { }

        #endregion

        public Transform TransHP { get => SelfTransHP; set { } }
        public Transform TransFloatText { get => SelfTransHP; set { } }
        public Transform TransPickUp { get; set; }
    }
}
