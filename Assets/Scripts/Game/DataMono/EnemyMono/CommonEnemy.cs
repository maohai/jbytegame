using System;
using System.Collections;
using System.Collections.Generic;
using BTTree;
using Game;
using UnityEngine;
public class CommonEnemy : BaseRoleMono
{
    protected override void Start()
    {
        base.Start();
        Init(RoleManager.Ins.GetLandData(LandRoleType.E1));

        var hp = ResManager.Ins.Load<ElementHP>("UI/Common/HP");
        UIManager.Ins.AddElementUI(hp, Data);
    }

    protected override void OnTriggerStay(Collider collider)
    {
        base.OnTriggerStay(collider);
        if (canHurt && alreayAttack && collider.TryGetComponent(out PlayerController player))
        {
            canHurt = false;
            alreayAttack = false;
            player.Data.UseGoods(GoodsManager.Ins.GetNewGoods(BuffType.ReduceHP, new object[] { -20 }));
        }
    }

    #region 接口

    public int Vertival = Animator.StringToHash("CommonEnemyVertical");
    public int Horizontal = Animator.StringToHash("CommonEnemyHorizontal");

    #region 站立
    private float SwitchMoveTime = 0;
    private string locomotion = "Locomotion";
    public override void EnterAIIdle()
    {
        base.EnterAIIdle();
        SwitchMoveTime = UnityEngine.Random.Range(2, 5);
        anim.CrossFade(locomotion, 0.2f);
    }

    public override void LogicUpdateAIIdle()
    {
        base.LogicUpdateAIIdle();
        anim.SetFloat(Vertival, 0, 0.1f, Time.deltaTime);

        //被抓到了,需要持续追踪
        if (followHold)
        {
            //先等待一会再去抓
            if (CurrentFollowWaitTime > 0)
            {
                CurrentFollowWaitTime -= Time.deltaTime;
                return;
            }
            followHold = false;
            stateManager.SwitchState(this, typeof(AIFollowState));
        }
        if (Vector3.Distance(transform.position, player.transform.position) < 3f)
        {
            stateManager.SwitchState(this, typeof(AIFollowState));
        }
        if (SwitchMoveTime >= 0)
        {
            SwitchMoveTime -= Time.deltaTime;
            if (SwitchMoveTime <= 0)
            {
                stateManager.SwitchState(this, typeof(AIMoveState));
            }
        }
    }

    #endregion

    #region 攻击

    private bool canHurt;//是否能够伤害
    private bool alreayAttack;//是否已经造成了攻击
    private string attackName = "ZombieAttack";
    private float attackBaseTime => Utility.Ins.GetAnimatorTimer(anim, attackName);
    private float attackCurrentTime = 0;
    public override void EnterAIAttack()
    {
        base.EnterAIAttack();
        alreayAttack = true;
        CanSwitchArmState = false;
        anim.CrossFade(attackName, 0.2f);
        attackCurrentTime = attackBaseTime / 2;//先减少一半时间
    }
    public override void ExitAIAttack()
    {
        base.ExitAIAttack();
        alreayAttack = false;
        attackCurrentTime = 0;
        CanSwitchArmState = true;
        canHurt = false;
        anim.CrossFade("Empty", 0.2f);
    }
    public override void LogicUpdateAIAttack()
    {
        base.LogicUpdateAIAttack();
        if (attackCurrentTime > 0)
        {
            attackCurrentTime -= Time.deltaTime;
            if (attackBaseTime - attackCurrentTime > 0.8f)
            {
                canHurt = true;
            }
            return;
        }
        stateManager.ClearCurrentArmState(this);
    }
    public override void PhysicUpdateAIAttack()
    {
        base.PhysicUpdateAIAttack();
    }

    #endregion

    #region 移动
    private float MoveTime = 0;
    public override void EnterAIMove()
    {
        base.EnterAIMove();
        anim.CrossFade(locomotion, 0.2f);
        MoveTime = UnityEngine.Random.Range(5, 10);
    }

    public override void LogicUpdateAIMove()
    {
        base.LogicUpdateAIMove();
        anim.SetFloat(Vertival, 0.5f, 0.1f, Time.deltaTime);
    }
    public override void PhysicUpdateAIMove()
    {
        base.PhysicUpdateAIMove();
        //时间到了就切到走路
        if (MoveTime > 0)
        {
            MoveTime -= Time.deltaTime;
            if (MoveTime < 0)
            {
                stateManager.SwitchState(this, typeof(AIIdleState));
            }
        }
        //如果离主角太近,抓人
        if (Vector3.Distance(transform.position, player.transform.position) < 3f)
        {
            stateManager.SwitchState(this, typeof(AIFollowState));
        }
        //持续移动
        if (Data.NextMovePos == Vector3.zero || Vector3.Distance(Data.NextMovePos, transform.position) < 0.1f)
        {
            Data.NextMovePos = Utility.Ins.AIMove(new Vector3(transform.position.x, 1, transform.position.z), 10);
        }
        else
        {
            var dir = (Data.NextMovePos - transform.position).normalized;
            transform.rotation = Quaternion.LookRotation(dir);
            transform.Translate(Vector3.forward * Data.WalkMoveSpeed * Time.deltaTime);
        }
    }

    #endregion

    #region 追踪

    private bool followHold;//抓到了
    private float baseFollowWaitTime = 1f;
    private float CurrentFollowWaitTime = 0;
    public override void EnterAIFollow()
    {
        base.EnterAIFollow();
        anim.CrossFade(locomotion, 0.2f);
    }

    public override void LogicUpdateAIFollow()
    {
        base.LogicUpdateAIFollow();
        anim.SetFloat(Vertival, 1, 0.2f, Time.deltaTime);
    }

    public override void PhysicUpdateAIFollow()
    {
        base.PhysicUpdateAIFollow();
        if (Data.NextMovePos != Vector3.zero)
            Data.NextMovePos = Vector3.zero;
        var dis = Vector3.Distance(player.transform.position, transform.position);
        if (dis >= 1f)
        {
            var dir = (player.transform.position - transform.position).normalized;
            transform.rotation = Quaternion.LookRotation(dir);
            transform.Translate(Vector3.forward * Data.RunMoveSpeed * Time.deltaTime);
            //大于十米直接傻眼
            if (dis > 10f)
            {
                stateManager.SwitchState(this, typeof(AIIdleState));
            }
        }
        else
        {
            stateManager.ArmSwitchState(this, typeof(AIAttackState));

            followHold = true;
            CurrentFollowWaitTime = baseFollowWaitTime;
            stateManager.SwitchState(this, typeof(AIIdleState));
        }
    }

    #endregion

    #region 受伤

    private string reactionAnim = "ZombieReaction";
    private float reactionBaseTime => Utility.Ins.GetAnimatorTimer(anim, reactionAnim);
    private float reactionCurrentTime = 0;
    public override void EnterAIReaction()
    {
        base.EnterAIReaction();
        CanSwitchState = false;
        reactionCurrentTime = reactionBaseTime;
        anim.CrossFade(reactionAnim, 0.2f);
    }

    public override void ExitAIReaction()
    {
        base.ExitAIReaction();
    }

    public override void LogicUpdateAIReaction()
    {
        base.LogicUpdateAIReaction();
        if (reactionCurrentTime > 0)
        {
            reactionCurrentTime -= Time.deltaTime;
            return;
        }
        CanSwitchState = true;
        stateManager.SwitchState(this, typeof(AIIdleState));
    }

    public override void PhysicUpdateAIReaction()
    {
        base.PhysicUpdateAIReaction();
    }

    #endregion

    #region 死亡
    private string deathAnim = "ZombieDeath";
    public override void EnterAIDeath()
    {
        CanSwitchState = false;
        anim.CrossFade(deathAnim, 0.2f);
        base.EnterAIDeath();
    }

    public override void ExitAIDeath()
    {
        base.ExitAIDeath();
    }

    public override void LogicUpdateAIDeath()
    {
        base.LogicUpdateAIDeath();
    }

    public override void PhysicUpdateAIDeath()
    {
        base.PhysicUpdateAIDeath();
    }


    #endregion

    #endregion
}
