using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game {
    public class PoolManager : SingletonMono<PoolManager> {
        //Pool就是一个对象池
        static Dictionary<string, Pool> dictionary = new ();
        static Dictionary<string, Transform> dicTrans = new();

        void AddPool(Pool pool) {
            dictionary.Add(pool.PrefabName, pool);//key不同，值相同
            Transform poolParent = new GameObject("Pool:" + pool.PrefabName).transform;
            poolParent.parent = transform;
            pool.Initialize(poolParent);
        }

        public void Recycle(GameObject obj) {
            obj.SetActive(false);
        }

        #region RELEASE
        public GameObject ReleaseNoPool(string prefab, Action<GameObject> objEvent) {
            Transform poolParent = null;
            if (dicTrans.ContainsKey(prefab)) {
                poolParent = dicTrans[prefab];
            } else {
                poolParent = new GameObject("Pool:" + prefab).transform;
                dicTrans.Add(prefab, poolParent);
            }

            poolParent.parent = transform;
            var obj = ResManager.Ins.Load<GameObject>(prefab);
            obj.transform.parent = poolParent;
            objEvent?.Invoke(obj);
            return obj;
        }
        public GameObject Release(string prefab, Action<GameObject> objEvent = null, int size = -1) {//释放出一个预制体
            if (!dictionary.ContainsKey(prefab)) {
                AddPool(new Pool(prefab, size));
            }
            //将对应的池中预制件取出
            return dictionary[prefab].PrepareObject(objEvent);
        }
        public GameObject Release(string prefab, Vector3 pos, Action<GameObject> objEvent = null, int size = -1) {//释放出一个预制体
            if (!dictionary.ContainsKey(prefab)) {
                AddPool(new Pool(prefab, size));
            }
            //将对应的池中预制件取出
            return dictionary[prefab].PrepareObject(pos, objEvent);
        }
        public GameObject Release(string prefab, Vector3 pos, Quaternion rotation, Action<GameObject> objEvent = null, int size = -1) {//释放出一个预制体
            if (!dictionary.ContainsKey(prefab)) {
                AddPool(new Pool(prefab, size));
            }
            //将对应的池中预制件取出
            return dictionary[prefab].PrepareObject(pos, rotation, objEvent);
        }
        public GameObject Release(string prefab, Vector3 pos, Quaternion rotation, Vector3 localScale, Action<GameObject> objEvent = null, int size = -1) {//释放出一个预制体
            if (!dictionary.ContainsKey(prefab)) {
                AddPool(new Pool(prefab, size));
            }
            //将对应的池中预制件取出
            return dictionary[prefab].PrepareObject(pos, rotation, localScale, objEvent);
        }
        #endregion

    }

}

