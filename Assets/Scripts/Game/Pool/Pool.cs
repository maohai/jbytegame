using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public class Pool {
        //public GameObject Prefab{get=>prefab;};
        public string PrefabName;
        private const int DefaultSize = 20;
        private int size;

        public Pool(string prefabName, int size = -1) {
            this.size = size != -1 ? size : DefaultSize;
            PrefabName = prefabName;
        }
        Queue<GameObject> queue;

        public int Size => size;
        public int RuntimeSize => queue.Count;
        Transform parent;
        public void Initialize(Transform parent, Action<GameObject> objEvent = null) {
            queue = new Queue<GameObject>();
            this.parent = parent;
            for (int i = 0; i < size; i++) {
                //最末尾添加一个元素
                queue.Enqueue(Copy());
            }
        }
        GameObject Copy() {//创建对象
            GameObject copy = ResManager.Ins.Load<GameObject>(PrefabName);
            copy.transform.SetParent(parent);
            copy.SetActive(false);
            return copy;
        }
        GameObject AvailableObject(Action<GameObject> objEvent = null) {//取出可用的对象
            GameObject availableObject = null;

            //当对象个数大于0，且第一个不是启用的时候
            if (queue.Count > 0 && !queue.Peek().activeSelf)
                //取出最前面的元素并删除队列里的
                availableObject = queue.Dequeue();
            else availableObject = Copy();
            objEvent?.Invoke(availableObject);
            //返回可用的对象之前直接入列
            queue.Enqueue(availableObject);

            return availableObject;
        }
        public GameObject PrepareObject(Action<GameObject> objEvent) {
            GameObject prepareObject = AvailableObject(objEvent);

            prepareObject.SetActive(true);

            return prepareObject;
        }
        public GameObject PrepareObject(Vector3 pos, Action<GameObject> objEvent) {
            GameObject prepareObject = AvailableObject(objEvent);

            prepareObject.SetActive(true);
            prepareObject.transform.position = pos;

            return prepareObject;
        }
        public GameObject PrepareObject(Vector3 pos, Quaternion rotation, Action<GameObject> objEvent) {
            GameObject prepareObject = AvailableObject(objEvent);

            prepareObject.SetActive(true);
            prepareObject.transform.position = pos;
            prepareObject.transform.rotation = rotation;

            return prepareObject;
        }
        public GameObject PrepareObject(Vector3 pos, Quaternion rotation, Vector3 localScale, Action<GameObject> objEvent) {
            GameObject prepareObject = AvailableObject(objEvent);

            prepareObject.SetActive(true);
            prepareObject.transform.position = pos;
            prepareObject.transform.rotation = rotation;
            prepareObject.transform.localScale = localScale;

            return prepareObject;
        }

    }
}