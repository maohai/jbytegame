using System.Collections;
using BehaviorDesigner.Runtime;
using Game;
using Unity.VisualScripting;
using UnityEngine;

public class BTManager : SingletonMono<BTManager>
{
    public void AddTree(Transform trans,string path,object[] o = null){
        var bt = trans.AddComponent<BehaviorTree>();
        bt.StartWhenEnabled = false;
        ResManager.Ins.LoadAsync<ExternalBehaviorTree>("BTRes/"+path, tree => {
            //设置外部行为树
            bt.ExternalBehavior = tree;
            //执行行为树
            bt.EnableBehavior();
        });
    }

    public void RemoveTree(Transform trans){
        if (trans.TryGetComponent(out BehaviorTree com)){
            Destroy(com);
        }
    }
}
