using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : Singleton<SkillManager>{
    public Dictionary<SkillType, SkillData> SkillDatas = new();
    public Dictionary<SkillType, Skill> Skills = new();
    public override void Init(){
        var jsonData = Client.Ins.Data.List2Dic<SkillType,SkillJsonData>("SkillType");
        foreach (var item in jsonData)
        {
            var sd = new SkillData(item.Value);
            var s = new Skill(sd);
            SkillDatas.Add(item.Key, sd);
            Skills.Add(item.Key, s);
        }

        Debug.Log("技能加载完成");
    }

    /// <summary>
    /// 释放技能
    /// </summary>
    public Skill GetSkillData(SkillType type){
        if (Skills.ContainsKey(type))
            return new Skill(Skills[type]);
        return null;
    }

    /// <summary>
    /// 回收技能
    /// </summary>
    public void Back(){
        
    }
}
