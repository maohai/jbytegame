using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// 游戏管理器 用于存储和修改数据
/// </summary>
namespace Game {
    public class GameManager : Singleton<GameManager> {
        public GameManager() {
            //精灵图加载
            AtlasManager.Ins.Init();
            //物品加载
            GoodsManager.Ins.Init();
            //背包初始化
            BagManager.Ins.Init();
            //技能初始化
            SkillManager.Ins.Init();
            //人物数据初始化
            RoleManager.Ins.Init();

            Utility.Ins.Init(); //挂载脚本
        }
    }
}