using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 物品
 * 创造
 * 角色
 */
public enum ItemType{
    Goods,      //物品
    Create,     //创造
    ElementData,//角色
}
public abstract class Item
{
    public int ID;
    public int UID = -1;
    public string Name = "";
    public ItemType ItemType;
    public Item(){
        UID = GetHashCode();
    }

    public Item(Item item){
        ID = item.ID;
        Name = item.Name;
        UID = GetHashCode();
        ItemType = item.ItemType;
    }
}
