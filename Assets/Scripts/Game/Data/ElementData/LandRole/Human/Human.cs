using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public enum HumanType{
        Leading,
        
    }
    public class Human : LandRole{
        public int SoleID => GetHashCode();
        public Human(LandRole role):base(role){}
    }
}