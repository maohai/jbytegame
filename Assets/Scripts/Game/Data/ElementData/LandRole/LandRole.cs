using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public enum LandType{
        Human,//人类
        Robot,//机器人
    }
    public enum LandRoleType
    {
        Main,
        Robot,
        E1,
        E2,
    }
    public class LandRole : Role{
        public LandType LandType;
        public LandRoleType LandRoleType;
        public LandRole(){}

        public LandRole(LandRole role) : base(role){
            LandType = role.LandType;
            LandRoleType = role.LandRoleType;
        }
        public LandRole(RoleJsonData data)
        {
            ID = data.ID;
            LandRoleType = data.LandRoleType;
            Name = data.Name;
            CampType = data.CampType;
            LandType = data.LandType;
            MaxHP = data.MaxHP;
            MaxMP = data.MaxMP;
            BaseAttack = data.BaseAttack;
            BaseDefense = data.BaseDefense;
            MaxHunger = data.MaxHunger;
            MaxVariation = data.MaxVariation;
            OwnerSkillReader = data.OwnerSkillReader;
        }
    }
}