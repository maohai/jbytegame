using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;

public class RoleManager : Singleton<RoleManager>{

    public Dictionary<LandRoleType, LandRole> LandDatas = new ();
    
    public override void Init(){
        var roleList = Client.Ins.Data.List2Dic<LandRoleType,RoleJsonData>("LandRoleType");
        foreach (var role in roleList){
            LandDatas.Add(role.Key,new LandRole(role.Value));
        }
    }
    public Role GetRole(object t,int id){
        if (t is LandRoleType && LandDatas.ContainsKey((LandRoleType)t)){
            return GetLandData((LandRoleType) t);
        }

        return null;
    }
    public LandRole GetLandData(LandRoleType t)
    {
        switch (t)
        {
            case LandRoleType.Main:
            case LandRoleType.Robot:
            case LandRoleType.E1:
            case LandRoleType.E2:
                return new LandRole(LandDatas[t]);
        }
        return null;
    }
}
