using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game{
     /// <summary>
    /// 元素类型
    /// </summary>
    public enum ElementType{
        Land,//陆地生物
        Insect,//虫
        Bird,//鸟
        Ocean,//海洋
    }

    /// <summary>
    /// 阵营
    /// </summary>
    public enum CampType{//阵营
        Keep,//保守
        Fuse,//融合
        Neutrality,//中立
    }

    /// <summary>
    /// 关系
    /// </summary>
    public enum RelationType{//宠物 恋人 陌生人 友好的
        /// <summary>
        /// 敌对的
        /// </summary>
        Hostile,
        // /// <summary>
        // /// 友好的   可以细分
        // /// </summary>
        // Friendly,
        /// <summary>
        /// 盟友
        /// </summary>
        Ally,
        /// <summary>
        /// 中立的
        /// </summary>
        Neutral,
    }
    /// <summary>
    /// 游戏元素对象基类 可以是建筑 角色
    /// </summary>
    public class ElementData : Item{
        protected GoodsManager goodsManager => GoodsManager.Ins;
        
        #region 基础数据

        public ElementType ElementType;
        public CampType CampType;
        //绑定的游戏对象 和UI接口
        public BaseElementMono ElementMono;
        public IUIPos UIPos{
            get{
                if (ElementMono != null)
                    return ElementMono;
                return PlayerController.Ins;
            }
        }

        //血量相关
        public int MaxHP;
        public int HP;
        //蓝量
        public int MaxMP;
        public int MP;
        //攻击防御力
        public int BaseAttack;//基础攻击力
        public int Attack;//当前攻击力
        public int BaseDefense;
        public int Defense;//防御力
        //饥饿相关
        public int MaxHunger;
        public int Hunger;
        public bool KeepHunger;//保持不降低
        //变异相关
        public int MaxVariation;
        public int Variation;//变异程度
       
        //技能相关
        public Coroutine CurrentSkillCoroutine = null;
        public int CurrentSkillID = -1;
        public Skill CurrentSkill;
        public string OwnerSkillReader = "";
        public List<int> OwnerSkill = new ();//拥有的技能
        
        //移动相关数据
        public float BaseWalkMoveSpeed;
        public float WalkMoveSpeed;
        public float BaseRunMoveSpeed;
        public float RunMoveSpeed;
        public Vector3 NextMovePos;//下一次移动的位置

        public bool IsADC;//是否为远程攻击
        public bool IsVenom;//是否带毒
        public bool IsReduceDefence;//是否减防
        public float ReduceDefenceFactor;//减多少 小数为百分比,整数为具体数字 0为全减
        public bool IsDizze;// 是否眩晕
        
        public ElementData(){}
        public ElementData(ElementData data):base(data){
            ElementType = data.ElementType;
            CampType = data.CampType;
            MaxHP = data.MaxHP;
            HP = data.HP;
            MaxMP = data.MaxMP;
            MP = data.MP;
            Attack = BaseAttack = data.BaseAttack;
            Defense = BaseDefense = data.BaseDefense;
            ID = data.ID;
            Name = data.Name;
            Hunger = MaxHunger = data.MaxHunger;
            Variation = MaxVariation = data.MaxVariation;
            //技能相关
            OwnerSkillReader = data.OwnerSkillReader;
            if (OwnerSkillReader != null && OwnerSkillReader != ""){
                var list = Array.ConvertAll(OwnerSkillReader.Split("|"), int.Parse);
                foreach (var i in list){
                    var type = (SkillType)i;
                    if(SkillManager.Ins.GetSkillData(type) != null)
                        OwnerSkill.Add(i);
                    else Debug.Log("没有这个技能");
                }
                OwnerSkill.Distinct();//去重
            }
            //速度相关
            BaseWalkMoveSpeed = 0.3f;
            WalkMoveSpeed = 0.3f;
            BaseRunMoveSpeed = 2f;
            RunMoveSpeed = 2f;

        }

        #endregion
        
        #region 基础数据方法

        public void HP_pm(int value){
            var offsetHP = HP;
            Utility.NumClam(ref HP, Mathf.Abs(value), value >= 0 ? MaxHP : 0, value >= 0);
            //没变化 直接跳过
            if (offsetHP != HP)
            {
                //伤害飘字
                var floatText = ResManager.Ins.Load<ElementFloatText>("UI/Common/FloatText");
                FloatTextType textType = value > 0 ? FloatTextType.CommonAddHP : FloatTextType.CommonReduceHP;
                //todo 暴击
                // textType = FloatTextType.CriticalStrike;
                UIManager.Ins.AddElementUI(floatText, this, new object[] { value.ToString(), textType });
                UIManager.Ins.UpdateUIMono(typeof(ElementHP), this);

                //刷新UI变化
                if (value < 0)
                {
                    if (ElementMono != null)
                    {
                        if (HP <= 0) AIStateManager.Ins.SwitchState(ElementMono, typeof(AIDeathState));
                        else AIStateManager.Ins.SwitchState(ElementMono, typeof(AIReactState));
                    }
                    else
                    {
                        PlayerDialog.Ins.PlayerHPUI.RefreshHP(HP * 1.0f / MaxHP);
                        if (HP <= 0) PlayerStateManager.Ins.SwitchState(typeof(DeathState));
                        else PlayerStateManager.Ins.ArmSwitchState(typeof(ReactState));
                    }
                }
            }
        }
        public void MP_pm(int value)=> Utility.NumClam(ref MP,Mathf.Abs(value),value>=0?MaxMP:0,value>=0);
        public void BindMono(BaseElementMono mono) => ElementMono = mono;
        
        #endregion

        #region buff debuff 物品效果
        /// <summary>
        /// 正在生效的物品  存储效果相关
        /// id 物品
        /// </summary>
        public Dictionary<int, Goods> GoodsBeInEffects = new();
        
         /// <summary>
        /// 添加效果
        /// </summary>
        /// <param name="e">效果</param>
        /// <param name="o">传参</param>
        public void UseGoods(Goods goods,object[] o = null){
            if (goods == null){
                Debug.LogError("物品为空");
                return;
            }

            int id = goods.ID;
            // Debug.Log(e.GetType()+" "+e.GetHashCode());
            if (GoodsBeInEffects.ContainsKey(id)){
                //Debug.Log(goods.GetType()+"效果重置");
                //引用报空bug
                GoodsBeInEffects[id].Reset();
            }
            else{
                //先加入再初始化
                GoodsBeInEffects.Add(id,goods);
                goods.Init(this,o);
                //Debug.Log(goods.GetType()+"效果加入");
            }
         }

         public void UseGoods(int id, object[] o = null){
             var goods = goodsManager.GetNewGoods(id);
             UseGoods(goods,o);
         }
        /// <summary>
        /// 移除效果
        /// </summary>
        /// <param name="e"></param>
        public void RemoveGoods(Goods goods)=> RemoveGoods(goods.ID);
        public void RemoveGoods(int id){
            if (GoodsBeInEffects.ContainsKey(id)){
                var e = GoodsBeInEffects[id];
                GoodsBeInEffects.Remove(id);
                e.Remove();
                //Debug.Log(e.GetType()+"效果移除");
            }
        }
        
        public List<WeaponGoods> GetAllWeapon(){
            List<WeaponGoods> list = new List<WeaponGoods>();
            foreach (var item in GoodsBeInEffects){
                var temp = item.Value as WeaponGoods;
                if (temp != null)
                    list.Add(temp);
            }
            return list;
        }
        
        
        public void CalculateAttack(){
            Attack = BaseAttack;
            var list = GetAllWeapon();
            list.Sort((a,b)=>-a.GetWeaponGoods().IsAddCalculate.CompareTo(b.GetWeaponGoods().IsAddCalculate));
            foreach (var gene in list){
                gene.Calculate();
            }
        }
        public void ExcuteGene(Role role){
            var list = GetAllWeapon();
            foreach (var gene in list){
                gene.InflictOther(role);
            }
        }
        
        public bool CanAdc(){
            bool flag = false;
            foreach (var gene in GetAllWeapon()){
                if (gene.GetWeaponGoods().IsADC){
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        public bool ContainsKey(int id) => GoodsBeInEffects.ContainsKey(id);
        public bool ContainsKey(Goods goods) => ContainsKey(goods.ID);

        #endregion

        #region 关系相关方法
        /// <summary>
        /// 阵营相关  <关系,hashcode>
        /// </summary>
        public Dictionary<int,RelationType> SelfRelation = new();

        /// <summary>
        /// 添加关系
        /// </summary>
        /// <param name="hashCode"></param>
        /// <param name="type"></param>
        public void AddRelation(int hashCode,RelationType type){
            if (SelfRelation.ContainsKey(hashCode)){
                Debug.Log("你们是"+type+"关系");
                return;
            }
            SelfRelation.Add(hashCode,type);
        }

        /// <summary>
        /// 关系变化
        /// </summary>
        /// <param name="hashCode"></param>
        /// <param name="type"></param>
        public void TransRelation(int hashCode, RelationType type){
            if (SelfRelation.ContainsKey(hashCode)){
                SelfRelation[hashCode] = type;
                Debug.Log("你们改为"+type+"关系");
                return;
            }
            SelfRelation.Add(hashCode,type);
        }

        /// <summary>
        /// 移除关系   忘记
        /// </summary>
        /// <param name="hashCode"></param>
        /// <param name="type"></param>
        public void RemoveRelation(int hashCode, RelationType type){
            if (SelfRelation.ContainsKey(hashCode)){
                Debug.Log("移除成功");
                SelfRelation.Remove(hashCode);
            }
            else{
                Debug.Log("没有这个人");
            }
        }

        #endregion
        
        #region 技能相关

        private bool OwnerSkillByID(int id){
            foreach (int i in OwnerSkill)
                if (i == id)
                    return true;
            return false;
        }
        /// <summary>
        /// 释放技能
        /// </summary>
        /// <param name="id">技能id</param>
        /// <param name="pos">释放位置</param>
        /// <param name="dir">方向</param>
        /// <param name="isDir">是否只改变方向</param>
        /// <param name="other">是否是其他人为目标</param>
        /// <returns></returns>
        public IEnumerator ReleaseSkill(int id,ElementData other = null){
            if (OwnerSkillByID(id) && CurrentSkillID==-1 && CurrentSkill == null){
                CurrentSkillID = id;
                var type = (SkillType)CurrentSkillID;
                CurrentSkill = SkillManager.Ins.GetSkillData(type);
                CurrentSkill.SkillState = SkillState.Init;
                //todo 根据技能来确定释放方式
                yield return CurrentSkill.ReadyPutSkill();
                //技能释放目标  等待选定位置或者物体
                yield return new WaitUntil(() => CurrentSkill.SkillState == SkillState.Ready);
                yield return CurrentSkill.ReadyPutSkill(other==null?this:null,other);
                //技能释放位置
                yield return CurrentSkill.Put(CurrentSkill.Position,CurrentSkill.DirPosition,CurrentSkill.IsDir);
                yield return new WaitUntil(() => CurrentSkill.SkillState == SkillState.End);
                //结束
                SkillEnd();
            } else Debug.Log("没有这个技能");
        }

        /// <summary>
        /// 打断技能
        /// </summary>
        public void BreakSkill(){
            if (CurrentSkill != null){
                CurrentSkill.BreakSkill();
                SkillEnd();
                SkillCross.Ins.Hide();//指示器直接结束
            }
        }
        /// <summary>
        /// 当前技能循环结束
        /// </summary>
        private void SkillEnd(){
            CurrentSkillID = -1;
            CurrentSkill = null;
        }

        #endregion

    }
}