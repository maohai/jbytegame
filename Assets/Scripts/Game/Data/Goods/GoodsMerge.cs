using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MergeType
{
    Torch1,
    Torch2, 
    Torch3,
    Bonfire,    //篝火
}
public class GoodsMerge
{
    //数值表
    public int ID;
    public MergeType MergeType;
    public string Name;
    public string MergePath;
    public int TargetID;
    public GoodsType GoodsType;
    //解析数据
    public List<(int id, int num)> MergeMaterials = new();

    public GoodsMerge() { }
    public GoodsMerge(GoodsMerge data)
    {
        ID = data.ID;
        MergeType = data.MergeType;
        Name = data.Name;
        MergePath = data.MergePath;
        MergeMaterials = data.MergeMaterials;
        TargetID = data.TargetID;
        GoodsType = data.GoodsType;
    }
    public GoodsMerge(MergeJsonData data)
    {
        ID = data.ID;
        MergeType = data.MergeType;
        Name = data.Name;
        MergePath = data.MergePath;
        var list = MergePath.Split('|');
        foreach (var item in list)
        {
            var value = item.Split(',');
            MergeMaterials.Add((value[0].ToInt(), value[1].ToInt()));
        }
        TargetID = data.TargetID;


        var goods = GoodsManager.Ins.GetGoodsData(TargetID);
        GoodsType = goods.GoodsType;
    }

    /// <summary>
    /// 检查材料够不够
    /// </summary>
    /// <returns></returns>
    public bool CheckEnough()
    {
        bool flag = true;
        foreach (var item in MergeMaterials)
        {
            var num = BagManager.Ins.GetAllNumByGoods(item.id);
            if (num < item.num)
            {
                flag = false;
                break;
            }
        }
        return flag;
    }
    /// <summary>
    /// 扣除材料
    /// </summary>
    /// <returns></returns>
    public bool DeductMaterial()
    {
        bool flag = CheckEnough();
        //材料不够
        if (!flag) return flag;
        //扣除材料
        foreach (var item in MergeMaterials)
        {
            BagManager.Ins.RemoveUsedGoods(item.id, item.num);
        }
        return flag;
    }
    /// <summary>
    /// 合成
    /// </summary>
    /// <returns></returns>
    public bool Merge()
    {
        var flag = DeductMaterial();
        if (flag)
        {
            if (GoodsType == GoodsType.Consumables)
            {
                //退出界面并生成建筑
                UIManager.Ins.CloseAll();
                var build = GoodsManager.Ins.GetNewGoods(TargetID);
                build.GenerateCreate();
            }
            else
            {
                //合成物品
                BagManager.Ins.AddGoods(TargetID, 1);
            }
        }
        return flag;
    }
}
