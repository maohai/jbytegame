using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 篝火
/// </summary>
public class BonfireLifeBuildConsumablesGoods : LifeBuildConsumablesGoods {
    public BonfireLifeBuildConsumablesGoods() { }
    public BonfireLifeBuildConsumablesGoods(LifeBuildConsumablesGoods item, object[] o = null) : base(item, o) { }

    public override void Init(ElementData data, object[] o = null) {
        base.Init(data, o);
    }

    public override void CreateMonoInit() {
        base.CreateMonoInit();
        BuildMonoCom = BuildMono.AddComponent<BonfireLifeGoodsMono>();
        BuildMonoCom.Init(this);
    }

    public override void Near(int id) {
        if (!InfluenceData.ContainsKey(id)) return;
        base.Near(id);
        InfluenceData[id].UseGoods(GoodsManager.Ins.GetNewGoods(BuffType.ContinueHP, new object[] { 1 }));
    }

    public override void Far(int id) {
        base.Far(id);
        InfluenceData[id].RemoveGoods(GoodsManager.Ins.GetGoodsData(BuffType.ContinueHP));
    }

}
