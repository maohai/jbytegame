using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public enum LifeBuildCreateType
{
    Bonfire,//篝火
}
public class LifeBuildConsumablesGoods : BuildConsumablesGoods
{
    public LifeBuildCreateType LifeBuildCreateType;

    public LifeBuildConsumablesGoods() { }
    public LifeBuildConsumablesGoods(LifeBuildConsumablesGoods item,object[]o=null) : base(item,o) {
        LifeBuildCreateType = item.LifeBuildCreateType;
    }
    
    public LifeBuildConsumablesGoods(LifeBuildCreateJsonData data)
    {
        ID = data.ID;
        Name = data.Name;
        Path = data.Path;
        CreateTime = data.CreateTime;
        BuildCreateType = data.BuildCreateType;
        LifeBuildCreateType = data.LifeBuildCreateType;
        BagLimitNum = data.BagLimitNum;
        BuildingCircle = data.BuildingCircle;
        EffCircle = data.EffCircle;
    }
}

