using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Game;
using UnityEngine;

/*
    建筑
    科研
    生活
    回收利用
    炮塔
  */
public enum BuildCreateType {
    Life, //生存 - 篝火,烹饪锅,烤肉架,回血图腾
    Scientific, //科研 - 一级工作台,献祭
    Recycle, //回收利用 - 资源回收,尸体回收,装备回收,建筑打包技术
    Turret, //塔 - 箭塔 炮塔 陷阱
}

public enum BuildStateType {
    Init, //初始化装填
    Move, //移动状态
    Building, //正在建造状态
    Builded, //建造完成
    Disable, //禁用状态,也就是回收了
}

public enum BuildShaderType {
    Can,
    Cant,
    Buiding,
    End,
    Null
}

public class BuildConsumablesGoods : ConsumablesGoods, IAttack {
    public BuildCreateType BuildCreateType;

    public int MaxHP; //血量
    public int MaxDefense; //防御力
    public int MaxAttack; //攻击力
    public string Path; //建筑路径
    public float BuildingCircle;//建造范围大小
    public float EffCircle;//影响范围大小
    /// <summary>
    /// 建筑资源 id 数量
    /// </summary>
    public Dictionary<int, int> BuildResource = new();

    public GameObject BuildMono; //关联建筑
    public BaseBuildGoodsMono BuildMonoCom;//关联mono
    private BuildStateType BuildStateType;

    public BuildConsumablesGoods() { }
    public BuildConsumablesGoods(BuildConsumablesGoods item, object[] o = null) : base(item, o) {
        BuildCreateType = item.BuildCreateType;
        MaxHP = item.MaxHP;
        MaxDefense = item.MaxDefense;
        MaxAttack = item.MaxAttack;
        Path = item.Path;
        BuildingCircle = item.BuildingCircle;
        EffCircle = item.EffCircle;
    }
    public override void Init(ElementData data, object[] o = null) {
        base.Init(data, o);
        SetStateType(BuildStateType.Init);
    }

    #region 建造

    public BuildStateType GetStateType() => BuildStateType;
    public void SetStateType(BuildStateType type) {
        if (BuildStateType == type) return;
        BuildStateType = type;
    }
    /// <summary>
    /// 准备建造,用作选择位置
    /// </summary>
    public override void GenerateCreate() {
        base.GenerateCreate();
        if (GoodsManager.Ins.IsBuildMove) return;
        GoodsManager.Ins.IsBuildMove = true;
        SetStateType(BuildStateType.Move);
        EventManager.Ins.AddMouseContinuouseDetection(SelectPos);
        EventManager.Ins.AddMouseLeftDownClick(BeginBuild);
        EventManager.Ins.AddMouseRightDownClick(ExitBuild);

        EventManager.OnEnterBuildEvent.Trigger();
    }
    /// <summary>
    /// 选择建筑位置
    /// </summary>
    private int IgnoreMask = ~(1 << 8 | 1 << 9 | 1 << 10 | 1 << 11 | 1 << 15); //主角 敌人 物品 自身 忽略单位 
    public void SelectPos(IEventMessage e = null, object[] o = null) {
        var hp = Utility.Ins.MouseMove(null, 100, IgnoreMask);
        //更新位置
        if (BuildMono == null) {
            PoolManager.Ins.ReleaseNoPool(Path, obj => {
                BuildMono = obj;
                //添加脚本
                CreateMonoInit();
                BuildMonoCom.SelectPos(hp.hit.point);
            });
        } else BuildMonoCom.SelectPos(hp.hit.point);

        if (BuildMono != null) {
            //判断是否能够建造
            if (GoodsManager.Ins.GetCurrentOnTriggerCount() == 0) {
                BuildMonoCom.SetShader(BuildShaderType.Can);
            } else {
                BuildMonoCom.SetShader(BuildShaderType.Cant);
            }
        }
    }
    /// <summary>
    /// 开始建造
    /// </summary>
    public async void BeginBuild(IEventMessage e, object[] o = null) {
        if (GoodsManager.Ins.GetCurrentOnTriggerCount() != 0) return;

        SetStateType(BuildStateType.Building);
        GoodsManager.Ins.IsBuildMove = false;
        EventManager.Ins.RemoveMouseContinuouseDetection(SelectPos);
        EventManager.Ins.RemoveMouseLeftDownClick(BeginBuild);
        EventManager.Ins.RemoveMouseRightDownClick(ExitBuild);
        EventManager.OnBeginBuildEvent.Trigger();
        var pos = Utility.Ins.MouseMove(null, 100, IgnoreMask).hit.point;
        pos = GoodsManager.Ins.FreeBuildPos(pos);
        BuildMono.transform.position = pos;
        //执行脚本
        BuildMonoCom.Begin(CreateTime);
        await UniTask.WaitForSeconds(CreateTime);
        EndBuild();
    }
    public void EndBuild() {
        BuildMonoCom.EndBuild();
        SetStateType(BuildStateType.Builded);
    }
    /// <summary>
    /// 退出建造
    /// </summary>
    public void ExitBuild(IEventMessage e = null, object[] o = null) {
        SetStateType(BuildStateType.Disable);

        GoodsManager.Ins.IsBuildMove = false;
        GoodsManager.Ins.ResetCurrentOnTriggerCount();
        BuildMonoCom.CurrentShaderState = BuildShaderType.Null;

        GameObject.Destroy(BuildMono);
        EventManager.Ins.RemoveMouseContinuouseDetection(SelectPos);
        EventManager.Ins.RemoveMouseLeftDownClick(BeginBuild);
        EventManager.Ins.RemoveMouseRightDownClick(ExitBuild);

        //退出网格状态
        BottomBuildBegin.Ins?.Hide();
    }

    #endregion

    #region 功能

    protected ConcurrentDictionary<int, ElementData> InfluenceData = new();

    public ConcurrentDictionary<int, ElementData> GetInfluenceData() => InfluenceData;
    /// <summary>
    /// 进入范围添加受本建筑影响的角色
    /// </summary>
    /// <param name="data"></param>
    public void AddInfluenceData(ElementData data) {
        if (!InfluenceData.ContainsKey(data.UID)) {
            InfluenceData.TryAdd(data.UID, data);
            Near(data.UID);
        }
    }
    /// <summary>
    /// 移除受本建筑影响的角色
    /// </summary>
    /// <param name="data"></param>
    public void RemoveInfluenceData(ElementData data) {
        if (InfluenceData.ContainsKey(data.UID)) {
            Far(data.UID);
            InfluenceData.TryRemove(data.UID, out ElementData abandonData);
        }
    }
    /// <summary>
    /// 获取最近的进入范围的角色
    /// </summary>
    /// <returns></returns>
    public ElementData GetBestNear() {
        var pos = BuildMonoCom.transform.position;
        ElementData bestNearElementData = null;
        float min = int.MaxValue;
        foreach (var item in InfluenceData) {
            var attackPos = item.Value.ElementMono.transform.position;
            var dis = Vector3.Distance(new Vector3(attackPos.x,0, attackPos.z), pos);
            if (dis < min) {
                min = dis;
                bestNearElementData = item.Value;
            }
        }
        return bestNearElementData;
    }
    /// <summary>
    /// 移除所有
    /// </summary>
    public void RemoveAllInfluenceData() {
        InfluenceData.Clear();
    }
    /// <summary>
    /// 靠近
    /// </summary>
    public virtual void Near(int id) {

    }
    /// <summary>
    /// 远处
    /// </summary>
    public virtual void Far(int id) {

    }

    public int WeaponAttack { get => MaxAttack; set { } }

    /// <summary>
    /// 用于上buff用
    /// </summary>
    /// <param name="enemy"></param>
    public virtual void InflictOther(ElementData enemy) {

    }

    #endregion
}