using UnityEngine;

public class BottomToTopClip : MonoBehaviour
{
    public Material material;
    public float clipSpeed = 1.0f;
    public float clipHeight = 0.0f;

    void Update()
    {
        // clipHeight += clipSpeed * Time.deltaTime;
        material.SetFloat("_ClipHeight", clipHeight);
    }
}