using Game;
public enum TurretBuildCreateType
{
    SteelTurret,//钢铁炮塔
}
public class TurretBuildConsumablesGoods : BuildConsumablesGoods
{
    public TurretBuildCreateType TurretBuildCreateType;
    protected SteelTurretGoodsMono SteelTurretGoodsMonoCom => BuildMonoCom as SteelTurretGoodsMono;

    public TurretBuildConsumablesGoods() { }
    public TurretBuildConsumablesGoods(TurretBuildConsumablesGoods item, object[] o = null) : base(item, o)
    {
        TurretBuildCreateType = item.TurretBuildCreateType;
    }

    public TurretBuildConsumablesGoods(TurretBuildCreateJsonData data)
    {
        ID = data.ID;
        Name = data.Name;
        Path = data.Path;
        CreateTime = data.CreateTime;
        BuildCreateType = data.BuildCreateType;
        TurretBuildCreateType = data.TurretBuildCreateType;
        BagLimitNum = data.BagLimitNum;
        BuildingCircle = data.BuildingCircle;
        EffCircle = data.EffCircle;
    }
}

public class SteelTurretBuildConsumablesGoods : TurretBuildConsumablesGoods
{
    public SteelTurretBuildConsumablesGoods(){}
    public SteelTurretBuildConsumablesGoods(TurretBuildConsumablesGoods item, object[]o=null) : base(item) { }

    public override void Init(ElementData data, object[] o = null)
    {
        base.Init(data, o);
    }

    public override void CreateMonoInit()
    {
        base.CreateMonoInit();
        BuildMonoCom = BuildMono.AddComponent<SteelTurretGoodsMono>();
        BuildMonoCom.Init(this);
    }

    public override void Near(int id)
    {
        if (!InfluenceData.ContainsKey(id)) return;
        base.Near(id);
        SteelTurretGoodsMonoCom.Fire();
    }

    public override void Far(int id)
    {
        base.Far(id);
        InfluenceData[id].RemoveGoods(GoodsManager.Ins.GetGoodsData(BuffType.ContinueHP));
        SteelTurretGoodsMonoCom.StopFire();
    }
}