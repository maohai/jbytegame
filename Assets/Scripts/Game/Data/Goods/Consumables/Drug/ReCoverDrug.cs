using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// 减少变异药剂
    /// </summary>
    public class ReCoverDrug : Drug{
        public int EffectID{ get=>ID; set{ } }
        public ReCoverDrug(Drug drug):base(drug){}
        public GoodsType EffectType{ get => GoodsType.Consumables; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }

        public void Remove(){
        }

        public void Reset(){
        }

        public void RunEffect(){
            Utility.NumClam(ref ElementData.Variation,Value,0,false);
            ElementData.RemoveGoods(this);
        }
    }

}
