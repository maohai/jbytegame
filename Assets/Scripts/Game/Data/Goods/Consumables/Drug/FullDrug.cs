using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// 饱食药水
    /// </summary>
    public class FullDrug : Drug{
        public FullDrug(Drug drug) : base(drug){}
        public override void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
            Utility.Ins.StartCoroutine(DurationCoroutine());
        }

        public override void Remove(){
            ElementData.KeepHunger = false;
        }

        public override void Reset(){
            RunEffect();
        }

        public override void RunEffect(){
            ElementData.KeepHunger = true;
            Utility.NumClam(ref ElementData.Hunger,Value,ElementData.MaxHunger);
            RemainTime = Duration;
        }

        IEnumerator DurationCoroutine(){
            while (RemainTime > 0){
                yield return new WaitForSeconds(1f);
                --RemainTime;
                Debug.Log($"还剩{RemainTime}秒结束");
            }

            ElementData.KeepHunger = false;
            ElementData.RemoveGoods(this);
        }
    }
}