using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// 虚弱药剂
    /// </summary>
    public class WeakDrug : Drug{
        public int EffectID{ get=>ID; set{ } }
        public WeakDrug(Drug drug):base(drug){}
        public GoodsType EffectType{ get => GoodsType.Consumables; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            Duration = 30;
            RemainTime = Duration;
            EffNumber = 3;
            RemainNumber = EffNumber;
            RunEffect();
            Utility.Ins.StartCoroutine(DurationCoroutine());
        }

        public void Remove(){
            int times = EffNumber - RemainNumber;
            Utility.NumClam(ref ElementData.Attack,Value * times,Int32.MaxValue);
        }

        public void Reset(){
            RunEffect();
        }

        public void RunEffect(){
            if (RemainNumber < 0) return;
            --RemainNumber;
            Utility.NumClam(ref ElementData.Attack,Value,0,false);
        }
        IEnumerator DurationCoroutine(){
            while (RemainTime > 0){
                yield return new WaitForSeconds(1f);
                --RemainTime;
            }
            ElementData.RemoveGoods(this);
        }
    }
}