using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public enum DrugType{
        Full,
        HP,
        ReCover,
        Hunger,
        Hurt,
        Variation,
        Power,
        Weak,
    }
    
    /// <summary>
    /// 药品基类
    /// </summary>
    public class Drug : ConsumablesGoods{
        public int Value;
        public DrugType DrugType;

        public Drug():base(){}

        public Drug(DrugJsonData data){
            ID = data.ID;
            Name = data.Name;
            GoodsType = data.GoodsType;
            Price = data.Price;
            Des = data.Des;
            ImgID = data.ImgID;
            Duration = data.Duration;
            EffNumber = data.EffNumber;
            DrugType = data.DrugType;
            Value = data.Value;
            BagLimitNum = data.BagLimitNum;
        }
        public Drug(Drug drug):base(drug){
            DrugType = drug.DrugType;
            Value = drug.Value;
        }
    }
}
