using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    
    /// <summary>
    /// 变异药水
    /// </summary>
    public class VariationDrug : Drug{
        public int EffectID{ get=>ID; set{ } }
        public VariationDrug(Drug drug):base(drug){}
        public GoodsType EffectType{ get => GoodsType.Consumables; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }

        public void Remove(){
        }

        public void Reset(){
            
        }

        public void RunEffect(){
            Utility.NumClam(ref ElementData.Variation,Value,ElementData.MaxVariation);
            ElementData.RemoveGoods(this);
        }
    }


}