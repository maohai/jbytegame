using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// HP药水
    /// </summary>
    public class HPDrug : Drug{
        public HPDrug(Drug drug):base(drug){}
        public override void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }
        
        public override void RunEffect(){
            ElementData.HP_pm(Value);
            ElementData.RemoveGoods(this);
        }
    }
}
