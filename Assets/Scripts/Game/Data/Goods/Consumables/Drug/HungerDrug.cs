using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// 饥饿药剂
    /// </summary>
    public class HungerDrug : Drug{
        public int EffectID{ get=>ID; set{ } }
        public HungerDrug(Drug drug) : base(drug){}
        public GoodsType EffectType{ get => GoodsType.Consumables; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
            Utility.Ins.StartCoroutine(ReduceCoroutine());
        }

        public void Remove(){
            
        }

        public void Reset(){
            RunEffect();
        }

        public void RunEffect(){
            RemainTime = Duration;
            Utility.NumClam(ref ElementData.Hunger,Value,0,false);
        }

        IEnumerator ReduceCoroutine(){
            while (RemainTime>0){
                yield return new WaitForSeconds(1f);
                --RemainTime;
                Debug.Log(ElementData.Name+"-生效-"+Name);
                Utility.NumClam(ref ElementData.Hunger,1,0,false);
            }
            ElementData.RemoveGoods(this);
        }
    }

}