using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class ConsumablesGoods : Goods{
    public ConsumablesGoods():base(){}
    public ConsumablesGoods(ConsumablesGoods con,object[]o=null):base(con,o){
        GoodsType = GoodsType.Consumables;
    }
}
