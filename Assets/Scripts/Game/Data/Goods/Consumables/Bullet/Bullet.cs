using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public enum BulletType{
    Bullet762,
    Bullet556,
    MissileK99,
}
public class Bullet : ConsumablesGoods,IAttack{
    public int Amount;//子弹数量
    public int Power;//子弹攻击力
    public BulletType BulletType;
    
    protected IAttack SelfWeapon;
    public Bullet():base(){}
    public Bullet(BulletJsonData jsonData){
        ID = jsonData.ID;
        Name = jsonData.Name;
        GoodsType = jsonData.GoodsType;
        ImgID = jsonData.ImgID;
        Power = jsonData.Power;
        BulletType = jsonData.BulletType;
        BagLimitNum = jsonData.BagLimitNum;
    }
    public Bullet(Bullet con,object[]o=null) : base(con,o){
        Amount = con.Amount;
        Power = con.Power;
        BulletType = con.BulletType;
    }

    public void AmountVAriation(int num){
        Utility.VariationAuto(ref Amount,num);
    }

    public virtual void BindWeapon(IAttack weapon){
        SelfWeapon = weapon;
    }

    public override void Used(){
        
    }

    public int WeaponAttack { get => Power; set { } }
    public virtual void InflictOther(ElementData enemy){
        var weaponAttack = SelfWeapon.WeaponAttack;
        enemy.UseGoods(GoodsManager.Ins.GetNewGoods(BuffType.ReduceHP,new object[]{-(Power+ weaponAttack) }));
    }
}
