using System;
using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponSuivivalCreateType {
    Torch,      //火把
}

public class SuivivalWeaponGoods : WeaponGoods{
    public int OnetimeUsedUp;//使用一次掉多少耐久度
    public int MaxDurability;//耐久
    public int CurrentDurability;//耐久

    public WeaponSuivivalCreateType WeaponSuivivalCreateType;
    public SuivivalWeaponGoods() { }
    public SuivivalWeaponGoods(SuivivalWeaponGoods item) : base(item) {
        OnetimeUsedUp = item.OnetimeUsedUp; 
        MaxDurability = item.MaxDurability;
        CurrentDurability = item.CurrentDurability;
        WeaponSuivivalCreateType = item.WeaponSuivivalCreateType;
    }
    public SuivivalWeaponGoods(WeaponSuivivalCreateJsonData Item)
    {
        ID = Item.ID;
        Name = Item.Name;
        WeaponSuivivalCreateType = Item.WeaponSuivivalCreateType;
        BaseWeaponAttack = Item.BaseWeaponAttack;
        MaxDurability = Item.MaxDurability;
        BagLimitNum = Item.BagLimitNum;
        GoodsType = Item.GoodsType;
        WeaponType = Item.WeaponType;
    }
}

public class TorchSuivivalWeaponGoods : SuivivalWeaponGoods
{
    public TorchSuivivalWeaponGoods() { }
    public TorchSuivivalWeaponGoods(SuivivalWeaponGoods item, object[]o=null) : base(item) { }
    public override void Equip(Transform trans, Action<GameObject> cb = null){
        Debug.Log("加载武器:火把");
        PrefabWeapon = ResManager.Ins.LoadPrefab("Item/Goods/Weapon/Suivival/Torch");
        
        base.Equip(trans, obj => {
            weaponMono = obj.AddComponent<TorchSuivivalWeaponMono>();
            weaponMono.BindData(this);
            cb?.Invoke(null);
        });
    }
    public override WeaponGoods GetWeaponGoods(){
        return this;
    }
    public override void GenerateCreate()
    {
        base.GenerateCreate();
        BagManager.Ins.AddBagGoods(this);
    }
    
}
