using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class Biology : Goods{
    public Biology(){}
    public Biology(Biology bio,object[] o = null): base(bio,o){
        GoodsType = GoodsType.Biology;
    }
}

public class PlantBiology : Biology{
    public PlantBiologyType PlantBiologyType;
    public int Value;
    public PlantBiology(){}

    public PlantBiology(PlantJsonData data){
        ID = data.ID;
        Name = data.Name;
        GoodsType = data.GoodsType;
        Des = data.Des;
        ImgID = data.ImgID;
        Duration = data.Duration;
        EffNumber = data.EffNumber;
        PlantBiologyType = data.PlantBiologyType;
        Value = data.Value;
        BagLimitNum = data.BagLimitNum;
    }
    public PlantBiology(PlantBiology plant, object[] o = null) : base(plant, o){
        PlantBiologyType = plant.PlantBiologyType;
        Value = plant.Value;
    }
}

public enum PlantBiologyType{
    TreeWood,
    Grass
}
public class TreeWoodPlant : PlantBiology{
    public TreeWoodPlant(PlantBiology plant, object[] o = null) : base(plant, o){ }
    public override void Init(ElementData data, object[] o = null){
        ElementData = data;
        Debug.Log("我是树");
    }

    public override void Remove(){
        Debug.Log("我是树 被当做材料使用了");
    }
}
public class GrassPlant:PlantBiology{
    public GrassPlant(PlantBiology plant, object[] o = null) : base(plant, o){ }
    public override void Init(ElementData data, object[] o = null){
        ElementData = data;
        RunEffect();
    }

    public override void Remove(){
        Debug.Log("我是草 被当做材料使用了");
    }

    public override void RunEffect(){
        Debug.Log("我是草");
        ElementData.RemoveGoods(this);
    }
}