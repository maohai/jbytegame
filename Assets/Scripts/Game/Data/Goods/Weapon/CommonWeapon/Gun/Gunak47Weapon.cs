using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class Gunak47Weapon : GunWeaponGoods{
    public Gunak47Weapon(GunWeaponGoods goods, object[] o = null) : base(goods, o){ }

    public override void Equip(Transform trans, Action<GameObject> cb = null){
        Debug.Log("加载武器:ak47");
        PrefabWeapon = ResManager.Ins.LoadPrefab("Item/Goods/Weapon/Gun/Gunak47");
        
        base.Equip(trans, obj => {
            weaponMono = obj.AddComponent<GunAK47WeaponMono>();
            weaponMono.BindData(this);
            cb?.Invoke(null);
        });
    }

    public override WeaponGoods GetWeaponGoods(){
        return this;
    }
    protected override void Fire(IEventMessage e = null,object[]o=null){
        if (CurrentBulletLoadAmount <= 0 || PlayerStateManager.Ins.IsReload || PlayerStateManager.Ins.CurrentFireCD > 0) return;
        PlayerStateManager.Ins.SetArmFireCD(FireInterval);
        // 开火
        PlayerStateManager.Ins.SwitchArmShootFire();
        --CurrentBulletLoadAmount;
        PoolManager.Ins.Release("Item/GoodsUse/Consumables/Bullet/MissileK99", weaponMono.transform.position ,obj => {
            if (obj.TryGetComponent(out BulletMissileK99UseMono mono) ||(mono = obj.AddComponent<BulletMissileK99UseMono>())){
                mono.BindData(GoodsManager.Ins.GetNewGoods(BulletType));
                mono.GoodsData.BindWeapon(this);
            }
        },30);
        
        base.Fire(e,o);
    }

    public override void LoadBullet(IEventMessage e = null,object[]o=null){
        int num = 0;
        int needAdd = BaseBulletLoadAmount - CurrentBulletLoadAmount;
        Goods goods = GoodsManager.Ins.GetNewGoods(BulletType);
        //查询快捷背包子弹数量
        num += BagManager.Ins.GetNumByGoods(goods);
        if (num == 0) return;
        if (num < needAdd)
            //查询背包子弹数量
            num += BagManager.Ins.GetNumByGoods(goods,BagItemType.Bag);
        //装弹
        CurrentBulletLoadAmount += num > needAdd ? needAdd : num;
        //移除数量
        BagManager.Ins.RemoveUsedGoods(goods,CurrentBulletLoadAmount);
        
        //刷新UI
        base.LoadBullet(e,o);
    }
}
