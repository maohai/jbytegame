using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UIElements;

public enum GunWeaponType {
    Gunak47,
}
public class GunWeaponGoods : WeaponGoods {
    public GunWeaponType GunWeaponType;
    public BulletType BulletType;
    protected BulletUseMono BulletUseMono;

    public int BaseBulletLoadAmount;//能够装入的弹夹数量
    public int CurrentBulletLoadAmount;//当前子弹数量
    public float FireInterval;//开火时间间隔
    public GunWeaponGoods(GunWeaponJsonData data) {
        ID = data.ID;
        Name = data.Name;
        GoodsType = data.GoodsType;
        Source = data.Source;
        Price = data.Price;
        Des = data.Des;
        ImgID = data.ImgID;
        WeaponType = data.WeaponType;
        GunWeaponType = data.GunWeaponType;
        BulletType = data.BulletType;
        BaseWeaponAttack = data.BaseWeaponAttack;
        BaseBulletLoadAmount = data.BaseBulletLoadAmount;
        FireInterval = data.FireInterval;
        BagLimitNum = data.BagLimitNum;
    }
    public GunWeaponGoods(GunWeaponGoods goods, object[] o = null) : base(goods) {
        GunWeaponType = goods.GunWeaponType;
        BulletType = goods.BulletType;
        BaseBulletLoadAmount = goods.BaseBulletLoadAmount;
        FireInterval = goods.FireInterval;
    }

    public override void Equip(Transform trans, Action<GameObject> cb = null) {
        base.Equip(trans, cb);
        EventManager.Ins.AddMouseRightDownClick(EnterArm);
        EventManager.Ins.AddMouseRightUpClick(ExitArm);
        EventManager.Ins.AddMouseLeftDownClick(Fire);
        EventManager.Ins.AddKeyRDownClick(LoadBullet);
        EventManager.OnEquitWeapon.Trigger();
        PlayerController.Ins.OpenIK();
        PlayerController.Ins.SetIsCanHoldGun(true);
    }

    public override void Disequip() {
        base.Disequip();
        EventManager.Ins.RemoveMouseRightDownClick(EnterArm);
        EventManager.Ins.RemoveMouseRightUpClick(ExitArm);
        EventManager.Ins.RemoveMouseLeftDownClick(Fire);
        EventManager.Ins.RemoveKeyRDownClick(LoadBullet);
        EventManager.OnEquitWeapon.Trigger();
        PlayerController.Ins.SetIsCanHoldGun(false);
        PlayerController.Ins.CloseIK();
    }

    /// <summary>
    /// 进入瞄准状态
    /// </summary>
    /// <param name="e"></param>
    /// <param name="o"></param>
    protected virtual void EnterArm(IEventMessage e = null, object[] o = null) {
        //Debug.Log("武器进行瞄准");
        //todo 切换到瞄准动画
    }
    /// <summary>
    /// 退出瞄准状态
    /// </summary>
    /// <param name="e"></param>
    /// <param name="o"></param>
    protected virtual void ExitArm(IEventMessage e = null, object[] o = null) {
        //Debug.Log("武器退出瞄准");
    }

    protected virtual void Fire(IEventMessage e = null, object[] o = null) {
        Debug.Log("Fire");
        EventManager.OnRefreshWeapon.Trigger();
    }

    public virtual void LoadBullet(IEventMessage e = null, object[] o = null) {
        EventManager.OnRefreshWeapon.Trigger();
    }
}
