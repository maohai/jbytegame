using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public enum ColdWeaponType{
   Club,
   WoodShield,
}
public class ColdWeaponGoods : WeaponGoods{
   public ColdWeaponType ColdWeaponType;

   public ColdWeaponGoods(ColdWeaponJsonData jsonData){
        ID = jsonData.ID;
        Name = jsonData.Name;
        GoodsType = jsonData.GoodsType;
        Source = jsonData.Source;
        Price = jsonData.Price;
        Des = jsonData.Des;
        ImgID = jsonData.ImgID;
        WeaponType = jsonData.WeaponType;
        ColdWeaponType = jsonData.ColdWeaponType;
        BaseWeaponAttack = jsonData.BaseWeaponAttack;
        BagLimitNum = jsonData.BagLimitNum;
   }

   public ColdWeaponGoods(ColdWeaponGoods goods, object[] o = null) : base(goods){
      ColdWeaponType = goods.ColdWeaponType;
   }

}
