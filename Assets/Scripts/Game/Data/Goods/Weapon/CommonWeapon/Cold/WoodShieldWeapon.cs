using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class WoodShieldWeapon : ColdWeaponGoods
{
    public WoodShieldWeapon(ColdWeaponGoods goods, object[] o = null) : base(goods, o){ }

    public override void Equip(Transform trans, Action<GameObject> cb = null){
        Debug.Log("加载武器:木墩");
        PrefabWeapon = ResManager.Ins.LoadPrefab("Item/Goods/Weapon/Cold/WoodShield");

        base.Equip(trans, obj => {
            weaponMono = obj.AddComponent<WoodShieldWeaponMono>();
            weaponMono.BindData(this);
            cb?.Invoke(null);
        });
        
    }

    #region 接口
    
    public override void Init(ElementData data, object[] o = null){
        ElementData = data;
    }

    public override void Remove(){
        Utility.NumClam(ref ElementData.Defense,200,0,false);
        Debug.Log($"去掉木墩了,当前防御力是{ElementData.Defense}");
    }

    public override void Reset(){ }

    public override void RunEffect(){
        Utility.NumClam(ref ElementData.Defense,200,Int32.MaxValue);
        Debug.Log($"装备木墩了,当前防御力是{ElementData.Defense}");
    }

    public override WeaponGoods GetWeaponGoods(){
        return this;
    }

    public override void Calculate(){
        ElementData.Defense += 200;
    }

    public override void InflictOther(ElementData enemy){
        
    }

    #endregion
}
