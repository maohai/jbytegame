using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class ClubWeapon : ColdWeaponGoods{
    public ClubWeapon(ColdWeaponGoods goods, object[] o = null) : base(goods, o){
      
    }

    public override void Equip(Transform trans, Action<GameObject> cb = null){
        Debug.Log("加载武器:木棒");
        PrefabWeapon = ResManager.Ins.LoadPrefab("Item/Goods/Weapon/Cold/Club");
        
        base.Equip(trans, obj => {
            weaponMono = obj.AddComponent<ClubWeaponMono>();
            weaponMono.BindData(this);
            cb?.Invoke(null);
        });
    }

    #region 接口

    public override void Init(ElementData data, object[] o = null){
        ElementData = data;
        RunEffect();
    }

    public override void Remove(){
        RunEffect();
    }

    public override void RunEffect(){
        ElementData.CalculateAttack();
        Debug.Log($"当前攻击是{ElementData.Attack}");
    }

    public override WeaponGoods GetWeaponGoods(){
        return this;
    }

    public override void Calculate(){
        Utility.NumClam(ref ElementData.Attack,20,Int32.MaxValue);
    }

    /// <summary>
    /// 敌人在这里添加效果
    /// </summary>
    /// <param name="enemy"></param>
    public override void InflictOther(ElementData enemy){
        enemy.UseGoods(GoodsManager.Ins.GetNewGoods(BuffType.ReduceHP,new object[]{-30}));
    }

    #endregion
    
}
