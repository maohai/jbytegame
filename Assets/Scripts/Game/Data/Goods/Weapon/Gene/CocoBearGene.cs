using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// kekedou
    /// </summary>
    public class CocoBearGene : GeneWeaponGoods{
        public int EffectID{ get=>ID; set{ } }
        public GoodsType EffectType{ get => GoodsType.Weapon; set{ } }
        public CocoBearGene(GeneWeaponGoods gene):base(gene){}
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }

        public void Remove(){
            RunEffect();
        }

        public void Reset(){
        }

        public void RunEffect(){
            ElementData.CalculateAttack();
        }

        public WeaponGoods GetWeaponGoods(){
            return this;
        }

        public void Calculate(){
            if (IsAddCalculate){
                ElementData.Attack += (int)AttackFactor;
            }
            else{
                var temp = ElementData.Attack * AttackFactor;
                ElementData.Attack = (int)temp;
            }
        }

        public void InflictOther(ElementData enemy){
            
        }
    }
}