using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game{

    /// <summary>
    /// 基因基类
    /// </summary>
    public class GeneWeaponGoods : WeaponGoods{
        public GeneType GeneType;
        public GeneSiteType GeneSizeType;
        public int Level;//等阶
        public string StrGeneNum;//等阶
        public int GeneNum;//基因所需数量
        public GeneWeaponGoods(){}

        public GeneWeaponGoods(GeneWeaponGoods gene,object[] o = null) : base(gene,o){
            GeneType = gene.GeneType;
            GeneSizeType = gene.GeneSizeType;
            Level = gene.Level;
            StrGeneNum = gene.StrGeneNum;
            GeneNum = gene.GeneNum;
        }
    }

    public enum GeneSiteType{
        Head,
        Arm
    }
    public enum GeneType{
        CocoBear,
        Rattlesnake,
        Bear,
        Leopard,
        maoerduo,
        maoweiba,
        maozhua,
        gou,
        tu,
        Butterfly,
        sharenfeng,
        maomaochong,
        mayi,
        guoyin,
        lu,
        jiweiniao,
        ying,
        bailingniao,
        wuya,
        xintianwen,
        he,
        tiane,
        jing,
        e,
        jijuxie,
        shuwa,
        fancheyu,
        linxia,
        shadingyu,
        shuimu,
        dongxingban,
        wenzi,
    }
}