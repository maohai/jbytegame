using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    /// <summary>
    /// 熊
    /// </summary>
    public class BearGene : GeneWeaponGoods{
        public int EffectID{ get=>ID; set{ } }
        public GoodsType EffectType{ get => GoodsType.Special; set{ } }
        public BearGene(GeneWeaponGoods gene, object[] o = null) : base(gene, o){
            defenceFactor = (float) o[0];
        }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
        }

        public void Remove(){
            
        }

        public void Reset(){
        }

        public void RunEffect(){
        }

        public WeaponGoods GetWeaponGoods(){
            return this;
        }

        public void Calculate(){
            
        }

        private float defenceFactor;
        public void InflictOther(ElementData enemy){
            enemy.ReduceDefenceFactor = defenceFactor;
            enemy.UseGoods(new ReduceDefenseEffect());
        }
    }
}