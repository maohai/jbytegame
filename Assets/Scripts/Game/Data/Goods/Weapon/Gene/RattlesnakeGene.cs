using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class RattlesnakeGene : GeneWeaponGoods{
        public GoodsType EffectType{ get => GoodsType.Special; set{ } }
        public int EffectID{ get=>ID; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }

        public void Remove(){
            RunEffect();
            ElementData.IsVenom = false;
        }

        public void Reset(){
            
        }

        public void RunEffect(){
            ElementData.IsVenom = true;
            ElementData.CalculateAttack();
        }

        public WeaponGoods GetWeaponGoods(){
            return this;
        }

        public void Calculate(){
            if (IsAddCalculate){
                ElementData.Attack += (int)AttackFactor;
            }
            else{
                var temp = ElementData.Attack * AttackFactor;
                ElementData.Attack = (int)temp;
            }
        }

        public void InflictOther(ElementData enemy){
            enemy.UseGoods(new ReduceDefenseEffect());
        }

        public RattlesnakeGene(GeneWeaponGoods g):base(g){
            
        }

    }
}
