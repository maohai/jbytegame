using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class ButterflyGene : GeneWeaponGoods{
        public int EffectID{ get=>ID; set{ } }
        public GoodsType EffectType{ get => GoodsType.Weapon; set{ } }
        public ButterflyGene(GeneWeaponGoods gene):base(gene){}
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
        }

        public void Remove(){
            
        }

        public void Reset(){
            
        }

        public void RunEffect(){
            
        }

        public WeaponGoods GetWeaponGoods(){
            return this;
        }

        public void Calculate(){
            
        }
        
        public void InflictOther(ElementData enemy){
            enemy.UseGoods(new DizzeEffect());
        }
    }
}