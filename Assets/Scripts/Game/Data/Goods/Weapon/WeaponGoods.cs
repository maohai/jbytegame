using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public interface IAttack {
        public void InflictOther(ElementData enemy);
        public int WeaponAttack { get; set; }
    }
    public enum WeaponType {
        Cold,
        Gun
    }

    public enum WeaponKindType {
        Head,     //头盔
        Ear,      //耳坠
        Necklace, //项链
        Mask,     //面具
        Cloak,    //披风
        HandLeft, //左手武器
        HandRight,//右手武器
        Hand,     //手套
        Jecked,   //上衣
        Belt,     //腰带
        Trouser,  //裤子
        Shoe,     //鞋子
    }
    public class WeaponGoods : Goods, IAttack {
        public int BaseWeaponAttack;
        public bool IsADC;//是否为远程
        public bool IsAddCalculate;//是否为加算
        public float AttackFactor;//攻击计算因子
        public float HPFactor;//血量计算因子
        public WeaponType WeaponType;
        public WeaponKindType WeaponKindType;//武器装备类型
        /// <summary>
        /// 装备读取预制体
        /// </summary>
        protected GameObject PrefabWeapon;
        /// <summary>
        /// 挂在在武器上的脚本
        /// </summary>
        protected BaseWeaponMono weaponMono;
        protected GameObject ObjWeapon;
        public WeaponGoods() { }

        public WeaponGoods(WeaponGoods goods, object[] o = null) : base(goods) {
            BaseWeaponAttack = goods.BaseWeaponAttack;
            IsADC = goods.IsADC;
            IsAddCalculate = goods.IsAddCalculate;
            AttackFactor = goods.AttackFactor;
            HPFactor = goods.HPFactor;
            PrefabWeapon = goods.PrefabWeapon;
            weaponMono = goods.weaponMono;
            ObjWeapon = goods.ObjWeapon;

            WeaponKindType = WeaponKindType.HandLeft;
            GoodsType = GoodsType.Weapon;
        }

        /// <summary>
        /// 装备效果生效
        /// </summary>
        public void EquitEff() {
            Used();
        }
        /// <summary>
        /// 装备效果失效
        /// </summary>
        public void DisequitEff() {
            Nonuse();
        }

        /// <summary>
        /// 装备武器
        /// </summary>
        public virtual void Equip(Transform trans, Action<GameObject> cb = null) {
            Disequip();
            ObjWeapon = GameObject.Instantiate(PrefabWeapon, trans);
            cb?.Invoke(ObjWeapon);
        }
        /// <summary>
        /// 去掉装备
        /// </summary>
        public virtual void Disequip() {
            if (ObjWeapon != null) {
                GameObject.Destroy(ObjWeapon);
                ObjWeapon = null;
            }
        }

        #region 接口

        public virtual WeaponGoods GetWeaponGoods() {
            return this;
        }

        public virtual void Calculate() { }

        public int WeaponAttack { get => BaseWeaponAttack; set { } }
        public virtual void InflictOther(ElementData enemy) {

        }

        #endregion
    }
}