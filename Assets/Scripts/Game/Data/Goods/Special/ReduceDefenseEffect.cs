using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class ReduceDefenseEffect : SpecialGoods{
        public GoodsType EffectType{ get => GoodsType.Special; set{ } }
        public int EffectID{ get=>ID; set{ } }

        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RunEffect();
        }

        public void Remove(){
            ElementData.IsReduceDefence = false;
            ElementData.Defense = ElementData.BaseDefense;
        }

        public void Reset(){
            
        }

        public void RunEffect(){
            ElementData.IsReduceDefence = true;
            if (ElementData.ReduceDefenceFactor < 1){
                Utility.NumClam(ref ElementData.Defense,(int)(ElementData.Defense*ElementData.ReduceDefenceFactor),0,false);
            }else Utility.NumClam(ref ElementData.Defense,(int)ElementData.ReduceDefenceFactor,0,false);
        }
    }

}