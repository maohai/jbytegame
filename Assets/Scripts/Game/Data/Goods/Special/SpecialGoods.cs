using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class SpecialGoods : Goods
    {
        public SpecialGoods(){}
        public SpecialGoods(Goods goods, object[] o = null):base(goods,o){
            GoodsType = GoodsType.Special;
        }
    }

    public enum BuffType{
        ReduceHP,
        ContinueHP,
    }
    public class Buff : SpecialGoods{
        public int Value;
        public BuffType BuffType;
        public Buff():base(){}

        public Buff(SpecialJsonData data){
            ID = data.ID;
            Name = data.Name;
            GoodsType = data.GoodsType;
            Des = data.Des;
            ImgID = data.ImgID;
            Duration = data.Duration;
            EffNumber = data.EffNumber;
            BuffType = data.BuffType;
            Value = data.Value;
        }
        public Buff(Buff buff,object[]o=null):base(buff,o){
            BuffType = buff.BuffType;
            Value = buff.Value;
        }
    }

    public class ContinueHP : Buff{
        public ContinueHP(Buff buff,object[] o = null):base(buff,o){
            if (o != null && o.Length > 0)
            {
                value = (int)o[0];
            }
            else value = 0;
        }
        private int value;
        private Coroutine _coroutine;
        public override void Init(ElementData role, object[] o = null){
            ElementData = role;
            if (o != null && o.Length > 0)
            {
                value = (int)o[0];
            }
            else value = 0;
            RunEffect();
            if(_coroutine != null)
                Utility.Ins.StopCoroutine(_coroutine);
            _coroutine = Utility.Ins.StartCoroutine(ReduceCoroutine());
        }

        public override void Remove(){
            base.Remove();
            if(_coroutine != null)
                Utility.Ins.StopCoroutine(_coroutine);
        }

        IEnumerator ReduceCoroutine(){
            while (true){
                yield return new WaitForSeconds(1f);
                ElementData.HP_pm(value);
            }
        }
    }

}