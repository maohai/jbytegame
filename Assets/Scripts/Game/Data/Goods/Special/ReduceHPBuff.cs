using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class ReduceHPBuff : Buff{
        public ReduceHPBuff(Buff buff,object[] o = null):base(buff,o){
            if (o != null && o.Length > 0) { 
                value = (int) o[0];
            }else value = 0;
        }
        private int value; 
        
        public override void Init(ElementData role, object[] o = null){
            ElementData = role;
            if (o != null && o.Length > 0)
            {
                value = (int)o[0];
            }
            else value = 0;
            RunEffect();
        }

        public override void RunEffect(){
            ElementData.HP_pm(value);
            ElementData.RemoveGoods(this);
        }
    }

}