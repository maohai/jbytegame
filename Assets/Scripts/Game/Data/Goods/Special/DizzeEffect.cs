using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class DizzeEffect :SpecialGoods{
        public GoodsType EffectType{ get => GoodsType.Special; set{ } }
        public int EffectID{ get=>ID; set{ } }
        public void Init(ElementData data, object[] o = null){
            ElementData = data;
            RemainTime = Duration;
            RunEffect();
            Utility.Ins.StartCoroutine(RunCoroutine());
        }

        public void Remove(){
            ElementData.IsDizze = false;
        }

        public void Reset(){
            RunEffect();
        }

        public void RunEffect(){
            var flag = Random.Range(0, 4) == 0;
            if (flag){
                ElementData.IsDizze = true;
                RemainTime = 3;
            }
        }

        IEnumerator RunCoroutine(){
            while (RemainTime > 0){
                yield return new WaitForSeconds(1f);
                --RemainTime;
            }
            ElementData.RemoveGoods(this);
        }
    }

}