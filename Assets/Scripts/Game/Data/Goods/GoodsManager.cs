using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class RefreshBagMessage : Singleton<RefreshBagMessage>, IEventMessage {
    private object[] o = new object[1];
    public void SendMessage(Goods goods) {
        o[0] = goods;
        UniEvent.SendMessage(Ins, o);
    }
}

public class GoodsManager : Singleton<GoodsManager> {
    #region 初始化和获取数据

    #region 数据初始化
    private Dictionary<int, Goods> ItemDatas = new();
    private Dictionary<int, object> ItemID2TypeSelf = new();
    //近战武器数据
    private Dictionary<ColdWeaponType, ColdWeaponGoods> ColdWeaponDatas = new();
    //枪械数据
    private Dictionary<GunWeaponType, GunWeaponGoods> GunWeaponDatas = new();
    //子弹数据
    private Dictionary<BulletType, Bullet> BulletDatas = new();
    //基因数据
    private Dictionary<GeneType, GeneWeaponGoods> GeneDatas = new();
    //药品数据
    private Dictionary<DrugType, Drug> DrugDatas = new();
    //效果数据
    private Dictionary<BuffType, Buff> BuffDatas = new();
    //植物数据
    private Dictionary<PlantBiologyType,PlantBiology> PlantDatas= new ();

    #region 继承Build

    //生存装备数据
    private Dictionary<WeaponSuivivalCreateType, SuivivalWeaponGoods> WeaponSuivivalCreateData = new();
    //生存建筑数据
    private Dictionary<LifeBuildCreateType, LifeBuildConsumablesGoods> LifeBuildCreateData = new();
    //炮塔
    private Dictionary<TurretBuildCreateType,TurretBuildConsumablesGoods> TurretBuildCreateData = new();

    #endregion

    public override void Init() {
        //武器初始化
        var coldWeaponList = Client.Ins.Data.List2Dic<ColdWeaponType, ColdWeaponJsonData>("ColdWeaponType");
        foreach (var coldWeapon in coldWeaponList) {
            var g = GetNewColdWeapon(new ColdWeaponGoods(coldWeapon.Value)) as ColdWeaponGoods;
            ColdWeaponDatas.Add(g.ColdWeaponType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.ColdWeaponType);
        }
        var gunWeaponList = Client.Ins.Data.List2Dic<GunWeaponType,GunWeaponJsonData>("GunWeaponType");
        foreach (var gunWeapon in gunWeaponList) {
            var g = GetNewGunWeapon(new GunWeaponGoods(gunWeapon.Value)) as GunWeaponGoods;
            GunWeaponDatas.Add(g.GunWeaponType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.GunWeaponType);
        }
        var bulletList = Client.Ins.Data.List2Dic<BulletType,BulletJsonData>("BulletType");
        foreach (var bullet in bulletList) {
            var g = GetNewBullet(new Bullet(bullet.Value)) as Bullet;
            BulletDatas.Add(g.BulletType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.BulletType);
        }
        //药品初始化
        var drugList =  Client.Ins.Data.List2Dic<DrugType,DrugJsonData>("DrugType");
        foreach (var drug in drugList) {
            var g = GetNewDrug(new Drug(drug.Value)) as Drug;
            DrugDatas.Add(g.DrugType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.DrugType);
        }
        //基因初始化
        // var geneList = Client.Ins.Data.List2Dic<GeneType,GeneWeaponGoods>("GeneType");
        // foreach (var gene in geneList){
        //     var g = new GeneWeaponGoods(drug.Value);
        //     GeneDatas.Add(gene.GeneType,gene);
        //     ItemDatas.Add(gene.ID,gene);
        //     ItemID2TypeSelf.Add(gene.ID,gene.GeneType);
        // }
        //buff初始化
        var buffList = Client.Ins.Data.List2Dic<BuffType,SpecialJsonData>("BuffType");
        foreach (var buff in buffList) {
            var g = GetNewBuff(new Buff(buff.Value)) as Buff;
            BuffDatas.Add(g.BuffType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.BuffType);
        }
        //材料初始化
        var plantList =  Client.Ins.Data.List2Dic<PlantBiologyType,PlantJsonData>("PlantBiologyType");
        foreach (var plant in plantList) {
            var g = GetNewPlant(new PlantBiology(plant.Value)) as PlantBiology;
            PlantDatas.Add(g.PlantBiologyType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.PlantBiologyType);
        }
        //生存装备初始化
        var weaponSuivivalCreateList = Client.Ins.Data.List2Dic<WeaponSuivivalCreateType, WeaponSuivivalCreateJsonData>("WeaponSuivivalCreateType");
        foreach (var weaponSuivival in weaponSuivivalCreateList) {
            var g = GetNewWeaponSuivivalCreate(new SuivivalWeaponGoods(weaponSuivival.Value)) as SuivivalWeaponGoods;
            WeaponSuivivalCreateData.Add(g.WeaponSuivivalCreateType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.WeaponSuivivalCreateType);
        }
        //生存建筑初始化
        var lifeBuildCreateList = Client.Ins.Data.List2Dic<LifeBuildCreateType,LifeBuildCreateJsonData>("LifeBuildCreateType");
        foreach (var lifeBuild in lifeBuildCreateList) {
            var g = GetNewLifeBuildCreate(new LifeBuildConsumablesGoods(lifeBuild.Value)) as LifeBuildConsumablesGoods ;
            LifeBuildCreateData.Add(g.LifeBuildCreateType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.LifeBuildCreateType);
        }
        //炮塔初始化
        var turretList = Client.Ins.Data.List2Dic<TurretBuildCreateType, TurretBuildCreateJsonData>("TurretBuildCreateType");
        foreach (var item in turretList) {
            var g = GetNewTurretBuildGoods(new TurretBuildConsumablesGoods(item.Value)) as TurretBuildConsumablesGoods;
            TurretBuildCreateData.Add(g.TurretBuildCreateType, g);
            ItemDatas.Add(g.ID, g);
            ItemID2TypeSelf.Add(g.ID, g.TurretBuildCreateType);
        }

        Debug.Log("物品初始化完成");

        var mergeList = Client.Ins.Data.List2Dic<MergeType, MergeJsonData>("MergeType");
        foreach (var merge in mergeList) {
            GoodsMergeDic.Add(merge.Key, new GoodsMerge(merge.Value));
        }
        Debug.Log("合成初始化完成");
    }

    #endregion

    #region 合成路径

    public Dictionary<MergeType, GoodsMerge> GoodsMergeDic = new();

    #endregion

    #region 武器数据

    /// <summary>
    /// 获取近战武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private ColdWeaponGoods GetColdWeaponData(ColdWeaponType t) {
        if (ColdWeaponDatas.ContainsKey(t))
            return ColdWeaponDatas[t];
        return null;
    }
    /// <summary>
    /// 创建近战武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewColdWeapon(ColdWeaponType t, object[] o = null) {
        switch (t) {
            case ColdWeaponType.Club: return new ClubWeapon(GetColdWeaponData(t), o);
            case ColdWeaponType.WoodShield: return new WoodShieldWeapon(GetColdWeaponData(t), o);
        }
        return null;
    }
    private Goods GetNewColdWeapon(ColdWeaponGoods goods, object[] o = null) {
        switch (goods.ColdWeaponType) {
            case ColdWeaponType.Club: return new ClubWeapon(goods, o);
            case ColdWeaponType.WoodShield: return new WoodShieldWeapon(goods, o);
        }
        return null;
    }

    /// <summary>
    /// 获取远程武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private GunWeaponGoods GetGunWeaponData(GunWeaponType t) {
        if (GunWeaponDatas.ContainsKey(t))
            return GunWeaponDatas[t];
        return null;
    }
    /// <summary>
    /// 创建远程武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewGunWeapon(GunWeaponType t, object[] o = null) {
        switch (t) {
            case GunWeaponType.Gunak47: return new Gunak47Weapon(GetGunWeaponData(t), o);
        }
        return null;
    }
    private Goods GetNewGunWeapon(GunWeaponGoods goods, object[] o = null) {
        switch (goods.GunWeaponType) {
            case GunWeaponType.Gunak47: return new Gunak47Weapon(goods, o);
        }
        return null;
    }

    #endregion

    #region 子弹数据

    /// <summary>
    /// 获取武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Bullet GetBulletData(BulletType t) {
        if (BulletDatas.ContainsKey(t))
            return BulletDatas[t];
        return null;
    }
    /// <summary>
    /// 创建武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewBullet(BulletType t, object[] o = null) {
        switch (t) {
            case BulletType.Bullet762: return new Bullet762mm(GetBulletData(t), o);
            case BulletType.Bullet556: return new Bullet556mm(GetBulletData(t), o);
            case BulletType.MissileK99: return new MissileK99(GetBulletData(t), o);
        }
        return null;
    }
    private Goods GetNewBullet(Bullet goods, object[] o = null) {
        switch (goods.BulletType) {
            case BulletType.Bullet762: return new Bullet762mm(goods, o);
            case BulletType.Bullet556: return new Bullet556mm(goods, o);
            case BulletType.MissileK99: return new MissileK99(goods, o);
        }
        return null;
    }

    #endregion

    #region 基因数据

    /// <summary>
    /// 获取基因数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private GeneWeaponGoods GetGeneData(GeneType t) {
        if (GeneDatas.ContainsKey(t))
            return GeneDatas[t];
        return null;
    }
    private Goods GetNewGene(GeneType t, object[] o = null) {
        switch (t) {
            case GeneType.CocoBear: return new CocoBearGene(GetGeneData(t));
            case GeneType.Rattlesnake: return new RattlesnakeGene(GetGeneData(t));
            case GeneType.Bear: return new BearGene(GetGeneData(t), new[] { (object)0.5f });
            case GeneType.Leopard: return new LeopardGene(GetGeneData(t));
            case GeneType.Butterfly: return new ButterflyGene(GetGeneData(t));
        }
        return null;
    }

    #endregion

    #region 药品数据

    /// <summary>
    /// 获取药品数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Drug GetDrugData(DrugType t) {
        if (DrugDatas.ContainsKey(t))
            return DrugDatas[t];
        return null;
    }
    /// <summary>
    /// 创建物品数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewDrug(DrugType t, object[] o = null) {
        switch (t) {
            case DrugType.Full: return new FullDrug(GetDrugData(t));
            case DrugType.Hunger: return new HungerDrug(GetDrugData(t));
            case DrugType.Hurt: return new HurtDrug(GetDrugData(t));
            case DrugType.Power: return new PowerDrug(GetDrugData(t));
            case DrugType.Variation: return new VariationDrug(GetDrugData(t));
            case DrugType.Weak: return new WeakDrug(GetDrugData(t));
            case DrugType.HP: return new HPDrug(GetDrugData(t));
            case DrugType.ReCover: return new ReCoverDrug(GetDrugData(t));
        }
        return null;
    }
    private Goods GetNewDrug(Drug goods, object[] o = null) {
        switch (goods.DrugType) {
            case DrugType.Full: return new FullDrug(goods);
            case DrugType.Hunger: return new HungerDrug(goods);
            case DrugType.Hurt: return new HurtDrug(goods);
            case DrugType.Power: return new PowerDrug(goods);
            case DrugType.Variation: return new VariationDrug(goods);
            case DrugType.Weak: return new WeakDrug(goods);
            case DrugType.HP: return new HPDrug(goods);
            case DrugType.ReCover: return new ReCoverDrug(goods);
        }
        return null;
    }

    #endregion

    #region buff数据

    /// <summary>
    /// 获取buff数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Buff GetBuffData(BuffType t) {
        if (BuffDatas.ContainsKey(t))
            return BuffDatas[t];
        return null;
    }
    /// <summary>
    /// 创建buff数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewBuff(BuffType t, object[] o = null) {
        switch (t) {
            case BuffType.ReduceHP: return new ReduceHPBuff(GetBuffData(t), o);
            case BuffType.ContinueHP: return new ContinueHP(GetBuffData(t), o);
        }
        return null;
    }
    private Goods GetNewBuff(Buff goods, object[] o = null) {
        switch (goods.BuffType) {
            case BuffType.ReduceHP: return new ReduceHPBuff(goods, o);
            case BuffType.ContinueHP: return new ContinueHP(goods, o);
        }
        return null;
    }

    #endregion

    #region 植物数据

    /// <summary>
    /// 获取plant数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private PlantBiology GetPlantData(PlantBiologyType t) {
        if (PlantDatas.ContainsKey(t))
            return PlantDatas[t];
        return null;
    }
    /// <summary>
    /// 创建plant数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewPlant(PlantBiologyType t, object[] o = null) {
        switch (t) {
            case PlantBiologyType.TreeWood: return new TreeWoodPlant(GetPlantData(t), o);
            case PlantBiologyType.Grass: return new GrassPlant(GetPlantData(t), o);
        }
        return null;
    }
    private Goods GetNewPlant(PlantBiology goods, object[] o = null) {
        switch (goods.PlantBiologyType) {
            case PlantBiologyType.TreeWood: return new TreeWoodPlant(goods, o);
            case PlantBiologyType.Grass: return new GrassPlant(goods, o);
        }
        return null;
    }

    #endregion

    #region 获取生存装备数据

    /// <summary>
    /// 获取生存装备数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private SuivivalWeaponGoods GetWeaponSuivivalCreateData(WeaponSuivivalCreateType t) {
        if (WeaponSuivivalCreateData.ContainsKey(t))
            return WeaponSuivivalCreateData[t];
        return null;
    }
    /// <summary>
    /// 创建生存装备数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewWeaponSuivivalCreate(WeaponSuivivalCreateType t, object[] o = null) {
        switch (t) {
            case WeaponSuivivalCreateType.Torch: return new TorchSuivivalWeaponGoods(GetWeaponSuivivalCreateData(t), o);
        }
        return null;
    }
    private Goods GetNewWeaponSuivivalCreate(SuivivalWeaponGoods goods, object[] o = null) {
        switch (goods.WeaponSuivivalCreateType) {
            case WeaponSuivivalCreateType.Torch: return new TorchSuivivalWeaponGoods(goods, o);
        }
        return null;
    }

    #endregion

    #region 获取生存建筑数据

    /// <summary>
    /// 获取生存建筑数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private LifeBuildConsumablesGoods GetLifeBuildCreateData(LifeBuildCreateType t) {
        if (LifeBuildCreateData.ContainsKey(t))
            return LifeBuildCreateData[t];
        return null;
    }
    /// <summary>
    /// 创建生存建筑数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewLifeBuildCreate(LifeBuildCreateType t, object[] o = null) {
        switch (t) {
            case LifeBuildCreateType.Bonfire: return new BonfireLifeBuildConsumablesGoods(GetLifeBuildCreateData(t), o);
        }
        return null;
    }
    private Goods GetNewLifeBuildCreate(LifeBuildConsumablesGoods goods, object[] o = null) {
        switch (goods.LifeBuildCreateType) {
            case LifeBuildCreateType.Bonfire: return new BonfireLifeBuildConsumablesGoods(goods, o);
        }
        return null;
    }

    #endregion

    #region 获取炮塔数据

    /// <summary>
    /// 获取近战武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private TurretBuildConsumablesGoods GetTurretBuildGoodsData(TurretBuildCreateType t) {
        if (TurretBuildCreateData.ContainsKey(t))
            return TurretBuildCreateData[t];
        return null;
    }
    /// <summary>
    /// 创建近战武器数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    private Goods GetNewTurretBuildGoods(TurretBuildCreateType t, object[] o = null) {
        switch (t) {
            case TurretBuildCreateType.SteelTurret: return new SteelTurretBuildConsumablesGoods(GetTurretBuildGoodsData(t), o);
        }
        return null;
    }
    private Goods GetNewTurretBuildGoods(TurretBuildConsumablesGoods goods, object[] o = null) {
        switch (goods.TurretBuildCreateType) {
            case TurretBuildCreateType.SteelTurret: return new SteelTurretBuildConsumablesGoods(goods, o);
        }
        return null;
    }

    #endregion

    #endregion

    #region 对外变量和方法

    #region 对外变量

    private int CurrentOnTriggerCount = 0;
    public void AddCurrentOnTrigger(ClientEvent e = null, object[] o = null) => ++CurrentOnTriggerCount;
    public void ReduceCurrentOnTrigger(ClientEvent e = null, object[] o = null) => --CurrentOnTriggerCount;
    public void ResetCurrentOnTriggerCount() => CurrentOnTriggerCount = 0;
    public int GetCurrentOnTriggerCount() => CurrentOnTriggerCount;
    //处于建造移动状态
    public bool IsBuildMove = false;
    //是否可以防止建筑
    private bool buildCanPut = true;
    public bool BuildCanPut {
        get => buildCanPut; set => buildCanPut = value;
    }
    //是否为自由建造模式
    private bool freeBuild;
    public bool FreeBuild {
        get => freeBuild;
        set {
            freeBuild = value;
            BottomBuildBegin.Ins.SetGridShow(value);
        }
    }
    public Vector3 FreeBuildPos(Vector3 pos) => FreeBuild ? pos : new Vector3(Mathf.FloorToInt(pos.x), pos.y, Mathf.FloorToInt(pos.z));

    #endregion

    #region 对外方法

    /// <summary>
    /// 通过id查找类型
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public object GetGoodsTypeByID(int id) {
        if (ItemID2TypeSelf.ContainsKey(id))
            return ItemID2TypeSelf[id];
        return null;
    }
    /// <summary>
    /// 获得物品
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public Goods GetNewGoods(object t, object[] o = null) {
        if (t is ColdWeaponType && ColdWeaponDatas.ContainsKey((ColdWeaponType)t))
            return GetNewColdWeapon((ColdWeaponType)t, o);
        if (t is GunWeaponType && GunWeaponDatas.ContainsKey((GunWeaponType)t))
            return GetNewGunWeapon((GunWeaponType)t, o);
        if (t is DrugType && DrugDatas.ContainsKey((DrugType)t))
            return GetNewDrug((DrugType)t, o);
        if (t is GeneType && GeneDatas.ContainsKey((GeneType)t))
            return GetNewGene((GeneType)t, o);
        if (t is BuffType && BuffDatas.ContainsKey((BuffType)t))
            return GetNewBuff((BuffType)t, o);
        if (t is PlantBiologyType && PlantDatas.ContainsKey((PlantBiologyType)t))
            return GetNewPlant((PlantBiologyType)t, o);
        if (t is BulletType && BulletDatas.ContainsKey((BulletType)t))
            return GetNewBullet((BulletType)t, o);
        if (t is WeaponSuivivalCreateType && WeaponSuivivalCreateData.ContainsKey((WeaponSuivivalCreateType)t))
            return GetNewWeaponSuivivalCreate((WeaponSuivivalCreateType)t);
        if (t is LifeBuildCreateType && LifeBuildCreateData.ContainsKey((LifeBuildCreateType)t))
            return GetNewLifeBuildCreate((LifeBuildCreateType)t, o);
        if (t is TurretBuildCreateType && TurretBuildCreateData.ContainsKey((TurretBuildCreateType)t))
            return GetNewTurretBuildGoods((TurretBuildCreateType)t);
        return null;
    }
    public Goods GetNewGoods(int id) {
        if (ItemDatas.ContainsKey(id))
            return GetNewGoods(GetGoodsTypeByID(id));
        return null;
    }
    /// <summary>
    /// 获取数据,不创建新的
    /// </summary>
    /// <param name="t"></param>
    /// <param name="o"></param>
    /// <returns></returns>
    public Goods GetGoodsData(object t, object[] o = null) {
        if (t is ColdWeaponType && ColdWeaponDatas.ContainsKey((ColdWeaponType)t))
            return GetColdWeaponData((ColdWeaponType)t);
        if (t is GunWeaponType && GunWeaponDatas.ContainsKey((GunWeaponType)t))
            return GetGunWeaponData((GunWeaponType)t);
        if (t is DrugType && DrugDatas.ContainsKey((DrugType)t))
            return GetDrugData((DrugType)t);
        if (t is GeneType && GeneDatas.ContainsKey((GeneType)t))
            return GetGeneData((GeneType)t);
        if (t is BuffType && BuffDatas.ContainsKey((BuffType)t))
            return GetBuffData((BuffType)t);
        if (t is PlantBiologyType && PlantDatas.ContainsKey((PlantBiologyType)t))
            return GetPlantData((PlantBiologyType)t);
        if (t is BulletType && BulletDatas.ContainsKey((BulletType)t))
            return GetBulletData((BulletType)t);
        if (t is WeaponSuivivalCreateType && WeaponSuivivalCreateData.ContainsKey((WeaponSuivivalCreateType)t))
            return GetWeaponSuivivalCreateData((WeaponSuivivalCreateType)t);
        if (t is LifeBuildCreateType && LifeBuildCreateData.ContainsKey((LifeBuildCreateType)t))
            return GetLifeBuildCreateData((LifeBuildCreateType)t);
        if (t is TurretBuildCreateType && TurretBuildCreateData.ContainsKey((TurretBuildCreateType)t))
            return GetTurretBuildGoodsData((TurretBuildCreateType)t);
        return null;
    }
    public Goods GetGoodsData(int id) {
        if (ItemDatas.ContainsKey(id))
            return GetGoodsData(GetGoodsTypeByID(id));
        return null;
    }


    public bool BagItemCanClick(int id) {

        foreach (var item in BulletDatas)
            if (item.Value.ID == id)
                return false;
        foreach (var item in GunWeaponDatas)
            if (item.Value.ID == id)
                return false;
        foreach (var item in ColdWeaponDatas)
            if (item.Value.ID == id)
                return false;
        return true;
    }

    #endregion

    #endregion
}
