using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public enum GoodsType{
        Consumables,//消耗品
        Weapon,//装备
        Biology,//生物相关
        Materials,//材料
        Special,//特殊
    }
    /// <summary>
    /// 物品基类
    /// </summary>
    public class Goods:Item{
        public GoodsType GoodsType;
        public string Source;
        public float Price;
        public string Des;
        public ElementData ElementData;//关联使用者数据
        public int ImgID;//图片ID
        public int Duration;
        public int RemainTime;//剩余时间
        public int EffNumber;
        public int RemainNumber;//剩余次数
        public int BagLimitNum;//背包限制数量 超过则换一格
        
        public float CreateTime;//制造时间

        public Goods(){}

        public Goods(Goods goods,object[]o = null):base(goods){
            Name = goods.Name;
            GoodsType = goods.GoodsType;
            Source = goods.Source;
            Price = goods.Price;
            Des = goods.Des;
            ElementData = goods.ElementData;
            ImgID = goods.ImgID;
            Duration = goods.Duration;
            RemainTime = goods.RemainTime;
            EffNumber = goods.EffNumber;
            RemainNumber = goods.RemainNumber;
            BagLimitNum = goods.BagLimitNum;
            if (BagLimitNum == 0 && this is not Buff)
                Debug.LogError("背包容量为0,检查配表或者初始化");
            CreateTime = goods.CreateTime;
        }
        
        /// <summary>
        /// 物品使用
        /// </summary>
        public virtual void Used(){
            (ElementData??PlayerController.Ins.Data).UseGoods(ID);
        }
        /// <summary>
        /// 不使用物品
        /// </summary>
        public virtual void Nonuse(){
            (ElementData??PlayerController.Ins.Data).RemoveGoods(ID);
        }

        public virtual void Init(ElementData data, object[] o = null){
            ElementData = data;
        }
        public virtual void Remove(){ }
        public virtual void Reset(){ }
        public virtual void RunEffect(){ }
        //制作
        public virtual void GenerateCreate() { }
        //制作时关联的mono初始化
        public virtual void CreateMonoInit() { }
    }
}
