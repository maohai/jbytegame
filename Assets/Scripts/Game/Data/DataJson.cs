
using Newtonsoft.Json.Linq;

namespace Game
{
    
    public class BulletJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 物体类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 图片ID
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 子弹类型
		/// </summary>
		public BulletType BulletType{ get; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int Power{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }

        public BulletJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			ImgID = data["ImgID"].ToObject<int>();
			BulletType = data["BulletType"].ToObject<BulletType>();
			Power = data["Power"].ToObject<int>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
        }
    }


    public class GunWeaponJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 来源
		/// </summary>
		public string Source{ get; }
		/// <summary>
		/// 价格
		/// </summary>
		public int Price{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Des{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 武器类型
		/// </summary>
		public WeaponType WeaponType{ get; }
		/// <summary>
		/// 枪械类型
		/// </summary>
		public GunWeaponType GunWeaponType{ get; }
		/// <summary>
		/// 子弹类型
		/// </summary>
		public BulletType BulletType{ get; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int BaseWeaponAttack{ get; }
		/// <summary>
		/// 基础装弹数量
		/// </summary>
		public int BaseBulletLoadAmount{ get; }
		/// <summary>
		/// 开火间隔
		/// </summary>
		public float FireInterval{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }

        public GunWeaponJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			Source = data["Source"].ToObject<string>();
			Price = data["Price"].ToObject<int>();
			Des = data["Des"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			WeaponType = data["WeaponType"].ToObject<WeaponType>();
			GunWeaponType = data["GunWeaponType"].ToObject<GunWeaponType>();
			BulletType = data["BulletType"].ToObject<BulletType>();
			BaseWeaponAttack = data["BaseWeaponAttack"].ToObject<int>();
			BaseBulletLoadAmount = data["BaseBulletLoadAmount"].ToObject<int>();
			FireInterval = data["FireInterval"].ToObject<float>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
        }
    }


    public class RoleJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 类型
		/// </summary>
		public LandRoleType LandRoleType{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 阵容
		/// </summary>
		public CampType CampType{ get; }
		/// <summary>
		/// 人型类型
		/// </summary>
		public LandType LandType{ get; }
		/// <summary>
		/// 基础血量
		/// </summary>
		public int MaxHP{ get; }
		/// <summary>
		/// 基础蓝量
		/// </summary>
		public int MaxMP{ get; }
		/// <summary>
		/// 基础攻击力
		/// </summary>
		public int BaseAttack{ get; }
		/// <summary>
		/// 基础防御力
		/// </summary>
		public int BaseDefense{ get; }
		/// <summary>
		/// 基础饥饿
		/// </summary>
		public int MaxHunger{ get; }
		/// <summary>
		/// 基础变异
		/// </summary>
		public int MaxVariation{ get; }
		/// <summary>
		/// 基础技能
		/// </summary>
		public string OwnerSkillReader{ get; }

        public RoleJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			LandRoleType = data["LandRoleType"].ToObject<LandRoleType>();
			Name = data["Name"].ToObject<string>();
			CampType = data["CampType"].ToObject<CampType>();
			LandType = data["LandType"].ToObject<LandType>();
			MaxHP = data["MaxHP"].ToObject<int>();
			MaxMP = data["MaxMP"].ToObject<int>();
			BaseAttack = data["BaseAttack"].ToObject<int>();
			BaseDefense = data["BaseDefense"].ToObject<int>();
			MaxHunger = data["MaxHunger"].ToObject<int>();
			MaxVariation = data["MaxVariation"].ToObject<int>();
			OwnerSkillReader = data["OwnerSkillReader"].ToObject<string>();
        }
    }


    public class PlantJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 植物类型
		/// </summary>
		public PlantBiologyType PlantBiologyType{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 来源
		/// </summary>
		public string Source{ get; }
		/// <summary>
		/// 价格
		/// </summary>
		public int Price{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Des{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 持续时间
		/// </summary>
		public int Duration{ get; }
		/// <summary>
		/// 持续次数
		/// </summary>
		public int EffNumber{ get; }
		/// <summary>
		/// 值
		/// </summary>
		public int Value{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }

        public PlantJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			PlantBiologyType = data["PlantBiologyType"].ToObject<PlantBiologyType>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			Source = data["Source"].ToObject<string>();
			Price = data["Price"].ToObject<int>();
			Des = data["Des"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			Duration = data["Duration"].ToObject<int>();
			EffNumber = data["EffNumber"].ToObject<int>();
			Value = data["Value"].ToObject<int>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
        }
    }


    public class SkillJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 类型
		/// </summary>
		public SkillType SkillType{ get; }
		/// <summary>
		/// 等级
		/// </summary>
		public int Level{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Description{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 特效路径
		/// </summary>
		public string EffectPath{ get; }
		/// <summary>
		/// 值
		/// </summary>
		public int Value{ get; }
		/// <summary>
		/// 消耗
		/// </summary>
		public int Cost{ get; }
		/// <summary>
		/// 消耗类型
		/// </summary>
		public SkillCastType SkillCastType{ get; }
		/// <summary>
		/// 最大值
		/// </summary>
		public int MaxValue{ get; }
		/// <summary>
		/// 前摇时间
		/// </summary>
		public float RockTime{ get; }
		/// <summary>
		/// 技能持续时间
		/// </summary>
		public float Time{ get; }
		/// <summary>
		/// 剩余时间
		/// </summary>
		public float RemainTime{ get; }
		/// <summary>
		/// 释放次数
		/// </summary>
		public int CastTimes{ get; }
		/// <summary>
		/// CD
		/// </summary>
		public float CD{ get; }
		/// <summary>
		/// 技能剩余时间
		/// </summary>
		public float RemainCD{ get; }
		/// <summary>
		/// 范围类型
		/// </summary>
		public SkillRangeType SkillRangeType{ get; }
		/// <summary>
		/// 范围大小
		/// </summary>
		public float Range{ get; }
		/// <summary>
		/// 作用类型
		/// </summary>
		public SkillTargetType SkillTargetType{ get; }
		/// <summary>
		/// 影响人数
		/// </summary>
		public int MaxAffectSeveralPeople{ get; }
		/// <summary>
		/// 施放距离
		/// </summary>
		public float DischargeDistance{ get; }
		/// <summary>
		/// 施放法术半径
		/// </summary>
		public float DischargeRadius{ get; }
		/// <summary>
		/// 下一个技能
		/// </summary>
		public int ComboNext{ get; }
		/// <summary>
		/// 技能组合
		/// </summary>
		public int ComboEffects{ get; }
		/// <summary>
		/// 充能机制
		/// </summary>
		public ChargeMechanicType ChargeMechanicType{ get; }
		/// <summary>
		/// 技能效果
		/// </summary>
		public string EffectID{ get; }

        public SkillJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			SkillType = data["SkillType"].ToObject<SkillType>();
			Level = data["Level"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			Description = data["Description"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			EffectPath = data["EffectPath"].ToObject<string>();
			Value = data["Value"].ToObject<int>();
			Cost = data["Cost"].ToObject<int>();
			SkillCastType = data["SkillCastType"].ToObject<SkillCastType>();
			MaxValue = data["MaxValue"].ToObject<int>();
			RockTime = data["RockTime"].ToObject<float>();
			Time = data["Time"].ToObject<float>();
			RemainTime = data["RemainTime"].ToObject<float>();
			CastTimes = data["CastTimes"].ToObject<int>();
			CD = data["CD"].ToObject<float>();
			RemainCD = data["RemainCD"].ToObject<float>();
			SkillRangeType = data["SkillRangeType"].ToObject<SkillRangeType>();
			Range = data["Range"].ToObject<float>();
			SkillTargetType = data["SkillTargetType"].ToObject<SkillTargetType>();
			MaxAffectSeveralPeople = data["MaxAffectSeveralPeople"].ToObject<int>();
			DischargeDistance = data["DischargeDistance"].ToObject<float>();
			DischargeRadius = data["DischargeRadius"].ToObject<float>();
			ComboNext = data["ComboNext"].ToObject<int>();
			ComboEffects = data["ComboEffects"].ToObject<int>();
			ChargeMechanicType = data["ChargeMechanicType"].ToObject<ChargeMechanicType>();
			EffectID = data["EffectID"].ToObject<string>();
        }
    }


    public class SpecialJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 来源
		/// </summary>
		public string Source{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Des{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 持续时间
		/// </summary>
		public int Duration{ get; }
		/// <summary>
		/// 持续次数
		/// </summary>
		public int EffNumber{ get; }
		/// <summary>
		/// 药水类型
		/// </summary>
		public BuffType BuffType{ get; }
		/// <summary>
		/// 值
		/// </summary>
		public int Value{ get; }

        public SpecialJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			Source = data["Source"].ToObject<string>();
			Des = data["Des"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			Duration = data["Duration"].ToObject<int>();
			EffNumber = data["EffNumber"].ToObject<int>();
			BuffType = data["BuffType"].ToObject<BuffType>();
			Value = data["Value"].ToObject<int>();
        }
    }


    public class DrugJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 来源
		/// </summary>
		public string Source{ get; }
		/// <summary>
		/// 价格
		/// </summary>
		public int Price{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Des{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 持续时间
		/// </summary>
		public int Duration{ get; }
		/// <summary>
		/// 持续次数
		/// </summary>
		public int EffNumber{ get; }
		/// <summary>
		/// 药水类型
		/// </summary>
		public DrugType DrugType{ get; }
		/// <summary>
		/// 值
		/// </summary>
		public int Value{ get; }
		/// <summary>
		/// 背包
		/// </summary>
		public int BagLimitNum{ get; }

        public DrugJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			Source = data["Source"].ToObject<string>();
			Price = data["Price"].ToObject<int>();
			Des = data["Des"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			Duration = data["Duration"].ToObject<int>();
			EffNumber = data["EffNumber"].ToObject<int>();
			DrugType = data["DrugType"].ToObject<DrugType>();
			Value = data["Value"].ToObject<int>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
        }
    }


    public class ColdWeaponJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 来源
		/// </summary>
		public string Source{ get; }
		/// <summary>
		/// 价格
		/// </summary>
		public int Price{ get; }
		/// <summary>
		/// 描述
		/// </summary>
		public string Des{ get; }
		/// <summary>
		/// 图片
		/// </summary>
		public int ImgID{ get; }
		/// <summary>
		/// 武器类型
		/// </summary>
		public WeaponType WeaponType{ get; }
		/// <summary>
		/// 冷兵器类型
		/// </summary>
		public ColdWeaponType ColdWeaponType{ get; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int BaseWeaponAttack{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }

        public ColdWeaponJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			Source = data["Source"].ToObject<string>();
			Price = data["Price"].ToObject<int>();
			Des = data["Des"].ToObject<string>();
			ImgID = data["ImgID"].ToObject<int>();
			WeaponType = data["WeaponType"].ToObject<WeaponType>();
			ColdWeaponType = data["ColdWeaponType"].ToObject<ColdWeaponType>();
			BaseWeaponAttack = data["BaseWeaponAttack"].ToObject<int>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
        }
    }


    public class LifeBuildCreateJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 资源路径
		/// </summary>
		public string Path{ get; }
		/// <summary>
		/// 建造时间
		/// </summary>
		public float CreateTime{ get; }
		/// <summary>
		/// 建筑类型
		/// </summary>
		public BuildCreateType BuildCreateType{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public LifeBuildCreateType LifeBuildCreateType{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }
		/// <summary>
		/// 建造时受限范围
		/// </summary>
		public int BuildingCircle{ get; }
		/// <summary>
		/// 建造完成时影响范围
		/// </summary>
		public int EffCircle{ get; }

        public LifeBuildCreateJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			Path = data["Path"].ToObject<string>();
			CreateTime = data["CreateTime"].ToObject<float>();
			BuildCreateType = data["BuildCreateType"].ToObject<BuildCreateType>();
			LifeBuildCreateType = data["LifeBuildCreateType"].ToObject<LifeBuildCreateType>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
			BuildingCircle = data["BuildingCircle"].ToObject<int>();
			EffCircle = data["EffCircle"].ToObject<int>();
        }
    }


    public class WeaponSuivivalCreateJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public WeaponSuivivalCreateType WeaponSuivivalCreateType{ get; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int BaseWeaponAttack{ get; }
		/// <summary>
		/// 耐久
		/// </summary>
		public int MaxDurability{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public GoodsType GoodsType{ get; }
		/// <summary>
		/// 武器类型
		/// </summary>
		public WeaponType WeaponType{ get; }

        public WeaponSuivivalCreateJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			WeaponSuivivalCreateType = data["WeaponSuivivalCreateType"].ToObject<WeaponSuivivalCreateType>();
			BaseWeaponAttack = data["BaseWeaponAttack"].ToObject<int>();
			MaxDurability = data["MaxDurability"].ToObject<int>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
			GoodsType = data["GoodsType"].ToObject<GoodsType>();
			WeaponType = data["WeaponType"].ToObject<WeaponType>();
        }
    }


    public class TurretBuildCreateJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 资源路径
		/// </summary>
		public string Path{ get; }
		/// <summary>
		/// 建造时间
		/// </summary>
		public float CreateTime{ get; }
		/// <summary>
		/// 建筑类型
		/// </summary>
		public BuildCreateType BuildCreateType{ get; }
		/// <summary>
		/// 所属类型
		/// </summary>
		public TurretBuildCreateType TurretBuildCreateType{ get; }
		/// <summary>
		/// 背包限制
		/// </summary>
		public int BagLimitNum{ get; }
		/// <summary>
		/// 建造时受限范围
		/// </summary>
		public int BuildingCircle{ get; }
		/// <summary>
		/// 建造完成时影响范围
		/// </summary>
		public int EffCircle{ get; }

        public TurretBuildCreateJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			Path = data["Path"].ToObject<string>();
			CreateTime = data["CreateTime"].ToObject<float>();
			BuildCreateType = data["BuildCreateType"].ToObject<BuildCreateType>();
			TurretBuildCreateType = data["TurretBuildCreateType"].ToObject<TurretBuildCreateType>();
			BagLimitNum = data["BagLimitNum"].ToObject<int>();
			BuildingCircle = data["BuildingCircle"].ToObject<int>();
			EffCircle = data["EffCircle"].ToObject<int>();
        }
    }


    public class MergeJsonData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; }
		/// <summary>
		/// 类型
		/// </summary>
		public MergeType MergeType{ get; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; }
		/// <summary>
		/// 合成路径
		/// </summary>
		public string MergePath{ get; }
		/// <summary>
		/// 合成物品
		/// </summary>
		public int TargetID{ get; }

        public MergeJsonData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			MergeType = data["MergeType"].ToObject<MergeType>();
			Name = data["Name"].ToObject<string>();
			MergePath = data["MergePath"].ToObject<string>();
			TargetID = data["TargetID"].ToObject<int>();
        }
    }


}
