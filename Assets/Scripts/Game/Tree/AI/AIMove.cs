using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Game;
using UnityEngine;

namespace BTTree{
    public class AIMove : BaseAIAction{
        public override TaskStatus OnUpdate(){
            if (transform.TryGetComponent(out BaseElementMono e)){
                // e.Move();
                return TaskStatus.Running;
            }

            return TaskStatus.Success;
        }
    }
}
