using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using Game;
using UnityEngine;

namespace BTTree{
    public class AISeek :BaseAIAction
    {
        public override TaskStatus OnUpdate(){
            if (transform.TryGetComponent(out BaseElementMono e)){
                // if (e.Seek()) 
                return TaskStatus.Success;
            }
       
            return TaskStatus.Running;
        
        }
    }

}