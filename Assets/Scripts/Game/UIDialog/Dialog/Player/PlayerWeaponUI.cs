using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWeaponUI : MonoBehaviour {
    public Image ImgWeapon;
    public Text TxtWeaponName;
    public Text TxtBullet;
    public Text TxtOther;

    private GunWeaponGoods weaponGoods;

    private void OnEnable() {
        EventManager.OnRefreshWeapon.Register(RefreshBullet);
        EventManager.OnEquitWeapon.Register(RefreshWeapon);
        RefreshBullet();
    }

    private void OnDisable() {
        EventManager.OnRefreshWeapon.Remove(RefreshBullet);
        EventManager.OnEquitWeapon.Remove(RefreshWeapon);
    }

    public void RefreshWeapon(ClientEvent e = null, object[] o = null) {
        weaponGoods = PlayerController.Ins.rightWeaponSlotSlot.CurrentWeapon as GunWeaponGoods;
        //todo 根据ID查找图片
        // ImgWeapon.sprite = goods.ImgID;
        RefreshBullet();
    }
    public void RefreshBullet(ClientEvent e = null, object[] o = null) {
        if (weaponGoods != null) {
            TxtWeaponName.text = weaponGoods.Name;
            TxtBullet.text = $"{weaponGoods.CurrentBulletLoadAmount}/{weaponGoods.BaseBulletLoadAmount}";
        } else {
            TxtWeaponName.text = "无装备";
            TxtBullet.text = "0/0";
        }
    }
}
