using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBaseVariationLerpCom : MonoBehaviour {
    public Slider sliderBack;
    public Slider sliderFront;

    //用于初始化变化等待时间间隔
    public float baseWaitTime = 0.5f;
    //血条变化时间
    public float hpBaseVariationTime = 0.5f;
    //用于血条变化等待时间间隔
    private float hpWaitTime = 0;

    private bool isChange = false;
    private float target; //当前变化的值
    private float nextTarget; //下一次变化的值
    private float currentValue;//当前的动画值
    private Coroutine hpAnimTimer;
    private void Start() {
        Init();
    }

    public virtual void Init() {
        currentValue = 1;
        target = 1;
        nextTarget = 1;
        sliderBack.value = 1;
        sliderFront.value = 1;
    }

    private void OnEnable() {
        hpWaitTime = baseWaitTime;
    }

    private void Update() {
        if (currentValue != target) {
            if (hpWaitTime > 0) {
                hpWaitTime -= Time.deltaTime;
            } else if (!isChange) {
                isChange = true;
                hpAnimTimer = StartCoroutine(Utility.Ins.SliderValueVariationCoroutine(
                    sliderBack,
                    currentValue,
                    target,
                    hpBaseVariationTime,
                    EndAnim));
            }
        }
    }

    public void EndAnim() {
        isChange = false;
        currentValue = target;
        if (nextTarget != target) {
            target = nextTarget;
            hpWaitTime = baseWaitTime;
        }
    }

    public void RefreshHP(float value) {
        sliderFront.value = value;
        if (isChange) {
            nextTarget = value;
            //当血条回升的时候,如果大于正在运动的背景血量,直接结束动画,并重置数据
            if (nextTarget > sliderBack.value) {
                if (hpAnimTimer != null)
                    StopCoroutine(hpAnimTimer);
                isChange = false;
                currentValue = nextTarget;
                target = nextTarget;
                sliderBack.value = nextTarget;
            }
        } else {
            //如果没动画时,则刷新动画等待时间和目标值
            if (value > sliderBack.value) {
                sliderBack.value = value;
                currentValue = value;
            }
            hpWaitTime = baseWaitTime;
            target = value;
            nextTarget = target;
        }
    }
}