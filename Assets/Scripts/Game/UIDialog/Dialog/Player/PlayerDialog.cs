using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Cryptography;
using Game;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// 游戏开始页面
/// </summary>
public class PlayerDialog : BaseDialog<PlayerDialog>{
    public PlayerBaseVariationLerpCom PlayerHPUI;
    public GameObject Prefab_BagItem;
    public Transform TransBagMapContent;
    public Transform TransPlayerArm;
    
    private ConcurrentDictionary<int, BagItem> dicBagMap = new();
    private BagManager bag => BagManager.Ins;
    public static void Show(){
        uimanager.OverShow(
            () => { uimanager.Over("Dialog/Player/PlayerDialog");}
        );
    }
    public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null){
        base.OnUIShow(type, o);
        Init();
    }
    public override void Close(){
        base.Close();
        Debug.Log("我关闭了");
    }

    private void OnEnable(){
        EventManager.Ins.AddRefreshBag(OnRefreshBag);
        EventManager.OnUpdateRefresh.Register(RefreshArm);
        EventManager.OnInArmEvent.Register(OnInArm);
        EventManager.OnOutArmEvent.Register(OnOutArm);
    }

    private void OnDisable(){
        EventManager.Ins.RemoveRefreshBag(OnRefreshBag);
        EventManager.OnUpdateRefresh.Remove(RefreshArm);
        EventManager.OnInArmEvent.Remove(OnInArm);
        EventManager.OnOutArmEvent.Remove(OnOutArm);
    }

    private void OnRefreshBag(IEventMessage e = null, object[] o = null){
        RefreshBagMap((Goods)o[0]);
    }

    private void Init(){
        PlayerHPUI.Init();
        InitPlayerBag();
        TransPlayerArm.gameObject.SetActive(false);
    }
    private void InitPlayerBag(){
        for (int i = 0; i < BagManager.QuickBagSize; i++){
            var g = Instantiate(Prefab_BagItem, TransBagMapContent).transform;
            var bagCom = g.GetComponent<BagItem>();
            bagCom.Init(i,BagItemType.QuickBag);
            dicBagMap.TryAdd(i,bagCom);
        }
        RefreshBagMap();
    }
    
    public void RefreshBagMap(Goods refreshSingleGoods = null){
        //如果没有加入主背包,则直接
        if (!(bag.RefreshBagType == BagItemType.All ||bag.RefreshBagType == BagItemType.QuickBag)) return;
        //复制一份出来 方便原数据修改
        Dictionary<int, (int id, int num)> bagData = new(bag.GetQuickBagData());
        if (refreshSingleGoods != null){
            foreach (var item in bagData){
                if (item.Value.id == refreshSingleGoods.ID){
                    var bagItem = dicBagMap[item.Key];
                    if (bagItem.CheckGoodsIsNull())
                        bagItem.BindData(refreshSingleGoods);
                    else bagItem.Refresh();
                    
                    SetBagMap(item.Key,bagItem);
                    dicBagMap[item.Key] = bagItem;
                }
            }
        }
        else{
            foreach (var item in bagData){
                var tempBagItem = dicBagMap[item.Key];
                if (item.Value.id >= 0 && item.Value.num > 0)
                    tempBagItem.BindData(GoodsManager.Ins.GetNewGoods(item.Value.id));
                else tempBagItem.Refresh();
                
                SetBagMap(item.Key, tempBagItem);
                dicBagMap[item.Key] = tempBagItem;
            }
        }
    }
    
    private void SetBagMap(int index,BagItem bagItem){
        if (index > BagManager.QuickBagSize) index = BagManager.QuickBagSize - 1;
        if (index < 0) index = 0;
        if (bagItem.Data == null)
            PlayerInput.Ins.ClearAlphaAction(index+1);
        else{
            PlayerInput.Ins.SetAlphaAction(index+1, () => {
                bagItem.Data.Used();
                bag.RemoveUsedGoods(bagItem,1);
            });
        }
    }
    
    private void RefreshArm(ClientEvent e=null,object[]o=null){
        if(TransPlayerArm.gameObject.activeSelf)
            TransPlayerArm.position = Utility.Ins.GetUIMousePosition();
    }
    private void OnInArm(ClientEvent e=null,object[]o=null){
        TransPlayerArm.gameObject.SetActive(true);
    }
    private void OnOutArm(ClientEvent e=null,object[]o=null){
        TransPlayerArm.gameObject.SetActive(false);
    }
    
    
    /// <summary>
    /// 打开背包
    /// </summary>
    public void OnClickBag(){
        BagDialog.Show();
    }
    public void OnClickCreate()
    {
        BuildCreateDialog.Show();
    }
}