using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum BagItemType{
    Bag,//背包格子
    Weapon,//装备格子
    QuickBag,//快捷键格子
    Box,//箱子格子
    All,//用于刷新
}

public class BagItem : MonoBehaviour
    ,IInitializePotentialDragHandler    //拖拽初始化
    ,IBeginDragHandler                  //拖拽开始时候
    ,IDragHandler                       //拖拽时
    ,IEndDragHandler                    //拖拽结束
{
    protected BagManager bag => BagManager.Ins;
    public Image imgGoods;
    public Text txtGoods;
    
    public Goods Data;
    [HideInInspector] public int BagItemIndex;
    [HideInInspector] public BagItemType BagItemType;
    [HideInInspector] public WeaponKindType WeaponKindType => (WeaponKindType)BagItemIndex;
    private bool CanClick;
    /// <summary>
    /// 用于格子初始化
    /// </summary>
    /// <param name="index"></param>
    /// <param name="type"></param>
    public void Init(int index,BagItemType type = BagItemType.Bag){
        BagItemIndex = index;
        BagItemType = type;
        Hide();
    }

    /// <summary>
    /// 重置数据
    /// </summary>
    protected virtual void Reset(){
        Data = null;
    }
    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="data"></param>
    public virtual void BindData(Goods data){
        Data = data;
        CanClick = GoodsManager.Ins.BagItemCanClick(Data.ID);
        var c = Random.Range(0.5f, 1.0f);
        imgGoods.color = new Color(c,c,c);
        //imgGoods.sprite = AtlasManager.Ins.GetSpriteByName("blue");
        Refresh();
    }
    
    /// <summary>
    /// 移除数据
    /// </summary>
    private void RemoveData(){
        // 重置数据
        switch (BagItemType){
            case BagItemType.Bag:
                if (Data == null) return;
                bag.ResetBagGoodsByIndex(Data.GoodsType,BagItemIndex);
                break;
            case BagItemType.Weapon:
                // bag.ResetWeaponBagGoodsByKindType(WeaponKindType);
                break;
            case BagItemType.Box:
                break;
            case BagItemType.QuickBag:
                bag.ResetQuickBagGoodsByIndex(BagItemIndex);
                break;
        }
    }
    /// <summary>
    /// 检查数据是否为空
    /// </summary>
    /// <returns></returns>
    public bool CheckGoodsIsNull() => Data == null;
    /// <summary>
    /// 刷新
    /// </summary>
    public void Refresh(bool reset = false){
        if(reset) Reset();
        int num = 0;
        if (Data != null){
            switch (BagItemType){
                case BagItemType.Bag:
                    num = bag.GetBagNumByGoods(Data.GoodsType, BagItemIndex);
                    break;
                case BagItemType.Weapon:
                    num = 1;
                    break;
                case BagItemType.Box:
                    break;
                case BagItemType.QuickBag:
                    num = bag.GetQuickNumByIndex(BagItemIndex);
                    break;
            }
        }
        if (num <= 0)
            Hide();
        else{
            Hide(false);
            if (num <= 1) txtGoods.text = Data.Name;
            else txtGoods.text = $"{Data.Name} X{num}";
        }
    }
    
    private void Hide(bool flag = true){
        //隐藏的时候移除数据
        if (flag){
            RemoveData();
            Reset();
        }
        imgGoods.gameObject.SetActive(!flag);
        txtGoods.gameObject.SetActive(!flag);
        
    }
    /// <summary>
    /// 格子点击使用
    /// </summary>
    public void OnClick(){
        bag.CurrentDrapBagItem = this;
        bag.CurrentDrapIndex = BagItemIndex;
        if (PlayerInput.Ins.ShiftClick){
            var index = bag.GetEmptyQuickIndex();
            if(index != BagManager.DefaultID){
                bag.SwapCell();
                return;
            }
        }
        //是否能够点击使用消失
        if (CanClick){
            Data.Used();
            bag.RemoveUsedGoods(this,1);
        }
    }

    #region 鼠标操作相关接口
    
    /// <summary>
    /// 点击事件初始化
    /// </summary>
    /// <param name="eventData"></param>
    public void OnInitializePotentialDrag(PointerEventData eventData){ }
    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData){
        if (bag.CurrentDrapBagItem != null) return;
        bag.CurrentDrapBagItem = this;
        bag.CurrentDrapIndex = BagItemIndex;
    }
    /// <summary>
    /// 正在拖拽
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData){ }

    public void OnEndDrag(PointerEventData eventData){
        #region 空引用判断
        //立即返回
        var pointerEnter = eventData.pointerEnter;
        if (eventData.pointerEnter == null || pointerEnter.transform.parent == null){
            bag.CurrentDrapBagItem = null;
            bag.CurrentDrapIndex = -1;
            return;
        }
        pointerEnter.transform.parent.TryGetComponent(out BagItem bagEnter);
        if (bag.CurrentDrapBagItem == null || bagEnter == null || bag.CurrentDrapBagItem == bagEnter){
            bag.CurrentDrapBagItem = null;
            bag.CurrentDrapIndex = -1;
            return;
        }
        #endregion
        bag.SwapCell(bagEnter);
    }

    #endregion
}
