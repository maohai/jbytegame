using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

/// <summary>
/// 游戏开始页面
/// </summary>
public class BagDialog : BaseDialog<BagDialog>{
    public static void Show(){
        uimanager.OverShow(
            () => { uimanager.Over("Dialog/Bag/BagDialog");}
        );
    }

    public Transform TransBagContent;
    public GameObject Prefab_BagItem;
    public Image ImgPickUp;
    public Image[] ImgClick;

    private BagManager bag => BagManager.Ins;
    public int CurrentClickBagIndex => (int)bag.CurrentClickBagIndex;//当前页面的Index
    /// <summary>
    /// 下标 id 数量
    /// </summary>
    private Dictionary<int,(int id, int num)> currentPageData;//当前页面持有物品
    /// <summary>
    /// 下标 id 预制体
    /// </summary>
    private Dictionary<int,(int id, BagItem bagItem)> currentPageGoods = new();//当前页面对应预制体
    public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null){
        base.OnUIShow(type, o);
        InitBag();
        InitBagUI();
        ClickGoodsType(CurrentClickBagIndex);
    }

    private void OnEnable(){
        EventManager.Ins.AddRefreshBag(OnRefreshBag);
    }
    private void OnDisable(){
        EventManager.Ins.RemoveRefreshBag(OnRefreshBag);
    }
    private void OnRefreshBag(IEventMessage e = null, object[] o = null){
        RefreshCurrentPage((Goods)o[0]);
        RefreshWeaponUI();
    }
    private void InitBag(){
        for (int i = 0; i < BagManager.BagItemSize; i++){
            var g = Instantiate(Prefab_BagItem, TransBagContent).transform;
            var bagCom = g.GetComponent<BagItem>();
            bagCom.Init(i);
            currentPageGoods.Add(i,(BagManager.DefaultID,bagCom));
        }
    }

    private void InitBagUI(){
        RefreshPickUp();
        RefreshWeaponUI();
    }

    private void RefreshPickUp(){
        ImgPickUp.color = bag.FastPickUp ? Color.red : Color.green;
    }
    /// <summary>
    /// 点击切换页面
    /// </summary>
    /// <param name="i"></param>
    private bool OpenDialog = false;
    public void ClickGoodsType(int i){
        // Consumables,//消耗品
        // Weapon,//装备
        // Biology,//生物相关
        // Materials,//材料
        // Special,//特殊
        if (OpenDialog && i == CurrentClickBagIndex){
            return;
        }
        OpenDialog = true;
        bag.CurrentClickBagIndex = (GoodsType)i;
        RefreshCurrentPage();
        RefreshGoodsTypeClickUI();
    }
    public void RefreshCurrentData()=> currentPageData = new(bag.GetGoodsListByType((GoodsType)CurrentClickBagIndex));
    
    /// <summary>
    /// 刷新背包页面
    /// </summary>
    /// <param name="refreshSingleGoods"></param>
    /// <param name="num"></param>
    private void RefreshCurrentPage(Goods refreshSingleGoods = null){
        if (!(bag.RefreshBagType is BagItemType.All or BagItemType.Bag)) return;
        
        RefreshCurrentData();
        if (refreshSingleGoods != null){
            foreach (var item in currentPageData){
                if (item.Value.id == refreshSingleGoods.ID){
                    var bagItem = currentPageGoods[item.Key].bagItem;
                    if (bagItem.CheckGoodsIsNull()) bagItem.BindData(refreshSingleGoods);
                    else bagItem.Refresh();
                    currentPageGoods[item.Key] = (item.Value.num>0?refreshSingleGoods.ID:BagManager.DefaultID, bagItem);
                }
            }
        }
        else{
            //刷新所有 用于切换页面
            foreach (var item in currentPageData){
                if(item.Value.id != BagManager.DefaultID && item.Value.num == 0) {
                    Debug.LogError("背包限制数量为0 显示不出 需要修改数值表");
                }
                var tempBagItem = currentPageGoods[item.Key].bagItem;
                if (item.Value.id >= 0 && item.Value.num > 0)
                    tempBagItem.BindData(GoodsManager.Ins.GetGoodsData(item.Value.id));
                else tempBagItem.Refresh(true);
                //将数量为0的格子id设置为-1
                currentPageGoods[item.Key] = (item.Value.num>0?item.Value.id:BagManager.DefaultID, tempBagItem);
            }
        }
    }
    private void RefreshGoodsTypeClickUI()
    {
        foreach (var item in ImgClick)
        {
            item.color = Color.white;
        }
        ImgClick[CurrentClickBagIndex].color = Color.green;
    }

    private void RefreshWeaponUI(){
        WeaponBagUIManager.Ins.RefreshUI();
    }
    /// <summary>
    /// 自动整理背包
    /// </summary>
    public void ClickSortBag(){
        bag.SortBagGoods();
    }

    public void ClickSetFastPickUp(){
        bag.FastPickUp = !bag.FastPickUp;
        ElementPickUp.Ins.ClearPickUpGoods();
        RefreshPickUp();
    }
}