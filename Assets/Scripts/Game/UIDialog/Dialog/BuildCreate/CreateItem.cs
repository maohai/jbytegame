using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateItem : MonoBehaviour {
    public Image ImgIcon;
    public Text TxtName;
    private GoodsType GoodsType;
    public void Init(GoodsType type) {
        GoodsType = type;
        TxtName.text = $"{type}";
    }
    public void OnClickCreate() {
        BuildCreateDialog.Ins?.RefreshCreate(GoodsType);
    }
    public void RefreshIconColor() {
        ImgIcon.color = GoodsType == UIManager.Ins.BuildCreateDialog_CurrentGoodsType ? Color.green : Color.white;
    }
}
