using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Game;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

/// <summary>
/// 游戏开始页面
/// </summary>
public class BuildCreateDialog : BaseDialog<BuildCreateDialog> {
    public Transform TransCreateItemParent;
    public GameObject Prefab_ComplexGoodsItem;
    public Transform TransCreateContent;
    public GameObject Prefab_PlayerCreateItem;
    public Image ImgFreeBuild;

    private List<ComplexGoodsItem> ComComplexGoodsList = new();
    private List<CreateItem> ComCreateImte = new();
    public static void Show() {
        uimanager.OverShow(
            () => { uimanager.Over("Dialog/BuildCreate/BuildCreateDialog"); }
        );
    }
    public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null) {
        base.OnUIShow(type, o);
        Init();
    }
    public override void Close() {
        base.Close();
    }
    public void Init() {
        foreach (var item in GoodsManager.Ins.GoodsMergeDic) {
            var g = Instantiate(Prefab_ComplexGoodsItem, TransCreateItemParent).GetComponent<ComplexGoodsItem>();
            g.Init(item.Value);
            ComComplexGoodsList.Add(g);
        }

        InitCreateItem();
        RefreshCreate(UIManager.Ins.BuildCreateDialog_CurrentGoodsType);
        SetFreeBuild();
    }

    private void InitCreateItem() {
        Utility.Ins.BindActionByEnum(typeof(GoodsType), (e) => {
            var g = Instantiate(Prefab_PlayerCreateItem, TransCreateContent).transform;
            var createCom = g.GetComponent<CreateItem>();
            createCom.Init((GoodsType)e);
            ComCreateImte.Add(createCom);
        });
    }

    private GoodsType? currentType = null;
    public void RefreshCreate(GoodsType type) {
        if (currentType != null && currentType == type) return;
        currentType = type;
        UIManager.Ins.BuildCreateDialog_CurrentGoodsType = type;

        if (ComComplexGoodsList.Count != 0) {
            foreach (var item in ComComplexGoodsList) {
                item.gameObject.SetActive(item.CanShow(type));
            }
        }

        if (ComCreateImte.Count != 0) {
            foreach (var item in ComCreateImte) {
                item.RefreshIconColor();
            }
        }
    }

    public void OnClickFreeBuild() {
        GoodsManager.Ins.FreeBuild = !GoodsManager.Ins.FreeBuild;
        SetFreeBuild();
    }
    private void SetFreeBuild() {
        ImgFreeBuild.color = GoodsManager.Ins.FreeBuild ? Color.green : Color.red;
    }
}