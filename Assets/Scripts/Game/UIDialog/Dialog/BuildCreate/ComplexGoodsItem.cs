using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComplexGoodsItem : MonoBehaviour
{
    public Transform TransItemParent;
    public GameObject Prefab_NeewMateriaItem;
    public Image ImgCreate;
    public Text TxtCreateName;

    private GoodsMerge goodsMerge;
    private List<NeedMateralItem> needList = new();
    public void Init(GoodsMerge data)
    {
        goodsMerge = data;
        Refresh();
    }
    public void Refresh()
    {
        TxtCreateName.text = $"{goodsMerge.MergeType}";
        foreach (var item in goodsMerge.MergeMaterials)
        {
            var g = Instantiate(Prefab_NeewMateriaItem, TransItemParent).GetComponent<NeedMateralItem>();
            g.Init(item.id,item.num);
            needList.Add(g);
        }
    }
    public bool CanShow(GoodsType type)
    {
        return type == goodsMerge.GoodsType;
    }
    public void OnClickConfirm()
    {
        goodsMerge.Merge();
    }
}
