using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipRDialog : BaseDialog<TipRDialog>{
    public static void Show(){
        uimanager.RepeatShow(
            () => { uimanager.Repeat("Tip/TipRDialog",new []{(object)1.5f}); }
        );
    }

    public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null){
        base.OnUIShow(type, o);
        //设置自动消失时间为2秒
        Debug.Log("tip开启了");
    }

    public override void Close(){
        base.Close();
        Debug.Log("tip关闭了");
    }
}