using UnityEngine;

/// <summary>
/// 游戏开始页面
/// </summary>
namespace Game {
    public class BeginDialog : BaseDialog<BeginDialog> {
        public static void Show() {
            uimanager.OverShow(
                () => { uimanager.Over("BeginDialog"); }
            );
        }

        public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null) {
            base.OnUIShow(type, o);
            Init();
        }

        private void Init() {
        }

        public void OnClickBeginGame() {
            Close();
            //关于游戏主页面显示 是否是新游戏与继续游戏后续做行为判断
            ScenesManager.Ins.LoadSceneAsyn("Game", () => {
                PlayerDialog.Show();
            });
        }

        public override void Close() {
            base.Close();
        }
    }
}