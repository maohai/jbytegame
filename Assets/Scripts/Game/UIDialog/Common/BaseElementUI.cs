using Game;
using Unity.VisualScripting;
using UnityEngine;

public interface ElementUIUpdatePos{
    public void UpdateUIPos();
}

public class BaseElementUI : MonoBehaviour
    ,ElementUIUpdatePos
{
    protected UIManager ui => UIManager.Ins;
    protected IUIPos uiPos => Data.UIPos;
    protected ElementData Data;
    protected object[] paramsValue;

    public virtual void Init(ElementData data, object[] o = null){
        Data = data;
        paramsValue = o;
        RefreshUI();
    }

    public virtual void UpdateUIPos(){ }
    public virtual void RefreshUI(){}
}