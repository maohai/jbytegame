using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ElementPickUp : SingletonElementUI<ElementPickUp>{
    private Dictionary<BaseGoodsMono, int> pickUpGoods = new();
    private BagManager bag => BagManager.Ins;
    
    public GameObject objPickUp;
    public override void Init(ElementData data, object[] o = null){
        base.Init(data, o);
        RefreshSingtonUI();
    }

    public void AddCount(BaseGoodsMono mono){
        if (bag.FastPickUp){
            PickUpGoods(mono);
        }
        else{
            if(!pickUpGoods.ContainsKey(mono))
                pickUpGoods.Add(mono,mono.GoodsData.UID);
            RefreshSingtonUI();
        }
    }

    public void ReduceCount(BaseGoodsMono mono){
        pickUpGoods.Remove(mono);
        RefreshSingtonUI();
    }

    public void PickUpGoods(BaseGoodsMono mono){
        BagManager.Ins.AddGoods(mono.GoodsData);
        mono.GoodsHide();
    }

    public void ClearPickUpGoods(){
        foreach (var item in pickUpGoods){
            PickUpGoods(item.Key);
        }
        pickUpGoods.Clear();
        RefreshSingtonUI();
    }

    public BaseGoodsMono GetPickUpGoods(){
        if(pickUpGoods.Count != 0)
            foreach (var item in pickUpGoods){
                return item.Key;
            }

        return null;
    }
    
    public void RefreshSingtonUI(){
        objPickUp.SetActive(!bag.FastPickUp && pickUpGoods.Count != 0);
    }
}
