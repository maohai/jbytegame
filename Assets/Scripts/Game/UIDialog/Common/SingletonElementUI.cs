using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SingletonElementUI<T> : BaseElementUI where T : SingletonElementUI<T>{
    public static T Ins { get; protected set; }
    public override void Init(ElementData data, object[] o = null){
        Ins = (T)this;
        base.Init(data, o);
    }
}
