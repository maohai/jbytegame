using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ElementHP : BaseElementUI{

    public Transform TransMain;
    public Slider TransHP;
    
    public override void UpdateUIPos(){
        ResetPos(Utility.Ins.World2Screen(uiPos.TransHP.position));
    }
    private void ResetPos(Vector2 screenPos){
        TransMain.transform.localPosition = screenPos;
        // Debug.Log($"当前血条坐标为{screenPos.y} 归一化为{screenPos.normalized.y}");
        //如果主角在血条前面,则血条的y轴为正值
        Utility.Ins.ObjSetActive(TransMain.gameObject,screenPos.y is < 300);
    }

    public override void RefreshUI(){
        TransHP.value = Data.HP*1.0f/Data.MaxHP;
        if(Data.HP <= 0)
            ui.RemoveElementUI(this);
    }
}
