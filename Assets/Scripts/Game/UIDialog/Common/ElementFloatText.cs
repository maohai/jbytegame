using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Game;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 飘字类型
/// </summary>
public enum FloatTextType{
    Null,//没有类型
    CommonAddHP,//普通加血 绿色
    CommonReduceHP,//普通减血  红色
    CriticalStrike,//暴击 黄色
}

public class ElementFloatText : BaseElementUI{
    public Text TxtContent;
    protected float Speed = 5;
    private float baseTime = 0;
    protected float hideTime = 0;
    protected FloatTextType FloatTextType;

    #region 曲线运动

    private int beizerNum = 45;
    private Vector3 currentPos = Vector3.zero;
    private Vector3[] path;
    private float beizerAnimTime = 0;
    private int beizerCurrentPointIndex = 0;

    #endregion
    
    public override void Init(ElementData data, object[]o=null){
        base.Init(data,o);
        TxtContent.text = (string) paramsValue[0];
        FloatTextType = (FloatTextType) paramsValue[1];
        SetTxtColor();
        if (o.Length > 2)
            baseTime = (float) paramsValue[2];
        else baseTime = 1;
        hideTime = baseTime;
        
        
        currentPos = Utility.Ins.World2Screen(uiPos.TransFloatText.position);
        //获取贝塞尔曲线运动路径
        var x = Random.Range(-100, 100);
        var controlPos = currentPos + new Vector3(x, 100, 0);
        var endPos = currentPos + new Vector3(x, -200, 0);
        path = Utility.Ins.GetBeizerList(currentPos, controlPos, endPos, beizerNum);
    }
    
    Vector3 newPos = Vector3.zero;
    public override void UpdateUIPos(){
        hideTime -= Time.deltaTime;
        if (hideTime < 0){
            UIManager.Ins.RemoveElementUI(this);
        }
        newPos = Utility.Ins.World2Screen(uiPos.TransFloatText.position);
        //获取当前运动点的偏移
        Utility.Ins.GetBeizerIndex(beizerNum, baseTime,ref beizerCurrentPointIndex,ref beizerAnimTime);
        var offset =  path[beizerCurrentPointIndex-1] - currentPos;//偏移
        
        TxtContent.transform.localPosition = newPos+offset;
    }

    public void SetTxtColor(){
        switch (FloatTextType){
            case FloatTextType.CommonAddHP:{
                TxtContent.color = Color.green;
                break;
            }
            case FloatTextType.CommonReduceHP:{
                TxtContent.color = Color.red;
                break;
            }
            case FloatTextType.CriticalStrike:{
                TxtContent.color = Color.yellow;
                break;
            }
        }
    }
}
