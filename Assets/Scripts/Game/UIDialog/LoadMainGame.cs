using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMainGame : MonoBehaviour{
    /// <summary>
    /// 游戏入口
    /// </summary>
    void Start(){
        UIManager.Ins.GameEntry();
        Client.Ins.Init();
        //物品数据初始化
        
    }
}