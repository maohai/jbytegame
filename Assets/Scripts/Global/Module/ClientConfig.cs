using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Platform
{
    StandaloneWindows,
    StandaloneWindows64,
    Android,
    WebGL
}
[CreateAssetMenu(fileName = "ClientConfig", menuName = "Config/Client")]
public class ClientConfig : ScriptableObject
{
    [Header("热更IP地址")]
    public string HotIpAddress;

    [Header("版本")]
    public string Version;
    [Header("构建平台")]
    public Platform BuildPlatform;
    [Space(10)]
    [Header("保存数据是否加密")]
    public bool SaveDataEncryption = false;
    [Header("秘钥")]
    public string Key = "QWERASDF";
    [Space(10)]
    [Header("开启socket")]
    public bool StarSocketServer;

    [Header("服务器IP地址")]
    public string ServerIpAddress;


    [Header("本地服务器路径")]
    public string WebPath;
}