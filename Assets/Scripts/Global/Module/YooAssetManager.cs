using Cysharp.Threading.Tasks;
using System;
using UnityEngine.U2D;
using UnityEngine;
using YooAsset;
using System.Collections;

public class YooAssetManager : SingletonAutoMono<YooAssetManager> {
    public static UniTask<ResourcePackage> LoadPackage(string packageName = "Main") => Ins._LoadPackage(packageName);

    public static ResourcePackage DefaultPackage => YooAssets.GetPackage("Main");

    public static void UnloadAssets(string packageName) {
        try {
            var package = YooAssets.GetPackage(packageName);
            if (package != null) {
                package.UnloadUnusedAssets();
            }
        } catch (Exception e) {
            // Debug.LogError(e);
            // throw;
        }

    }


    public EPlayMode PlayMode;
    public event Action<long, long> EventDownloadUpdate;

    private long downloadSize;
    private long totalSize;

    private string updateVersion = "";
    private string packageName = "Main";

    private ClientConfig _config;
    protected override void Awake() {
        base.Awake();
        //_config = Resources.Load<ClientConfig>("ClientConfig");
        //注册
        //SpriteAtlasManager.atlasRegistered += (atlas =>
        //{
        //Debug.LogError("注册了图集" + atlas.name);
        //});
        //请求
        //SpriteAtlasManager.atlasRequested += (s, action) =>
        //{
        //Debug.LogError("请求了图集" + s);
        //};
    }

    private async UniTask<ResourcePackage> _LoadPackage(string packageName) {
        // 初始化资源系统
        // YooAssets.Initialize();
        // 创建默认的资源包
        this.packageName = packageName;

        if (YooAssets.HasPackage(packageName)) {
            return YooAssets.GetPackage(packageName);
        }
        var package = YooAssets.CreatePackage(packageName);

#if UNITY_WEBGL
            YooAssets.SetCacheSystemDisableCacheOnWebGL();
#endif
        await InitializeYooAsset();
        return package;
    }


    private IEnumerator InitializeYooAsset() {
        var package = YooAssets.GetPackage(packageName);

        InitializationOperation initializationOperation = null;
        switch (PlayMode) {
            case EPlayMode.HostPlayMode:
                string defaultHostServer = GetHostServerURL();
                string fallbackHostServer = GetHostServerURL();
                var initParametersHost = new HostPlayModeParameters();
                //分发资源查询
                initParametersHost.DeliveryQueryServices = new DefaultDeliveryQueryServices();
                //内置资源查询接口
                //initParametersHost.BuildinQueryServices = new GameQueryServices();
                // initParametersHost.DecryptionServices = new GameDecryptionServices();
                //远端服务器查询服务接口
                initParametersHost.RemoteServices = new RemoteServices(defaultHostServer, fallbackHostServer);
                initializationOperation = package.InitializeAsync(initParametersHost);
                break;
            case EPlayMode.WebPlayMode:
                string defaultHostServerWeb = GetHostServerURL();
                string fallbackHostServerWeb = GetHostServerURL();
                var initParametersWeb = new WebPlayModeParameters();
                //initParametersWeb.BuildinQueryServices = new GameQueryServices(); //太空战机DEMO的脚本类，详细见StreamingAssetsHelper
                initParametersWeb.RemoteServices = new RemoteServices(defaultHostServerWeb, fallbackHostServerWeb);
                initializationOperation = package.InitializeAsync(initParametersWeb);
                break;
            default:
                var initParameters = new EditorSimulateModeParameters();
                initParameters.SimulateManifestFilePath = EditorSimulateModeHelper.SimulateBuild(packageName);
                initializationOperation = package.InitializeAsync(initParameters);
                break;
        }

        yield return initializationOperation;
        if (initializationOperation.Status == EOperationStatus.Succeed) {
            Debug.Log("资源包初始化成功！");
        } else {
            Debug.LogError($"资源包初始化失败：{initializationOperation.Error}");
        }

        yield return UpdatePackageVersion();
    }

    private IEnumerator UpdatePackageVersion() {
        var package = YooAssets.GetPackage(packageName);
        var operation = package.UpdatePackageVersionAsync();
        yield return operation;

        if (operation.Status == EOperationStatus.Succeed) {
            //更新成功
            updateVersion = operation.PackageVersion;
            Debug.Log($"Updated package Version : {updateVersion}");
            yield return UpdatePackageManifest();
        } else {
            //更新失败
            Debug.LogError(operation.Error);
        }
    }

    private IEnumerator UpdatePackageManifest() {
        // 更新成功后自动保存版本号，作为下次初始化的版本。
        // 也可以通过operation.SavePackageVersion()方法保存。
        var package = YooAssets.GetPackage(packageName);
        var operation = package.UpdatePackageManifestAsync(updateVersion);
        yield return operation;

        if (operation.Status == EOperationStatus.Succeed) {
            //更新成功
            Debug.Log(packageName + "更新成功");
        } else {
            //更新失败
            Debug.LogError(operation.Error);
        }
#if !UNITY_WEBGL
        yield return Download();
#else
            downloadSize = totalSize;
#endif
    }

    IEnumerator Download() {
        // yield return new WaitForSecondsRealtime(0.5f);
        int downloadingMaxNum = 10;
        int failedTryAgain = 3;
        var package = YooAssets.GetPackage(packageName);
        var downloader = package.CreateResourceDownloader(downloadingMaxNum, failedTryAgain);

        //没有需要下载的资源
        if (downloader.TotalDownloadCount == 0) {
            Debug.Log("没有需要下载的");
            downloadSize = totalSize;
            // OnUpdateCompile();
            yield break;
        }

        // StartProgress();
        //需要下载的文件总数和总大小
        // int totalDownloadCount = downloader.TotalDownloadCount;
        // long totalDownloadBytes = downloader.TotalDownloadBytes;    

        //注册回调方法
        downloader.OnDownloadErrorCallback = OnDownloadErrorFunction;
        downloader.OnDownloadProgressCallback = OnDownloadProgressUpdateFunction;
        downloader.OnDownloadOverCallback = OnDownloadOverFunction;
        downloader.OnStartDownloadFileCallback = OnStartDownloadFileFunction;

        //开启下载
        downloader.BeginDownload();
        yield return downloader;

        //检测下载结果
        if (downloader.Status == EOperationStatus.Succeed) {
            //下载成功
            Debug.Log("下载完成");
            downloadSize = totalSize;
            // OnUpdateCompile();
        } else {
            //下载失败
            Debug.LogError("下载失败");
        }
    }

    public void OnDownloadOverFunction(bool isSucceed) {
    }

    public void OnDownloadProgressUpdateFunction(int totalDownloadCount, int currentDownloadCount,
        long totalDownloadBytes, long currentDownloadBytes) {
        downloadSize = currentDownloadBytes;
        totalSize = totalDownloadBytes;
        EventDownloadUpdate?.Invoke(currentDownloadBytes, totalDownloadBytes);
        Debug.Log("下载中" + downloadSize + "/" + totalSize);
    }

    public void OnDownloadErrorFunction(string fileName, string error) {
        Debug.LogError("下载失败" + fileName);
    }

    public void OnStartDownloadFileFunction(string fileName, long sizeBytes) {
        Debug.Log("开始下载" + fileName);
    }

    /// <summary>
    /// 获取资源服务器地址
    /// </summary>
    private string GetHostServerURL() {
        //string hostServerIP = "http://10.0.2.2"; //安卓模拟器地址

        string p = PlayerPrefs.GetString("ip", "");
        string hostServerIP = p != "" ? $"http://{p}:7878" : _config.HotIpAddress;
        string appVersion = _config.Version;

#if UNITY_EDITOR
        if (UnityEditor.EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android)
            return $"{hostServerIP}/CDN/Android/{appVersion}";
        else if (UnityEditor.EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.iOS)
            return $"{hostServerIP}/CDN/IPhone/{appVersion}";
        else if (UnityEditor.EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.WebGL)
            return $"{hostServerIP}/CDN/WebGL/{appVersion}";
        else
            return $"{hostServerIP}/CDN/PC/{appVersion}";
#else
		if (Application.platform == RuntimePlatform.Android)
			return $"{hostServerIP}/CDN/Android/{appVersion}";
		else if (Application.platform == RuntimePlatform.IPhonePlayer)
			return $"{hostServerIP}/CDN/IPhone/{appVersion}";
		else if (Application.platform == RuntimePlatform.WebGLPlayer)
			return $"{hostServerIP}/CDN/WebGL/{appVersion}";
		else
			return $"{hostServerIP}/CDN/PC/{appVersion}";
#endif
    }


    /// <summary>
    /// 远端资源地址查询服务类
    /// </summary>
    private class RemoteServices : IRemoteServices {
        private readonly string _defaultHostServer;
        private readonly string _fallbackHostServer;

        public RemoteServices(string defaultHostServer, string fallbackHostServer) {
            _defaultHostServer = defaultHostServer;
            _fallbackHostServer = fallbackHostServer;
        }

        string IRemoteServices.GetRemoteMainURL(string fileName) {
            return $"{_defaultHostServer}/{fileName}";
        }

        string IRemoteServices.GetRemoteFallbackURL(string fileName) {
            return $"{_fallbackHostServer}/{fileName}";
        }
    }

    /// <summary>
    /// 默认的分发资源查询服务类
    /// </summary>
    private class DefaultDeliveryQueryServices : IDeliveryQueryServices {
        public DeliveryFileInfo GetDeliveryFileInfo(string packageName, string fileName) {
            throw new NotImplementedException();
        }

        public bool QueryDeliveryFiles(string packageName, string fileName) {
            return false;
        }
    }
}