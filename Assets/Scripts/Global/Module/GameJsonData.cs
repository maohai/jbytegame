using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game
{
    //所有数据
    public class GameJsonData
    {
        private Dictionary<string, List<object>> data = new Dictionary<string, List<object>>();
        //private Dictionary<string, LocalizationData> _localizationDatas = new Dictionary<string, LocalizationData>();

        public GameJsonData(string content)
        {
            LoadData(content);
        }

        private void LoadData(string content)
        {
            JObject jo = JObject.Parse(content);
            jo = CustomParse(jo);
            LoadAllData(jo);

            //_localizationDatas = ListToDictionary<string, LocalizationData>("Key");
        }

        public List<T> GetData<T>()
        {
            string strT = typeof(T).ToString();
            if (data.ContainsKey(strT))
            {
                return data[strT].OfType<T>().ToList();
            }

            Debug.LogError($"没有{strT}类型");
            return null;
        }

        //public string GetLocalization(string key, Language language = Language.Zh)
        //{
        //    if (!_localizationDatas.ContainsKey(key))
        //    {
        //        return key;
        //    }

        //    var l = _localizationDatas[key];

        //    switch (language)
        //    {
        //        case Language.Zh_CN:
        //            return l.Zh_CN;
        //        case Language.En:
        //            return l.En;
        //        case Language.Zh:
        //            return l.Zh;
        //        default:
        //            Debug.LogError("No Language " + language);
        //            return key;
        //    }
        //}

        private void LoadAllData(JObject db)
        {
            data.Clear();
            foreach (var keyValuePair in db)
            {
                List<JToken> token = keyValuePair.Value?.ToList();

                string className = $"Game.{keyValuePair.Key}Data";
                Type t = Type.GetType(className);
                if (t != null)
                {
                    var constructor = t.GetConstructor((new Type[] { typeof(JObject) }));
                    List<object> objs = new List<object>();
                    foreach (var jToken in token)
                    {
                        var d = constructor?.Invoke(new object[] { jToken });
                        objs.Add(d);

                    }
                    data.Add(className, objs);
                }
                else
                {
                    Debug.LogError($"没有{className}类型");
                }
            }
        }

        private JObject CustomParse(JObject jo)
        {
            JObject newJo = new JObject();
            foreach (var keyValuePair in jo)
            {
                JArray data = keyValuePair.Value.ToObject<JArray>();

                string[] names = data[0].ToObject<string[]>(); //属性名
                JArray j = new JArray();
                for (int y = 1; y < data.Count; y++)
                {
                    JObject d = new JObject();
                    for (int x = 0; x < names.Length; x++)
                    {
                        d[names[x]] = data[y].ToObject<JArray>()[x];
                    }

                    j.Add(d);
                }

                newJo[keyValuePair.Key] = j;
            }

            return newJo;
        }


        /// <summary>
        /// list转字典
        /// </summary>
        /// <param name="keyName">key值</param>
        /// <typeparam name="T">表数据类型</typeparam>
        /// <returns></returns>
        public Dictionary<D, T> List2Dic<D, T>(string keyName)
        {
            var t = typeof(T);
            var d = GetData<T>();
            if (d != null)
            {
                var propertyInfo = t.GetProperty(keyName);
                if (propertyInfo == null)
                {
                    Debug.LogError($"{t.Name}不存在{keyName}键");
                    return null;
                }
                Dictionary<D, T> result = new Dictionary<D, T>();
                foreach (var x1 in d)
                {
                    D k = (D)propertyInfo.GetValue(x1);
                    if (result.ContainsKey(k))
                    {
                        Debug.LogError("重复主键" + k);
                    }
                    else
                    {
                        result.Add(k, x1);
                    }
                }

                return result;
            }
            return null;
        }
    }
}