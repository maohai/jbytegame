using Cysharp.Threading.Tasks;
using Game;
using LitJson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using YooAsset;

public class Client : SingletonAutoMono<Client>
{
    public override void Init()
    {
        InitModules();
        InitLoad();
    }

    private int curLoadIndex = 0; //当前加载第几个模块
    private List<IMod> _mods = new List<IMod>();
    private bool isLoadComplete;

    public GameJsonData Data;

    private BaseModule Bag = new ();
    private BaseModule Dialog = new();
    private BaseModule Player = new();
    private BaseModule Game = new();

    private ClientConfig _config;

    /// <summary>
    /// 所有模块加载完成事件
    /// </summary>
    public event Action OnLoadAllModulesFinish;
    /// <summary>
    /// 配置信息
    /// </summary>
    public ClientConfig Config => _config;

    private void InitModules()
    {
        //_config = Resources.Load<ClientConfig>("ClientConfig");
        _mods.Add(Bag);
        _mods.Add(Dialog);
        _mods.Add(Player);
        _mods.Add(Game);
    }

    private async void InitLoad()
    {
        //加载持久化数据
        JObject saveData = new JObject();
        var savePath = GetSavePath();
        if (FileExist(savePath))
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            FileStream fs = new FileStream(savePath, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            long start = br.BaseStream.Position;
            //获取数据长度
            br.BaseStream.Seek(0, SeekOrigin.End);
            long end = br.BaseStream.Position;
            int byteCount = (int)(end - start);
            br.BaseStream.Seek(0, SeekOrigin.Begin);
            byte[] b = br.ReadBytes(byteCount);
            br.Close();
            fs.Close();
#else
                WXFileSystemManager fs = WX.GetFileSystemManager();
                Debug.LogError("读取文件" + savePath);
                byte[] b=new byte[0];
                bool isComplete = false;
                fs.ReadFile(new ReadFileParam()
                {
                    success = (s) => { b = s.binData; },
                    fail = f=>{Debug.LogError("读取失败"+f);},
                    complete = (e) =>
                    {
                        isComplete = true;
                    }
                });
                await UniTask.WaitUntil(() => isComplete);
#endif
            try
            {
                using (StreamReader sr = new StreamReader(savePath))
                {
                    var firstLine = sr.ReadLine();
                    if (firstLine[0] != '{') //加密过，需加密读取
                    {
                        b = AESDecrypt(b, _config.Key);
                        Debug.Log("本地缓存加密过，使用加密读取");
                    }
                    else
                    {
                        Debug.Log("本地缓存无加密直接读取");
                    }
                }

                string data = Encoding.UTF8.GetString(b);
                saveData = JObject.Parse(data);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                saveData = new JObject();
            }
        }
        //读取数据
        YooAssets.Initialize();
        var package = await YooAssetManager.LoadPackage("Main");
        var d = package.LoadRawFileAsync("Data");
        await d.Task;
        //初始化所有游戏数据
        Data = new GameJsonData(d.GetRawFileText());
        //加载数据
        GameManager.Ins.Init();
        //加载所有模块
        StartCoroutine(LoadModules(saveData));
    }
    //递归初始化模块
    private IEnumerator LoadModules(JObject saveData)
    {
        long startTimestamp = GetCurTimestampMilliseconds();
        foreach (var mod in _mods)
        {
            StartCoroutine(LoadSingleModule(mod, saveData));
        }

        yield return new WaitUntil(() => curLoadIndex >= _mods.Count);
        OnInitModuleCompile();
        Debug.Log("所有模块初始化完成：" + (GetCurTimestampMilliseconds() - startTimestamp) + "ms");
    }
    public long GetCurTimestampMilliseconds()
    {
        return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }
    private IEnumerator LoadSingleModule(IMod mod, JObject saveData)
    {
        bool isFinish = false;
        var modName = mod.GetType().Name;
        if (saveData == null)
        {
            curLoadIndex++; 
            yield break;
        }
        //保存的模块数据
        JObject d = (JObject)saveData[modName];
        //开始时间
        long startTimestamp = GetCurTimestampMilliseconds();
        mod.Init(d, () =>
        {
            isFinish = true;
            //结束时间
            long endTimestamp = GetCurTimestampMilliseconds();
            Debug.Log($"加载模块{modName} 耗时{endTimestamp - startTimestamp}ms");
        });
        yield return new WaitUntil(() => isFinish);
        curLoadIndex++;
    }
    private void OnInitModuleCompile()
    {
        isLoadComplete = true;
        OnLoadAllModulesFinish?.Invoke();
    }

    private string GetSavePath()
    {
#if UNITY_EDITOR
        string folderPath = Path.Combine(Application.persistentDataPath, "Editor");
        if (!Directory.Exists(folderPath))
        {
            // 如果文件夹不存在，则创建文件夹
            Directory.CreateDirectory(folderPath);
        }

        return Path.Combine(folderPath, "SaveData");
#elif UNITY_WEBGL
            return Path.Combine(WX.env.USER_DATA_PATH, "SaveData");

#endif
        return Path.Combine(Application.persistentDataPath, "SaveData");
    }

    private bool FileExist(string path)
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        return File.Exists(path);
#else
            var fs = WX.GetFileSystemManager();
            var result = fs.AccessSync(path);
            Debug.LogError("结果"+result);
            return fs.AccessSync(path).Equals("access:ok");
#endif
    }

    // 解密字节数组
    /// <summary>
    /// AES 解密(高级加密标准，是下一代的加密算法标准，速度快，安全级别高，目前 AES 标准的一个实现是 Rijndael 算法)
    /// </summary>
    /// <param name="DecryptString">待解密密文</param>
    /// <param name="DecryptKey">解密密钥</param>
    private byte[] AESDecrypt(byte[] DecryptByte, string DecryptKey)
    {
        if (DecryptByte.Length == 0)
        {
            throw (new Exception("密文不得为空"));
        }

        if (string.IsNullOrEmpty(DecryptKey))
        {
            throw (new Exception("密钥不得为空"));
        }

        byte[] m_strDecrypt;
        byte[] m_btIV = Convert.FromBase64String("Rkb4jvUy/ye7Cd7k89QQgQ==");
        byte[] m_salt = Convert.FromBase64String("gsf4jvkyhye5/d7k8OrLgM==");
        Rijndael m_AESProvider = Rijndael.Create();
        try
        {
            MemoryStream m_stream = new MemoryStream();
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(DecryptKey, m_salt);
            ICryptoTransform transform = m_AESProvider.CreateDecryptor(pdb.GetBytes(32), m_btIV);
            CryptoStream m_csstream = new CryptoStream(m_stream, transform, CryptoStreamMode.Write);
            m_csstream.Write(DecryptByte, 0, DecryptByte.Length);
            m_csstream.FlushFinalBlock();
            m_strDecrypt = m_stream.ToArray();
            m_stream.Close();
            m_stream.Dispose();
            m_csstream.Close();
            m_csstream.Dispose();
        }
        catch (IOException ex)
        {
            throw ex;
        }
        catch (CryptographicException ex)
        {
            throw ex;
        }
        catch (ArgumentException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            m_AESProvider.Clear();
        }

        return m_strDecrypt;
    }
}
