using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMod
{
    void Init(JObject saveData, Action cb);

    JObject GetSaveData();
}
public class BaseModule : IMod
{
    public virtual JObject GetSaveData()
    {
        return null;
    }

    public virtual void Init(JObject saveData, Action cb)
    {
        cb?.Invoke();
    }
}
public class BagModule : BaseModule
{

}
public class DialogModule : BaseModule { }
public class PlayerModule : BaseModule { }
public class GameModule : BaseModule { }
