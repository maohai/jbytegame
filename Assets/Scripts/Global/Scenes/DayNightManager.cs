using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game {
    public class DayNightManager : SingletonMono<DayNightManager> {
        // 控制方向光的引用
        public Light directionalLight;
        public float dayDurationInSeconds = 24.0f; // 调整一整天的持续时间（以秒为单位）
        public int currentHour;// 当前的小时
        public int currentMinute;// 当前的分钟
        float currentTimeOfDay = 0.35f;// 当前时间在一天中所占比例（范围在0到1之间）
        public List<SkyboxTimeMapping> timeMappings;// 存储不同时间段对应的天空盒
        float blendedValue = 0.0f;// 用于插值的值（范围在0到1之间）
        bool lockNextDayTrigger = false; // 是否锁定下一个白天触发
        public TextMeshProUGUI timeUI;// 用于显示时间的UI元素
        public Renderer MapRender;

        // 在每帧更新
        private int hourUpdate = -1;
        void Update() {
            // 根据游戏时间计算当前的时间
            currentTimeOfDay += Time.deltaTime / dayDurationInSeconds;
            currentTimeOfDay = currentTimeOfDay % 1; // 确保值在0到1之间

            // 计算当前的小时
            currentHour = Mathf.FloorToInt(currentTimeOfDay * 24);
            // 计算当前的分钟数
            currentMinute = Mathf.FloorToInt(currentTimeOfDay * 1440) % 60; // 一天有 24 小时 * 60 分钟 = 1440 分钟
                                                                            //更新时间UI
                                                                            // timeUI.text = $"{currentHour:D2}:{currentMinute:D2}";

            // 更新方向光的旋转
            directionalLight.transform.rotation = Quaternion.Euler(new Vector3((currentTimeOfDay * 360) - 90, 170, 0));

            if (hourUpdate != currentHour) {
                hourUpdate = currentHour;
                MapRender.material.SetInt("_Hour", hourUpdate);
            }
            // 根据时间更新天空盒材质
            // UpdateSkybox();
        }

        // 更新天空盒材质的方法
        private void UpdateSkybox() {
            // 寻找当前小时对应的天空盒材质
            Material currentSkybox = null;
            foreach (SkyboxTimeMapping mapping in timeMappings) {
                if (currentHour == mapping.hour) {
                    currentSkybox = mapping.skyboxMaterial;
                    if (currentSkybox.shader != null) {
                        if (currentSkybox.shader.name == "Custom/SkyboxTransition") {
                            blendedValue += Time.deltaTime;
                            blendedValue = Mathf.Clamp01(blendedValue);
                            currentSkybox.SetFloat("_TransitionFactor", blendedValue);
                        } else {
                            blendedValue = 0;
                        }
                    }
                    break;
                }
            }
            if (currentHour == 0 && lockNextDayTrigger == false) {
                // TimeManager.Instance.TriggerNextDay();
                lockNextDayTrigger = true;
            }
            if (currentHour != 0) {
                lockNextDayTrigger = false;
            }

            // 如果找到了对应的天空盒材质，则更新RenderSettings的skybox
            if (currentSkybox != null) {
                RenderSettings.skybox = currentSkybox;
            }
        }
    }

    // 存储时间和对应天空盒材质的类
    [System.Serializable]
    public class SkyboxTimeMapping {
        public string phaseName;
        public int hour; // 小时
        public Material skyboxMaterial; // 对应的天空盒材质
    }


}