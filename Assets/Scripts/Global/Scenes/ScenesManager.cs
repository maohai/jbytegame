using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// 场景切换模块
/// </summary>
namespace Game {
    public class ScenesManager : Singleton<ScenesManager> {
        public void LoadScene(string name, UnityAction fun) {
            SceneManager.LoadScene(name);
            fun();
        }

        public void LoadSceneAsyn(string name, UnityAction fun) {
            Utility.Ins.StartCoroutine(ReallyLoadSceneAsyn(name, fun));
        }

        private IEnumerator ReallyLoadSceneAsyn(string name, UnityAction fun) {
            float currentProgress = 0;

            AsyncOperation ao = SceneManager.LoadSceneAsync(name);
            ao.allowSceneActivation = false;


            while (currentProgress < 0.9f) {
                Debug.Log("场景加载中~~~~");
                currentProgress = ao.progress;

                yield return new WaitForSeconds(0.1f);
                yield return currentProgress;
            }


            while (!ao.isDone) {
                Debug.Log("场景加载完成!");
                ao.allowSceneActivation = true;

                yield return ao.progress;
            }

            fun();
        }
    }
}