using System;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class EventManager : Singleton<EventManager> {

    #region 事件驱动链

    public readonly EventGroup MouseLeftClickDownGroup = new();
    public readonly EventGroup MouseLeftClickUpGroup = new();
    public readonly EventGroup MouseLeftContinueClickGroup = new();
    public readonly EventGroup MouseRightClickDownGroup = new();
    public readonly EventGroup MouseRightClickUpGroup = new();
    public readonly EventGroup MouseContinuouseDetectionGroup = new(); //鼠标持续检测
    public readonly EventGroup PlayerMoveGroup = new();
    public readonly EventGroup KeyRClickDownGroup = new();
    public readonly EventGroup StateDetectionGroup = new();

    #region UI相关

    public readonly EventGroup RefreshBagGroup = new();

    #endregion

    #region 游戏相关

    public void AddPlayerMove(Action<IEventMessage, object[]> listener) {
        PlayerMoveGroup.AddListener<PlayerMoveMessage>(listener);
    }
    /// <summary>
    /// 移除事件
    ///       EventManager.RemoveListener(new MouseLeftClick(),testevent);
    /// </summary>
    /// <param name="t"></param>
    /// <param name="listener"></param>
    public void RemovePlayerMove(Action<IEventMessage, object[]> listener) {
        PlayerMoveGroup.RemoveListener(typeof(PlayerMoveMessage), listener);
    }
    public void AddMouseContinuouseDetection(Action<IEventMessage, object[]> listener) {
        MouseContinuouseDetectionGroup.AddListener<MouseContinueDetectionMessage>(listener);
    }
    public void RemoveMouseContinuouseDetection(Action<IEventMessage, object[]> listener) {
        PlayerMoveGroup.RemoveListener(typeof(MouseContinueDetectionMessage), listener);
    }
    public void AddMouseLeftDownClick(Action<IEventMessage, object[]> listener) {
        MouseLeftClickDownGroup.AddListener<MouseLeftClickDownMessage>(listener);
    }
    public void RemoveMouseLeftDownClick(Action<IEventMessage, object[]> listener) {
        MouseLeftClickDownGroup.RemoveListener(typeof(MouseLeftClickDownMessage), listener);
    }
    public void AddMouseLeftUpClick(Action<IEventMessage, object[]> listener) {
        MouseLeftClickUpGroup.AddListener<MouseLeftClickUpMessage>(listener);
    }
    public void RemoveMouseLeftUpClick(Action<IEventMessage, object[]> listener) {
        MouseLeftClickUpGroup.RemoveListener(typeof(MouseLeftClickUpMessage), listener);
    }
    public void AddMouseLeftContinueClick(Action<IEventMessage, object[]> listener) {
        MouseLeftContinueClickGroup.AddListener<MouseLeftContinueClickMessage>(listener);
    }
    public void RemoveMouseLeftContinueClick(Action<IEventMessage, object[]> listener) {
        MouseLeftContinueClickGroup.RemoveListener(typeof(MouseLeftContinueClickMessage), listener);
    }
    public void AddMouseRightDownClick(Action<IEventMessage, object[]> listener) {
        MouseRightClickDownGroup.AddListener<MouseRightClickDownMessage>(listener);
    }
    public void RemoveMouseRightDownClick(Action<IEventMessage, object[]> listener) {
        MouseRightClickDownGroup.RemoveListener(typeof(MouseRightClickDownMessage), listener);
    }
    public void AddMouseRightUpClick(Action<IEventMessage, object[]> listener) {
        MouseRightClickUpGroup.AddListener<MouseRightClickUpMessage>(listener);
    }
    public void RemoveMouseRightUpClick(Action<IEventMessage, object[]> listener) {
        MouseRightClickUpGroup.RemoveListener(typeof(MouseRightClickUpMessage), listener);
    }
    public void AddKeyRDownClick(Action<IEventMessage, object[]> listener) {
        KeyRClickDownGroup.AddListener<KeyRClickDownMessage>(listener);
    }
    public void RemoveKeyRDownClick(Action<IEventMessage, object[]> listener) {
        KeyRClickDownGroup.RemoveListener(typeof(KeyRClickDownMessage), listener);
    }

    #endregion

    #region UI相关

    public void AddRefreshBag(Action<IEventMessage, object[]> listener) {
        RefreshBagGroup.AddListener<RefreshBagMessage>(listener);
    }
    public void RemoveRefreshBag(Action<IEventMessage, object[]> listener) {
        RefreshBagGroup.RemoveListener(typeof(RefreshBagMessage), listener);
    }

    #endregion

    #endregion

    #region 单体事件

    private Dictionary<int, Action<ClientEvent, object[]>> evtMap =
        new Dictionary<int, Action<ClientEvent, object[]>>();

    public void Register(ClientEvent evt, Action<ClientEvent, object[]> listener) {
        if (!evtMap.ContainsKey(evt.ID)) {
            evtMap[evt.ID] = listener;
        } else {
            evtMap[evt.ID] += listener;
        }
    }

    public void Remove(ClientEvent evt, Action<ClientEvent, object[]> listener) {
        if (evtMap.ContainsKey(evt.ID)) {
            evtMap[evt.ID] -= listener;
        }
    }

    public void RemoveAll(ClientEvent evt) {
        evtMap.Remove(evt.ID);
    }

    public void RemoveAll() {
        evtMap.Clear();
    }

    public void Trigger(ClientEvent evt, params object[] args) {
        evtMap.TryGetValue(evt.ID, out var cb);
        try {
            cb?.Invoke(evt, args);
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    #endregion

    #region 游戏单体事件

    public static readonly ClientEvent OnContinueFire = new();  //持续开火事件
    public static readonly ClientEvent OnInArmEvent = new();    //进入瞄准事件
    public static readonly ClientEvent OnOutArmEvent = new();   //退出瞄准事件
    public static readonly ClientEvent OnUpdateRefresh = new(); //瞄准刷新事件
    public static readonly ClientEvent OnBuildFinisEvent = new();   //建造完成事件
    public static readonly ClientEvent OnEnterBuildEvent = new();   //进入建造事件
    public static readonly ClientEvent OnBeginBuildEvent = new();   //开始建造事件

    #endregion

    #region UI单体事件

    public static readonly ClientEvent OnRefreshWeapon = new();
    public static readonly ClientEvent OnEquitWeapon = new();

    #endregion

    #region 按键相关

    public readonly static ClientEvent OnEventEClick = new();
    public readonly static ClientEvent OnEventQClick = new();

    #endregion
}

public class ClientEvent {
    private static int EvtID = 0;

    public readonly int ID = ++EvtID;

    public void Register(Action<ClientEvent, object[]> listener) {
        if (EventManager.Ins != null) {
            EventManager.Ins.Register(this, listener);
        }
    }

    public void Remove(Action<ClientEvent, object[]> listener) {
        if (EventManager.Ins != null) {
            EventManager.Ins.Remove(this, listener);
        }
    }

    public void RemoveAll() {
        if (EventManager.Ins != null) {
            EventManager.Ins.RemoveAll(this);
        }
    }

    public void Trigger(params object[] args) {
        if (EventManager.Ins != null) {
            EventManager.Ins.Trigger(this, args);
        }
    }
}
