using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EventGroup
{
    private readonly Dictionary<System.Type, List<Action<IEventMessage,object[]>>> _cachedListener = new ();

    /// <summary>
    /// 添加一个监听
    /// </summary>
    public void AddListener<TEvent>(System.Action<IEventMessage,object[]> listener) where TEvent : IEventMessage
    {
        System.Type eventType = typeof(TEvent);
        if (_cachedListener.ContainsKey(eventType) == false)
            _cachedListener.Add(eventType, new List<Action<IEventMessage,object[]>>());

        if (_cachedListener[eventType].Contains(listener) == false)
        {
            _cachedListener[eventType].Add(listener);
            UniEvent.AddListener(eventType, listener);
        }
        else
        {
            Debug.Log($"Event listener is exist : {eventType}");
        }
    }

    /// <summary>
    /// 移除所有缓存的监听
    /// </summary>
    public void RemoveAllListener()
    {
        foreach (var pair in _cachedListener)
        {
            System.Type eventType = pair.Key;
            for (int i = 0; i < pair.Value.Count; i++)
                UniEvent.RemoveListener(eventType, pair.Value[i]);
            pair.Value.Clear();
        }
        _cachedListener.Clear();
    }

    public void RemoveListener(Type t,System.Action<IEventMessage,object[]> listnenr){
        if (_cachedListener.ContainsKey(t)){
            int index = _cachedListener[t].IndexOf(listnenr);  
            if (index != -1){
                _cachedListener[t].RemoveAt(index);
            }
        }
        UniEvent.RemoveListener(t, listnenr);
    }
    
}