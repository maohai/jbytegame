using UnityEngine;

/// <summary>
/// 将位置抽象成接口,嘎嘎存储
/// </summary>
public interface IUIPos{
    public Transform TransHP{ get; set; }
    public Transform TransFloatText{ get; set; }
    public Transform TransPickUp{ get; set; }
}
