using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Game;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// UI堆栈类型
/// </summary>
public enum UIShowType{
    Over,       //覆盖,不可以重复弹,需要手动触发弹出逻辑
    Repeat,     //重复弹,自动关闭,第一次弹出需要手动触发,然后全部弹出
}

public class UIManager : Singleton<UIManager>
{
    #region UI队列

    private Dictionary<UIShowType, Queue<Action>> _uiShow = new();//队列

    /// <summary>
    /// 游戏初始化
    /// </summary>
    public void GameEntry(){
        BeginDialog.Show();
    }

    /// <summary>
    /// 入队
    /// </summary>
    /// <param name="cb"></param>
    public void OverShow(Action cb){
        //当队列没有over或者队列为空的时候立刻弹出
        bool canDe = !_uiShow.ContainsKey(UIShowType.Over)|(_uiShow.ContainsKey(UIShowType.Over)&&_uiShow[UIShowType.Over].Count == 0) ;
        EnqueueUI(cb);
        //立刻出队
        if(canDe) DequeueOverUI(true);
    }
    /// <summary>
    /// 重复入队
    /// </summary>
    /// <param name="cb"></param>
    public void RepeatShow(Action cb){
        EnqueueUI(cb,UIShowType.Repeat);
    }
    /// <summary>
    /// 根据类型入队
    /// </summary>
    /// <param name="cb"></param>
    /// <param name="type"></param>
    public void EnqueueUI(Action cb, UIShowType type = UIShowType.Over){
        if (_uiShow.ContainsKey(type)){
            _uiShow[type].Enqueue(cb);
        }
        else{
            _uiShow.Add(type,new Queue<Action>());
            _uiShow[type].Enqueue(cb);
        }
    }
    /// <summary>
    /// 出队
    /// </summary>
    public void DequeueOverUI(bool immediately = false){
        if (!_uiShow.ContainsKey(UIShowType.Over)){
            Debug.LogError("无法出队,字典为空");
            return;
        }

        _uiShow[UIShowType.Over].TryDequeue(out Action cb);
        if (immediately)
            cb?.Invoke();
        else{
            Utility.Ins.Delay(() => {
                cb?.Invoke();
            },0.8f);
        }
    }
    /// <summary>
    /// 重复出队
    /// </summary>
    public void DequeueRepeatUI(){
        if (!_uiShow.ContainsKey(UIShowType.Repeat)){
            Debug.LogError("无法出队,字典为空");
            return;
        }

        Utility.Ins.StartCoroutine(DequeueRepeatUICoroutine());
    }
    private const float waitForDequeueRepeatTime = 2f;
    private WaitForSeconds waitForDequeueRepeat = new (waitForDequeueRepeatTime);
    private IEnumerator DequeueRepeatUICoroutine(){
        while (_uiShow[UIShowType.Repeat].TryDequeue(out Action cb)){
            cb?.Invoke();
            yield return waitForDequeueRepeat;
        }
    }

    #endregion

    #region UI启用禁用

    public Camera UICamera;
    /// <summary>
    /// 基础画布
    /// </summary>
    private Canvas Canvas;
    /// <summary>
    /// 基础画布空间
    /// </summary>
    public RectTransform TransCanvas;
    /// <summary>
    /// 其他UI显示
    /// </summary>
    public RectTransform TransElementUI;
    /// <summary>
    /// 最底层的UI
    /// </summary>
    public UIBehaviour MainUI => PlayerDialog.Ins;//自定义

    /// <summary>
    /// UI不同层
    /// </summary>
    public int UILayerIndex = 0;
    public int UILayerRepeatIndex = 0;
    /// <summary>
    /// UI管理
    /// </summary>
    public Dictionary<string,UIBehaviour> _dialog= new Dictionary<string,UIBehaviour>();
    /// <summary>
    /// UI父物体
    /// </summary>
    private Transform Group;
    /// <summary>
    /// 创建根界面和UI专用摄像机
    /// </summary>
    public UIManager(){
        UICamera = CinemachineManager.CreateUICamere();
        GameObject obj = ResManager.Ins.Load<GameObject>("UI/Canvas");
        Canvas = obj.GetComponentInChildren<Canvas>();
        TransCanvas=obj.transform as RectTransform;
        GameObject.DontDestroyOnLoad(obj);
        Canvas.renderMode = RenderMode.ScreenSpaceCamera;
        (Canvas.transform as RectTransform).localScale = Vector3.one;
        Canvas.worldCamera = UICamera;
        Canvas.planeDistance = 0;

        Group = TransCanvas.Find("Group");
        TransElementUI = TransCanvas.Find("ElementUI").transform as RectTransform;
        
        obj = ResManager.Ins.Load<GameObject>("UI/EventSystem");
        GameObject.DontDestroyOnLoad(obj);
    }
    /// <summary>
    /// 显示面板
    /// </summary>
    /// <param name="panelName">面板名</param>
    /// <param name="layer">显示在哪一层</param>
    /// <param name="callback">预设体创建后执行的事情</param>
    public void Over(string name,object[] o = null,UnityAction callback=null){
        var dialogName = name.Split("/")[^1];
        if(_dialog.ContainsKey(dialogName))
        {
            _dialog[dialogName].OnUIShow(UIShowType.Over,o);
            if (callback != null)
                callback();
            return;
        }
        ResManager.Ins.LoadAsync<GameObject>("UI/" + name,obj =>
        {
            obj.transform.SetParent(Group);
            obj.transform.localScale= Vector3.one;
            (obj.transform as RectTransform).offsetMax = Vector2.zero;
            (obj.transform as RectTransform).offsetMin= Vector2.zero;
            (obj.transform as RectTransform).anchoredPosition = Vector2.zero;
            (obj.transform as RectTransform).sizeDelta = Vector2.zero;
            var d=obj.GetComponent<UIBehaviour>();
            if(callback!=null)
                callback();
            d.OnUIShow(UIShowType.Over,o);
            d.UILayerIndex = UILayerIndex += 20;
            _dialog.Add(dialogName, d);
        });
    }
    /// <summary>
    /// 出队
    /// </summary>
    /// <param name="name"></param>
    /// <param name="o"></param>
    /// <param name="callback"></param>
    public void Repeat(string name,object[] o = null,UnityAction callback=null){
        var dialogName = name.Split("/")[^1];
        if(_dialog.ContainsKey(dialogName))
        {
            _dialog[dialogName].OnUIShow(UIShowType.Repeat,o);
            if (callback != null)
                callback();
            Utility.Ins.Delay(() => _dialog[dialogName].Close(), (float) o[0]);
            return;
        }
        ResManager.Ins.LoadAsync<GameObject>("UI/" + name,obj => {
            obj.transform.SetParent(Group);
            obj.transform.localScale= Vector3.one;
            (obj.transform as RectTransform).offsetMax = Vector2.zero;
            (obj.transform as RectTransform).offsetMin= Vector2.zero;
            (obj.transform as RectTransform).anchoredPosition = Vector2.zero;
            (obj.transform as RectTransform).sizeDelta = Vector2.zero;
            var d=obj.GetComponent<UIBehaviour>();
            if(callback!=null)
                callback();
            d.OnUIShow(UIShowType.Repeat,o);
            Utility.Ins.Delay(() => d.Close(), (float) o[0]);
            _dialog.Add(dialogName, d);
        });
    }
    /// <summary>
    /// 隐藏面板
    /// </summary>
    public void Close(string name){
        if(_dialog.ContainsKey(name))
        {
            _dialog[name].UILayerIndex = UILayerIndex -= 20;
            _dialog[name].OnUIClose();
            GameObject.Destroy(_dialog[name].gameObject);
            _dialog.Remove(name);
        }
    }
    /// <summary>
    /// 关闭除了最底层的所有UI
    /// </summary>
    public void CloseAll()
    {
        var diaName = new List<string>();
        var mainName = MainUI.GetType().Name;
        foreach (var item in _dialog)
        {
            if (item.Key == mainName) continue;
            diaName.Add(item.Key);
        }
        foreach (var item in diaName)
        {
            Close(item);
        }
    }
    #endregion

    #region 游戏对象UI

    /// <summary>
    /// 一个UI只能对应一个对象,也就说他的数据
    /// </summary>
    private ConcurrentDictionary<BaseElementUI,ElementData> dicAllElementUI = new();
    private ConcurrentDictionary<string, Transform> dicUIParent = new();
    public ConcurrentDictionary<BaseElementUI, ElementData> DicAllElementUI{
        get => dicAllElementUI;
        private set{}
    }
    /// <summary>
    /// 添加角色的UI
    /// </summary>
    /// <param name="ui">ui类型</param>
    /// <param name="obj">角色</param>
    /// <param name="o">传参</param>
    public void AddElementUI(BaseElementUI ui,ElementData data,object[]o = null){
        if (!dicUIParent.ContainsKey(ui.name)){
            var newTran = new GameObject(ui.name).transform;
            newTran.SetParent(TransElementUI);
            //在挂载后面赋值,防止发生继承变化
            newTran.transform.localScale = Vector3.one;
            newTran.transform.position = Vector3.zero;

            dicUIParent.TryAdd(ui.name,newTran);
        }
        var u = GameObject.Instantiate(ui, dicUIParent[ui.name]);
        u.Init(data,o);
        dicAllElementUI.TryAdd(u,data);
    }
    /// <summary>
    /// 移除敌人相关UI,如血条
    /// </summary>
    /// <param name="ui"></param>
    public void RemoveElementUI(BaseElementUI ui){
        if (dicAllElementUI.ContainsKey(ui)){
            dicAllElementUI.TryRemove(ui,out var value);
            GameObject.Destroy(ui.gameObject);
        }
    }
    /// <summary>
    /// 更新敌人相关UI界面
    /// </summary>
    /// <param name="ui">需要更新的UI,例如血条</param>
    /// <param name="mono">更新谁的ui</param>
    /// <param name="o">传参</param>
    public void UpdateUIMono(Type ui,ElementData d,object[]o = null){
        var uiList = DicAllElementUI
            .Where(data => data.Key.GetType() == ui && data.Value == d)
            .Select(data=>data.Key);
        foreach (var u in uiList){
            u.RefreshUI();
        }
    }

    #endregion

    #region

    public GoodsType BuildCreateDialog_CurrentGoodsType = default;

    #endregion
}
