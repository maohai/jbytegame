using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public class UIElementManager : SingletonMono<UIElementManager> {
        private UIManager ui => UIManager.Ins;
        private void Update() {
            foreach (var item in ui.DicAllElementUI) {
                item.Key.UpdateUIPos();
            }
        }
    }

}