using System;
using UnityEngine;
using UnityEngine.Events;
public class UIBehaviour :MonoBehaviour{
    [HideInInspector]
    public int UILayerIndex = 0;
    /// <summary>
    /// UI显示时
    /// </summary>
    public virtual void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null){ }
    /// <summary>
    /// UI关闭时
    /// </summary>
    public virtual void OnUIClose(){ }
    public virtual void Close(){ }
}
public class BaseDialog<T> : UIBehaviour where T: BaseDialog<T>{
    public static T Ins { get; protected set; }
    protected static UIManager uimanager => UIManager.Ins;
    private CanvasGroup canvasGroup;
    private float alphaSpeed = 8;
    private UIShowType currentType;

    protected virtual void Awake(){
        canvasGroup = GetComponent<CanvasGroup>();
        if (canvasGroup == null)
            canvasGroup = gameObject.AddComponent<CanvasGroup>();
    }

    private int currentLayerIndex = 0;
    protected void Update(){
        if (currentLayerIndex != UILayerIndex){
            currentLayerIndex = UILayerIndex;
            //todo 刷新层级UI层级
        }        
    }

    /// <summary>
    /// UI显示时
    /// </summary>
    public override void OnUIShow(UIShowType type = UIShowType.Over, object[] o = null){
        Ins = (T) this;
        currentType = type;
    }

    /// <summary>
    /// UI关闭时
    /// </summary>
    public override void OnUIClose(){
        if (currentType == UIShowType.Over){
            UIManager.Ins.DequeueOverUI();
            if (Ins == this) 
                Ins = null;
        }
        else{
            if (Ins == this) 
                Ins = null;
        }
    }
    
    /// <summary>
    /// 关闭界面
    /// </summary>
    public override void Close(){
        uimanager.Close(GetType().Name);
    }
}