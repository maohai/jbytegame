using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 单例模式基类
/// </summary>
public class Singleton<T> where T:new()
{
    private static T ins;
    public static T Ins
    {
        get{
            if (ins == null)
                ins = new T();
            return ins;
        }
    }

    public virtual void Init(){
        
    }
}
