using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 音乐管理器
/// </summary>
namespace Game {
    public class MusicManager : Singleton<MusicManager> {
        private AudioSource bkMusic = null;
        private float bkValue = 1;

        //容器存储音效
        private GameObject soundObj= null;
        private List<AudioSource> soundList= new List<AudioSource>();
        private float soundValue = 1;


        private void Update() {
            for (int i = soundList.Count - 1; i >= 0; i--) {
                if (!soundList[i].isPlaying) {
                    GameObject.Destroy(soundList[i]);
                    soundList.RemoveAt(i);
                }
            }
        }
        /// <summary>
        /// 播放背景音乐
        /// </summary>
        /// <param name="name"></param>
        public void PlayBkMusic(string name) {
            if (bkMusic == null) {
                GameObject obj = new GameObject();
                obj.name = "BKMusic";
                bkMusic = obj.AddComponent<AudioSource>();
            }
            ResManager.Ins.LoadAsync<AudioClip>("Music/BK/" + name, (clip) => {
                bkMusic.clip = clip;
                bkMusic.loop = true;
                bkMusic.volume = bkValue;
                bkMusic.Play();
            });
        }

        /// <summary>
        /// 暂停背景音乐
        /// </summary>
        public void PauseBKMusic() {
            if (bkMusic == null) {
                return;
            }
            bkMusic.Pause();
        }

        /// <summary>
        /// 停止背景音乐
        /// </summary>
        public void StopBKMusic() {
            if (bkMusic == null) {
                return;
            }
            bkMusic.Stop();
        }

        /// <summary>
        /// 改变背景音乐大小
        /// </summary>
        /// <param name="v"></param>
        public void ChangeBKValue(float v) {
            bkValue = v;
            if (bkMusic != null) {
                return;
            }
            bkMusic.volume = bkValue;
        }

        /// <summary>
        /// 改变音效大小
        /// </summary>
        /// <param name="value">音量大小</param>
        public void ChangeSoundValue(float value) {
            soundValue = value;
            for (int i = 0; i < soundList.Count; i++) {
                soundList[i].volume = value;
            }
        }

        /// <summary>
        /// 播放音效
        /// </summary>
        /// <param name="name"></param>
        public void PlaySound(string name, bool isLoop, UnityAction<AudioSource> callback = null) {
            if (soundObj == null) {
                soundObj = new GameObject();
                soundObj.name = "Sound";
            }
            ResManager.Ins.LoadAsync<AudioClip>("Music/Sound/" + name, (clip) => {
                AudioSource source=soundObj.AddComponent<AudioSource>();
                source.clip = clip;
                source.loop = isLoop;
                source.volume = soundValue;
                source.Play();
                soundList.Add(source);
                if (callback != null)
                    callback(source);
            });
        }

        /// <summary>
        /// 停止音效
        /// </summary>
        public void StopSound(AudioSource source) {
            if (soundList.Contains(source)) {
                soundList.Remove(source);
                source.Stop();
                GameObject.Destroy(source);
            }
        }
    }
}
