using Cinemachine;
using Unity.VisualScripting;
using UnityEngine;

namespace Game {
    public class CinemachineManager : SingletonMono<CinemachineManager> {
        public Camera MainCamera => Camera.main;
        public Transform TransMainCamera => MainCamera.transform;
        private CinemachineBrain brain;

        private void OnEnable() {
            brain = TransMainCamera.AddComponent<CinemachineBrain>();
            //改为fixed,使其动画不卡顿
            brain.m_UpdateMethod = CinemachineBrain.UpdateMethod.FixedUpdate;
        }

        public const int CAM_DEPTH = 2;
        public const int CAM_LAYER = 5;
        public static Camera CreateUICamere() {
            var CameraObj = new GameObject("UICamera");
            Camera cam = CameraObj.AddComponent<Camera>();
            cam.transform.localPosition = Vector3.zero;
            cam.transform.localScale = Vector3.one /* *0.002777778f */;
            cam.depth = CAM_DEPTH;
            cam.cullingMask = 1 << CAM_LAYER;//显示层级
            cam.gameObject.layer = CAM_LAYER;//自身层级
            cam.clearFlags = CameraClearFlags.Depth;//清除模式为深度

            cam.orthographic = true;        //投射方式：orthographic正交//
            cam.orthographicSize = 100;       //投射区域大小//
            cam.nearClipPlane = -300;      //前距离//
            cam.farClipPlane = 300f;       //后距离//
            cam.rect = new Rect(0, 0, 1f, 1f);
            DontDestroyOnLoad(CameraObj);
            return cam;
        }

        public void RefreshSkyBox(Camera camera, string materialPath) {
            Skybox skybox;
            camera.TryGetComponent(out skybox);
            if (skybox == null)
                skybox = camera.transform.AddComponent<Skybox>();
            var mat = ResManager.Ins.Load<Material>(materialPath);
            skybox.material = mat;
        }

        /// <summary>
        /// 初始化相机为简单追踪
        /// </summary>
        /// <param name="follow"></param>
        /// <param name="lookAt">跟随</param>
        /// <param name="mode"></param>
        public void InitFollowCamSimpleFollow(
            Transform follow
            , Transform lookAt = null

            ) {
            var camComponent = TransMainCamera.AddComponent<CinemachineVirtualCamera>();
            camComponent.Follow = follow;
            camComponent.LookAt = lookAt;
            //设置vertical FOV
            camComponent.m_Lens.FieldOfView = 50;
            //找到组件才会设置属性
            var transposer = camComponent.AddCinemachineComponent<CinemachineTransposer>();
            //默认设置为世界模式,物体动摄像机动,物体旋转摄像机不动
            transposer.m_BindingMode = CinemachineTransposer.BindingMode.LockToTargetWithWorldUp;
            transposer.m_FollowOffset = new Vector3(0, 5, -10);
            transposer.m_XDamping = 0;
            transposer.m_YDamping = 0;
            transposer.m_ZDamping = 0;
            var transComposer = camComponent.AddCinemachineComponent<CinemachineComposer>();
            transComposer.m_ScreenX = 0.5f;
            transComposer.m_ScreenY = 0.6f;
            transComposer.m_SoftZoneWidth = 0f;
            transComposer.m_SoftZoneHeight = 0f;
            transComposer.m_HorizontalDamping = 0f;
            transComposer.m_VerticalDamping = 0f;
        }

        /// <summary>
        /// 第三人称自由视角
        /// </summary>
        public void InitThirdPersonFreeLookCharacter(Transform follow, Transform lookAt) {
            var camComponent = TransMainCamera.AddComponent<CinemachineFreeLook>();
            camComponent.Follow = follow;
            camComponent.LookAt = lookAt;
            SetThirdPersonPictureComposition(camComponent);
        }

        /// <summary>
        /// 设置画面构图
        /// </summary>
        /// <param name="screenX">整个Zoon屏幕X位置</param>
        /// <param name="screenY">整个Zoon屏幕Y位置</param>
        /// <param name="deadZoneWidth">范围内主角移动不会触发相机旋转</param>
        /// <param name="deadZoneHieight">范围内主角移动不会触发相机旋转</param>
        /// <param name="softZoneWidth">缓冲区域,区域内相机差值旋转,会保证主角在区域内</param>
        /// <param name="softZoneHeight"></param>
        /// <param name="biasX">softZone相对于整个Zoon的位置,也就是偏移</param>
        /// <param name="biasY">softZone相对于整个Zoon的位置,也就是偏移</param>
        public void SetThirdPersonPictureComposition(CinemachineFreeLook camComponent,
            float screenX = 0.5f,
            float screenY = 0.6f,
            float deadZoneWidth = 0.0f,
            float deadZoneHieight = 0.0f,
            float softZoneWidth = 0.0f,
            float softZoneHeight = 0.0f,
            float biasX = 0,
            float biasY = 0
        ) {
            var c = camComponent.AddComponent<CinemachineComposer>();
            // c.
            c.m_ScreenX = screenX;
            c.m_ScreenY = screenY;
            c.m_DeadZoneWidth = deadZoneWidth;
            c.m_DeadZoneHeight = deadZoneHieight;
            c.m_SoftZoneWidth = softZoneWidth;
            c.m_SoftZoneHeight = softZoneHeight;
            c.m_BiasX = biasX;
            c.m_BiasY = biasY;
        }
    }
}
