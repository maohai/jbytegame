using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasManager : SingletonAutoMono<AtlasManager> {
    private List<SpriteAtlas> allSpriteAtlas = new List<SpriteAtlas>();
    private Dictionary<string,Sprite> allSprite = new Dictionary<string, Sprite>();

    public override void Init() {
        base.Init();
        //Drug 为Texture下面随便的一个资源路径,然后返回的是Texture里面所有的 资源 所以名字可以随意
        var gos = YooAssetManager.DefaultPackage.LoadAllAssetsAsync<SpriteAtlas>("Drug");
        gos.Completed += allGo => {
            foreach (var go in allGo.AllAssetObjects) {
                var sa = go as SpriteAtlas;
                allSpriteAtlas.Add(sa);
                Sprite[] sp = new Sprite[sa.spriteCount];
                sa.GetSprites(sp);
                foreach (var item in sp) {
                    var name = item.name.Replace("(Clone)");
                    if (allSprite.ContainsKey(name)) {
                        Debug.LogError($"图集有重复名称 {name}");
                    } else {
                        allSprite.Add(name, item);
                    }
                }
                Debug.Log($"图集加载完成 {sa.name}");
            }
        };
    }
    /// <summary>
    /// 获取图片
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public Sprite GetSpriteByName(string name) {
        if(allSprite.ContainsKey(name)) return allSprite[name];
        Debug.LogError($"{name} is not found.");
        return null;
    }
}
