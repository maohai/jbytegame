using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

/// <summary>
/// 资源加载模块
/// </summary>
namespace Game {
    public class ResManager : Singleton<ResManager> {
        public T Load<T>(string name) where T : Object {
            T res = Resources.Load<T>(name);
            if (res is GameObject)
                return GameObject.Instantiate(res);
            else
                return res;
        }
        public GameObject LoadPrefab(string name) {
            return Resources.Load<GameObject>(name);
        }

        public void LoadAsync<T>(string name, UnityAction<T> callback, Action after = null) where T : Object {
            Utility.Ins.StartCoroutine(ReallyLoadAsync(name, callback, after));
        }

        private IEnumerator ReallyLoadAsync<T>(string name, UnityAction<T> callback, Action after = null) where T : Object {
            ResourceRequest r = Resources.LoadAsync<T>(name);
            yield return r;
            if (r.asset is GameObject)
                callback(GameObject.Instantiate(r.asset) as T);
            else
                callback(r.asset as T);
            after?.Invoke();
        }
    }
}