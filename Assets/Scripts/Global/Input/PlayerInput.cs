using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerInput : SingletonAutoMono<PlayerInput>,
    InputController.IPlayerActions,
    InputController.IMapActions {
    public InputController input;
    protected override void Awake() {
        base.Awake();
        input = new InputController();
        input.Player.SetCallbacks(this);
        input.Map.SetCallbacks(this);
    }
    public override void Init() {
        base.Init();
        EnablePlayer();
        EnableMap();
    }


    #region Player表相关方法
    public void EnablePlayer() => input.Player.Enable();
    public void DisablePlayer() => input.Player.Disable();

    #region 移动

    //获取一个轴上的值,(-1,1)之间
    Vector2 axes => input.Player.Move.ReadValue<Vector2>();
    public float AxisX => axes.x; //取得x轴上的值
    public float AxisY => axes.y; //取得y轴上的值
    public bool Move => AxisX != 0f || AxisY != 0f;//判断是否移动

    public (float h, float v) AnimMoveHV(float horizontal, float vertical) {
        (float h, float v) value = (0,0);
        value.h = AxisX > 0 ? horizontal : -horizontal;
        value.v = AxisY > 0 ? vertical : -vertical;
        if (AxisX == 0) value.h = 0;
        if (AxisY == 0) value.v = 0;
        return value;
    }

    #endregion

    #region E

    public bool EClick => input.Player.E.WasReleasedThisFrame();

    public void DetetionE() {
        if (EClick) {
            EventManager.OnEventEClick.Trigger();
        }
    }

    #endregion
    #region Q

    public bool QClick => input.Player.Q.WasReleasedThisFrame();

    public void DetetionQ() {
        if (QClick) {
            EventManager.OnEventQClick.Trigger();
        }
    }

    #endregion

    #region R

    public bool RClick => input.Player.R.WasReleasedThisFrame();

    public void DetetionR(Action cb = null) {
        if (RClick) {
            cb?.Invoke();
        }
    }

    #endregion

    #region LeftMouseClick鼠标左键

    public bool LeftMouseClick => input.Player.LeftMouseClick.WasPressedThisFrame();
    public bool LeftMouseContinueClick => input.Player.LeftMouseClick.IsPressed();

    public void DetetionLeftMouseClick(Action down = null, Action up = null) {
        if (LeftMouseContinueClick) {
            down?.Invoke();
        } else {
            up?.Invoke();
        }
    }

    #endregion

    #region RightMouseClick 鼠标右键

    public bool RightMouseClick => input.Player.RightMouseClick.WasPressedThisFrame();
    public bool RightMouseDetetion => input.Player.RightMouseClick.IsPressed();

    public void DetetionRightMouseClick(Action down = null, Action up = null) {
        if (RightMouseDetetion) {
            down?.Invoke();
        } else {
            up?.Invoke();
        }
    }

    #endregion

    #region Shift按键

    public bool ShiftClick => input.Player.Run.IsPressed();
    private bool shiftToggle = false;
    public bool ShiftSwitchState {
        get {
            if (input.Player.Run.WasPressedThisFrame())
                shiftToggle = !shiftToggle;
            return shiftToggle;
        }
    }

    public void SetShiftToggleValue(bool state = false) {
        shiftToggle = state;
    }

    #endregion

    #region F键位

    public bool FClick => input.Player.PickUp.WasPressedThisFrame();
    public void DetetionFClick(Action down = null, Action up = null) {
        if (FClick) {
            down?.Invoke();
        } else {
            up?.Invoke();
        }
    }
    #endregion

    #endregion

    #region Map表相关方法
    public void EnableMap() => input.Map.Enable();
    public void DisableMap() => input.Map.Disable();

    #region Tab

    public bool TabClick => input.Map.Bag.WasPressedThisFrame();

    public void DetetionTabClick(Action down = null, Action up = null) {
        if (TabClick) {
            down?.Invoke();
        } else {
            up?.Invoke();
        }
    }

    #endregion

    #region B

    public bool BClick => input.Map.B.WasReleasedThisFrame();

    public void DetetionBClick(Action cb = null) {
        if (BClick) {
            cb?.Invoke();
        }
    }

    #endregion

    #region 键盘12345678
    public bool Alpha1KeyClick => input.Map.Alpha1.WasPressedThisFrame();
    public bool Alpha2KeyClick => input.Map.Alpha2.WasPressedThisFrame();
    public bool Alpha3KeyClick => input.Map.Alpha3.WasPressedThisFrame();
    public bool Alpha4KeyClick => input.Map.Alpha4.WasPressedThisFrame();
    public bool Alpha5KeyClick => input.Map.Alpha5.WasPressedThisFrame();
    public bool Alpha6KeyClick => input.Map.Alpha6.WasPressedThisFrame();
    public bool Alpha7KeyClick => input.Map.Alpha7.WasPressedThisFrame();
    public bool Alpha8KeyClick => input.Map.Alpha8.WasPressedThisFrame();

    private Action alpha1KeyClickEvent = null;
    private Action alpha2KeyClickEvent = null;
    private Action alpha3KeyClickEvent = null;
    private Action alpha4KeyClickEvent = null;
    private Action alpha5KeyClickEvent = null;
    private Action alpha6KeyClickEvent = null;
    private Action alpha7KeyClickEvent = null;
    private Action alpha8KeyClickEvent = null;

    public void SetAlphaAction(int alpha, Action cb) {
        switch (alpha) {
            case 1: alpha1KeyClickEvent = cb; break;
            case 2: alpha2KeyClickEvent = cb; break;
            case 3: alpha3KeyClickEvent = cb; break;
            case 4: alpha4KeyClickEvent = cb; break;
            case 5: alpha5KeyClickEvent = cb; break;
            case 6: alpha6KeyClickEvent = cb; break;
            case 7: alpha7KeyClickEvent = cb; break;
            case 8: alpha8KeyClickEvent = cb; break;
        }
    }

    public void ClearAlphaAction(int alpha) {
        switch (alpha) {
            case 1: alpha1KeyClickEvent = null; break;
            case 2: alpha2KeyClickEvent = null; break;
            case 3: alpha3KeyClickEvent = null; break;
            case 4: alpha4KeyClickEvent = null; break;
            case 5: alpha5KeyClickEvent = null; break;
            case 6: alpha6KeyClickEvent = null; break;
            case 7: alpha7KeyClickEvent = null; break;
            case 8: alpha8KeyClickEvent = null; break;
        }
    }
    public void DetetionNumKeyClick() {
        if (Alpha1KeyClick) alpha1KeyClickEvent?.Invoke();
        if (Alpha2KeyClick) alpha2KeyClickEvent?.Invoke();
        if (Alpha3KeyClick) alpha3KeyClickEvent?.Invoke();
        if (Alpha4KeyClick) alpha4KeyClickEvent?.Invoke();
        if (Alpha5KeyClick) alpha5KeyClickEvent?.Invoke();
        if (Alpha6KeyClick) alpha6KeyClickEvent?.Invoke();
        if (Alpha7KeyClick) alpha7KeyClickEvent?.Invoke();
        if (Alpha8KeyClick) alpha8KeyClickEvent?.Invoke();
    }

    #endregion

    #endregion

    #region 接口方法 暂时不用

    #region Player

    public void OnMove(InputAction.CallbackContext context) {
        //当接受的信号为移动信号时 InputActionPhase是一个枚举类型
        //Disabled = 0,禁用时
        //Waiting = 1,被启用时，但是没有响应的信号输入
        //Started = 2,按下按键的那一帧，相当于Input.GetKeyDown()
        //Performed = 3,输入动作已执行的时候，包含了按下未弹起相当于Input.GetKey
        //Canceled = 4,信号停止的时候
        // Debug.Log("pp"+context.ReadValue<Vector2>());
        //因为是持续的移动，所以选择Performed
        // if (context.performed || context.started)
        //读取输入动作的二维向量的值
        // PlayerMove.SendMessage(new Vector3(context.ReadValue<Vector2>().x,0,context.ReadValue<Vector2>().y));

        // if (context.canceled)
        // onStopMove.Invoke();
        //bug 可能会触发两次
        // if(context.performed)
        // MouseLeftClick.Ins.SendMessage();
    }
    public void OnE(InputAction.CallbackContext context) { }
    public void OnQ(InputAction.CallbackContext context) { }
    public void OnR(InputAction.CallbackContext context) { }
    public void OnLeftMouseClick(InputAction.CallbackContext context) { }
    public void OnRightMouseClick(InputAction.CallbackContext context) { }
    public void OnRun(InputAction.CallbackContext context) { }
    public void OnPickUp(InputAction.CallbackContext context) { }

    #endregion

    #region Map

    public void OnBag(InputAction.CallbackContext context) { }
    public void OnAlpha1(InputAction.CallbackContext context) { }
    public void OnAlpha2(InputAction.CallbackContext context) { }
    public void OnAlpha3(InputAction.CallbackContext context) { }
    public void OnAlpha4(InputAction.CallbackContext context) { }
    public void OnAlpha5(InputAction.CallbackContext context) { }
    public void OnAlpha6(InputAction.CallbackContext context) { }
    public void OnAlpha7(InputAction.CallbackContext context) { }
    public void OnAlpha8(InputAction.CallbackContext context) { }
    public void OnB(InputAction.CallbackContext context) { }

    #endregion

    #endregion

}
