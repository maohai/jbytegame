using UnityEngine;

namespace Game{
    public class WalkState : PlayerState{
        public override void Enter(){
            base.Enter();
            //添加事件
            EventManager.Ins.AddPlayerMove(playerController.WalkMove);
           
        }

        public override void Exit(){
            base.Exit();
            EventManager.Ins.RemovePlayerMove(playerController.WalkMove);
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            if (!stateManager.IsInAim){
                playerController.PlayerAnimator.SetFloat(Vertical, 0.5f, 0.1f, Time.deltaTime);
                playerController.PlayerAnimator.SetFloat(Horizontal, 0, 0.1f, Time.deltaTime);
            }
            else{
                var v = input.AnimMoveHV(0.5f, 0.5f).v;
                var h = input.AnimMoveHV(0.5f, 0.5f).h;
                var playerDir = Utility.Ins.GetDirByMainCameraMousePoint();
                Utility.Ins.TransformDir2Anim(ref v, ref h, playerDir.x, playerDir.y);
                playerController.PlayerAnimator.SetFloat(Vertical,v, 0.1f, Time.deltaTime);
                playerController.PlayerAnimator.SetFloat(Horizontal,h, 0.1f, Time.deltaTime);
            }
           
              
            #region 移动
            if (!input.Move) {
                //切换为站立
                PlayerStateManager.Ins.SwitchState(typeof(IdleState));
            }

            if (input.ShiftClick){
                //切换为跑步
                PlayerStateManager.Ins.SwitchState(typeof(RunState));
            }
            #endregion
        }

        private Vector3 dir = Vector3.zero;
        public override void PhysicUpdate(){
            base.PhysicUpdate();
            dir.x = input.AxisX;
            dir.z = input.AxisY;
            PlayerMoveMessage.Ins.SendMessage(dir);
        }
    }
}