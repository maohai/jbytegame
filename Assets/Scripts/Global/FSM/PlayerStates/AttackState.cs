using UnityEngine;

namespace Game{
    public class AttackState : PlayerArmState{
        private float baseTime => Utility.Ins.GetAnimatorTimer(playerController.PlayerAnimator, "Attack1");
        private float currentTime = 0;
        public override void Enter(){
            base.Enter();
            currentTime = baseTime;
            //动画切换
            playerController.PlayerAnimator.CrossFade("Attack1",0.2f);
            stateManager.SetCanSwitchArm(false);
        }

        public override void Exit(){
            base.Exit();
            stateManager.SetCanSwitchArm();

            //动画切换为空
            playerController.PlayerAnimator.CrossFade("Empty",0.2f);
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            if (currentTime > 0){
                currentTime -= Time.deltaTime;
                return;
            }
            stateManager.ClearCurrentArmState();
        }
    }
}