using UnityEngine;

namespace Game {
    public class IdleState : PlayerState {
        public override void Enter() {
            base.Enter();
            //动画切换
            playerController.PlayerAnimator.CrossFade("BaseLocomotion", 0.2f);
        }

        public override void Exit() {
            base.Exit();
        }

        public override void LogicUpdate() {
            base.LogicUpdate();
            playerController.PlayerAnimator.SetFloat(Vertical, 0, 0.1f, Time.deltaTime);
            playerController.PlayerAnimator.SetFloat(Horizontal, 0, 0.1f, Time.deltaTime);

            //方向切换
            if (stateManager.IsInAim) {
                var mousePos = Input.mousePosition;
                Vector3 center = Utility.Ins.GetScreenCenter();
                var playerDir = (mousePos - center).normalized;
                // Debug.Log($"当前的朝向{playerDir}");
                playerController.RotationModel(new Vector3(playerDir.x, 0, playerDir.y));
            }


            if (input.Move) {
                //切换为跑步动作
                PlayerStateManager.Ins.SwitchState(typeof(WalkState));
            }
        }
    }
}