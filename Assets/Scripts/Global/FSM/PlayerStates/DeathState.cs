using UnityEngine;

namespace Game{
    public class DeathState : PlayerState{
        public override void Enter(){
            base.Enter();
            Debug.Log("进入"+this.GetType().Name + "状态");
            //动画切换
            playerController.PlayerAnimator.CrossFade("Death",0.2f);
            //作缓冲,执行完
            PlayerStateManager.Ins.SetCanSwitch(false);
            Debug.Log("game over");
        }
    }
}