using UnityEngine;

namespace Game{
    public class PickState : PlayerState{
        public override void Enter(){
            Debug.Log("进入"+this.GetType().Name + "状态");
        }

        public override void Exit(){
            Debug.Log("退出"+this.GetType().Name + "状态");
        }

        public override void LogicUpdate(){
        
        }

        public override void PhysicUpdate(){
        
        }
    }
}