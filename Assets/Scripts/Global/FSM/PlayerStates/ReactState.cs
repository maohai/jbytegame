using UnityEngine;

namespace Game{
    public class ReactState : PlayerState{
        private float baseTime => Utility.Ins.GetAnimatorTimer(playerController.PlayerAnimator,"ReactLargeGut");
        private float remainTime = 0;
        public override void Enter(){
            base.Enter();
            //动画切换
            playerController.PlayerAnimator.CrossFade("ReactLargeGut",0.2f);
            //作缓冲,执行完
            remainTime = baseTime / 2;
            stateManager.SetCanSwitchArm(false);
        }

        public override void Exit(){
            base.Exit();
            stateManager.SetCanSwitchArm();
            playerController.PlayerAnimator.CrossFade("Empty",0.2f);
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            if (remainTime > 0){
                remainTime -= Time.deltaTime;
                return;
            }
            stateManager.ClearCurrentArmState();
        }
    }
}