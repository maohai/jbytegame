using UnityEngine;

namespace Game{
    public class RunState : PlayerState{
        public override void Enter(){
            base.Enter();
            //添加事件
            EventManager.Ins.AddPlayerMove(playerController.RunMove);
        }

        public override void Exit(){
            base.Exit();
            input.SetShiftToggleValue();
            EventManager.Ins.RemovePlayerMove(playerController.RunMove);
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            if (!stateManager.IsInAim){
                playerController.PlayerAnimator.SetFloat(Vertical, 1, 0.1f, Time.deltaTime);
                playerController.PlayerAnimator.SetFloat(Horizontal, 0, 0.1f, Time.deltaTime);
            }
            else{
                //动画切换
                var v = input.AnimMoveHV(1,1).v;
                var h = input.AnimMoveHV(1,1).h;
                var playerDir = Utility.Ins.GetDirByMainCameraMousePoint();
                Utility.Ins.TransformDir2Anim(ref v, ref h, playerDir.x, playerDir.y);
                playerController.PlayerAnimator.SetFloat(Vertical,v, 0.1f, Time.deltaTime);
                playerController.PlayerAnimator.SetFloat(Horizontal,h, 0.1f, Time.deltaTime);
            }

            #region 移动
            if (!input.Move) {
                //切换为站立
                PlayerStateManager.Ins.SwitchState(typeof(IdleState));
            }
            if (!input.ShiftClick){
                //切换为走路
                PlayerStateManager.Ins.SwitchState(typeof(WalkState));
            }
            #endregion
        }

        private Vector3 dir = Vector3.zero;
        public override void PhysicUpdate(){
            base.PhysicUpdate();
            dir.x = input.AxisX;
            dir.z = input.AxisY;
            PlayerMoveMessage.Ins.SendMessage(dir);
        }
    }
}