using UnityEngine;

namespace Game{
    public class ShootWalkState : PlayerArmState{
        public override void Enter(){
            base.Enter();
            //动画切换
            playerController.PlayerAnimator.CrossFade("ShootLocomotion",0.2f);
        }
        
        public override void LogicUpdate(){
            base.LogicUpdate();
            if (!stateManager.IsInAim){
                stateManager.ClearCurrentArmState();
                //动画切换为空
                playerController.PlayerAnimator.CrossFade("Empty",0.2f);
            }
            playerController.PlayerAnimator.SetFloat(ShootMove, 1, 0.1f, Time.deltaTime);
            if (!input.Move) {
                //切换为走路
                stateManager.ArmSwitchState(typeof(ShootIdleState));
            }
            if (input.ShiftClick){
                //切换为跑步
                stateManager.ArmSwitchState(typeof(ShootRunState));
            }
                
        }
    }
}