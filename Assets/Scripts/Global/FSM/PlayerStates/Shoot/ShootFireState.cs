using UnityEngine;

namespace Game{
    public class ShootFireState : PlayerArmState{
        private float baseTime => Utility.Ins.GetAnimatorTimer(playerController.PlayerAnimator, "ShootFire");
        private float currentTime = 0;
        public override void Enter(){
            base.Enter();
            currentTime = baseTime;
            //动画切换
            playerController.PlayerAnimator.CrossFade("ShootFire",0f);
            stateManager.SetCanSwitchArm(false);
        }
        
        public override void LogicUpdate(){
            base.LogicUpdate();
            stateManager.ReduceFireCD();
            if (currentTime > 0){
                currentTime -= Time.deltaTime;
                return;
            }
            if (stateManager.IsContinueFire){
                currentTime = baseTime;
                return;
            }
            stateManager.SetCanSwitchArm();
            if (input.ShiftClick){
                //切换为跑步
                stateManager.ArmSwitchState(typeof(ShootRunState));
            }
            if (input.Move) {
                //切换为走路
                stateManager.ArmSwitchState(typeof(ShootWalkState));
            }
            else{
                stateManager.ArmSwitchState(typeof(ShootIdleState));
            }
            
                
        }
    }
}