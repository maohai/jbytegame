using UnityEngine;

namespace Game{
    public class ShootReloadState : PlayerArmState{
        private float baseTime => Utility.Ins.GetAnimatorTimer(playerController.PlayerAnimator, "ShootReload");
        private float currentTime = 0;
        public override void Enter(){
            base.Enter();
            //动画切换
            currentTime = baseTime;
            stateManager.SetCanSwitchArm(false);
            stateManager.SetArmReloadState();
            playerController.PlayerAnimator.CrossFade("ShootReload",0.2f);
        }
        
        public override void LogicUpdate(){
            base.LogicUpdate();
            base.LogicUpdate();
            if (currentTime > 0){
                currentTime -= Time.deltaTime;
                return;
            }
            KeyRClickDownMessage.Ins.SendMessage();
            stateManager.SetCanSwitchArm();
            stateManager.SetArmReloadState(false);
            if (input.ShiftClick){
                //切换为跑步
                stateManager.ArmSwitchState(typeof(ShootRunState));
            }
            if (input.Move) {
                //切换为走路
                stateManager.ArmSwitchState(typeof(ShootWalkState));
            }
            else{
                stateManager.ArmSwitchState(typeof(ShootIdleState));
            }

                
        }
    }
}