using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IState{
    void Enter();//状态进入
    void Exit();//状态退出
    void LogicUpdate();//状态逻辑更新
    void PhysicUpdate();//物理更新
}

