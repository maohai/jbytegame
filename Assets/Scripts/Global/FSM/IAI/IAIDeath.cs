using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIDeath{
        public void EnterAIDeath();
        public void ExitAIDeath();
        public void LogicUpdateAIDeath();
        public void PhysicUpdateAIDeath();
    }
}