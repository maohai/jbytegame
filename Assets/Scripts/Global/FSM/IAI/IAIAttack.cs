using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIAttack:IAIBase{
        public void EnterAIAttack();
        public void ExitAIAttack();
        public void LogicUpdateAIAttack();
        public void PhysicUpdateAIAttack();
    }
}
