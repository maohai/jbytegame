using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIFollow : IAIBase{
        public void EnterAIFollow();
        public void ExitAIFollow();
        public void LogicUpdateAIFollow();
        public void PhysicUpdateAIFollow();
    }
}