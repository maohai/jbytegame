using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIEnemyCapsule :
        IAIIdle,
        IAIAttack,
        IAIMove,
        IAIFollow,
        IAIReaction,
        IAIDeath
    { }
}