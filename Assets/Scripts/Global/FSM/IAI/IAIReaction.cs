using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIReaction{
        public void EnterAIReaction();
        public void ExitAIReaction();
        public void LogicUpdateAIReaction();
        public void PhysicUpdateAIReaction();
    }
}