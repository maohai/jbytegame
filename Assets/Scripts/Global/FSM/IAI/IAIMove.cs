using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIMove:IAIBase{
        public void EnterAIMove();
        public void ExitAIMove();
        public void LogicUpdateAIMove();
        public void PhysicUpdateAIMove();
    }
}