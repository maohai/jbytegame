using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public interface IAIIdle : IAIBase{
        public void EnterAIIdle();
        public void ExitAIIdle();
        public void LogicUpdateAIIdle();
        public void PhysicUpdateAIIdle();
    }
}