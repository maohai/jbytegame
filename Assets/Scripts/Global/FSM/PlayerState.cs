using UnityEngine;

namespace Game {
    public class PlayerState : RoleState {
        protected PlayerController playerController => PlayerController.Ins;
        protected PlayerInput input => PlayerInput.Ins;
        protected PlayerStateManager stateManager => PlayerStateManager.Ins;
        protected int Horizontal => Animator.StringToHash("PlayerHorizontal");
        protected int Vertical => Animator.StringToHash("PlayerVertical");
        protected int ShootMove => Animator.StringToHash("PlayerShootMove");

        public override void Enter() { }

        public override void Exit() { }

        public override void LogicUpdate() { }

        public override void PhysicUpdate() { }
    }
}