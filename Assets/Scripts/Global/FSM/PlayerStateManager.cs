using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    /// <summary>
    /// 跟Player强绑定
    /// </summary>
    public class PlayerStateManager : SingletonAutoMono<PlayerStateManager> {

        #region 初始化

        private IState currentState;
        private Dictionary<System.Type, IState> stateTable = new(); //<类型类,接口>

        private IState currentArmState;
        private Dictionary<Type, IState> stateArmTable = new(); //上半身状态

        public override void Init() {
            Init(new RunState());
            Init(new IdleState());
            Init(new WalkState());
            Init(new PickState());
            Init(new DeathState());
            currentState = stateTable[typeof(IdleState)];

            InitArm(new AttackState());
            InitArm(new ReactState());
            InitArm(new ShootIdleState());
            InitArm(new ShootWalkState());
            InitArm(new ShootRunState());
            InitArm(new ShootFireState());
            InitArm(new ShootReloadState());

            //初始化周期控制
            SetCanSwitch();
            SetCanSwitchArm();
        }
        private void Init<T>(T state) where T : PlayerState => stateTable.Add(typeof(T), state);
        private void InitArm<T>(T state) where T : PlayerState => stateArmTable.Add(typeof(T), state);

        #endregion

        #region 周期控制

        private Type[] ignoreArmState = new[]{typeof(ReactState)};
        private bool CheckIgnoreArmState(Type type) {
            foreach (var t in ignoreArmState) {
                if (type == t)
                    return true;
            }

            return false;
        }

        private bool CanSwitch = false;
        public void SetCanSwitch(bool flag = true) => CanSwitch = flag;
        public bool GetCanSwitch() => CanSwitch;

        private bool CanSwitchArm = false;
        public void SetCanSwitchArm(bool flag = true) => CanSwitchArm = flag;
        public bool GetCanSwitchArm() => CanSwitchArm;

        /// <summary>
        /// 是否持枪
        /// </summary>
        public int AnimName_IsCanHoldGun => Animator.StringToHash("CanHoldGun");

        public int AnimName_IsInAim => Animator.StringToHash("IsInAim");

        /// <summary>
        /// 瞄准
        /// </summary>
        private bool isInAim = false;
        public bool IsInAim {
            get => isInAim;
            set {
                PlayerController.Ins.SetAnimIsInAim(value);
                isInAim = value;
            }
        }
        /// <summary>
        /// 是否正在换弹
        /// </summary>
        private bool isReload;
        public bool IsReload => isReload;
        public void SetArmReloadState(bool flag = true) => isReload = flag;

        /// <summary>
        /// 开火间隔
        /// </summary>
        private float currentFireCD;
        public float CurrentFireCD => currentFireCD;
        public void ReduceFireCD() {
            if (currentFireCD >= 0)
                currentFireCD -= Time.deltaTime;
        }
        public void SetArmFireCD(float time) => currentFireCD = time;


        /// <summary>
        /// 是否正在持续开枪
        /// </summary>
        private bool isContinueFire = false;
        public bool IsContinueFire => isContinueFire;
        public void SetArmContinueFire(bool flag = true) => isContinueFire = flag;

        #endregion

        #region 单个状态切换

        public void SwitchArmAttack1(IEventMessage e = null, object[] o = null) => ArmSwitchState(typeof(AttackState));
        public void SwitchReact(IEventMessage e = null, object[] o = null) => SwitchState(typeof(ReactState));
        public void SwitchDeath(IEventMessage e = null, object[] o = null) => SwitchState(typeof(DeathState));
        public void SwitchArmShootFire(IEventMessage e = null, object[] o = null) => ArmSwitchState(typeof(ShootFireState));

        #endregion

        #region 状态切换

        private void OnDisable() {
            stateTable.Clear();
            stateArmTable.Clear();
        }

        private void Update() {
            currentState.LogicUpdate(); //逻辑状态更新
            currentArmState?.LogicUpdate(); //上半身逻辑状态更新
        }

        private void FixedUpdate() {
            currentState.PhysicUpdate(); //物理状态更新 
            currentArmState?.PhysicUpdate();
        }

        #region 全身

        private void SwitchOn(IState newState) {
            //当前状态的启动
            currentState = newState; //设置为新状态
            currentState.Enter(); //调用enter来启用
        }

        public void SwitchState(IState newState) {
            //共有状态,切换当前方法
            currentState.Exit(); //退出当前状态
            SwitchOn(newState); //掐换为新状态
        }

        public void SwitchState(Type newStateType) {
            if (!CanSwitch) return;
            SwitchState(stateTable[newStateType]);
        }

        #endregion

        #region 上半身

        private void ArmSwitchOn(IState newState) {
            //当前状态的启动
            currentArmState = newState; //设置为新状态
            currentArmState.Enter(); //调用enter来启用
        }

        private void ArmSwitchState(IState newState) {
            //共有状态,切换当前方法
            currentArmState?.Exit(); //退出当前状态
            ArmSwitchOn(newState); //掐换为新状态
        }

        public void ArmSwitchState(Type newStateType) {
            if (!CanSwitchArm && !CheckIgnoreArmState(newStateType)) return;
            ArmSwitchState(stateArmTable[newStateType]);
        }

        /// <summary>
        /// 清空当前上半身状态
        /// </summary>
        public void ClearCurrentArmState() {
            currentArmState?.Exit();
            currentArmState = null;
        }

        #endregion

        #endregion

    }
}