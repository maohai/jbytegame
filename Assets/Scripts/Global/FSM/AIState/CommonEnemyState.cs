using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class CommonEnemyState : RoleState{
        protected AIStateManager stateManager => AIStateManager.Ins;
        protected BaseElementMono enemy;
        
        public CommonEnemyState(BaseElementMono enemy){
            this.enemy = enemy;
        }
        public override void Enter(){ }
        public override void Exit(){ }
        public override void LogicUpdate(){ }
        public override void PhysicUpdate(){ }
    }
}