using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIDeathState : CommonEnemyState{
        public AIDeathState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIDeath();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIDeath();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIDeath();
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIDeath();
        }
    }
}