using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIIdleState : CommonEnemyState{
        public AIIdleState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIIdle();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIIdle();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIIdle();
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIIdle();
        }
    }
}