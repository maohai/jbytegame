using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIFollowState : CommonEnemyState{
        public AIFollowState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIFollow();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIFollow();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIFollow();
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIFollow();
        }
    }
}