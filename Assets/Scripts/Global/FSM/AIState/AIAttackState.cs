using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIAttackState : CommonEnemyState{
        public AIAttackState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIAttack();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIAttack();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIAttack();
            
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIAttack();
        }
    }
}