using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIReactState : CommonEnemyState{
        public AIReactState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIReaction();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIReaction();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIReaction();
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIReaction();
        }
    }
}