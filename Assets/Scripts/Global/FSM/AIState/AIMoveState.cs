using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIMoveState : CommonEnemyState{
        public AIMoveState(BaseElementMono enemy):base(enemy){}
        public override void Enter(){
            base.Enter();
            enemy.EnterAIMove();
        }

        public override void Exit(){
            base.Exit();
            enemy.ExitAIMove();
        }

        public override void LogicUpdate(){
            base.LogicUpdate();
            enemy.LogicUpdateAIMove();
            
        }

        public override void PhysicUpdate(){
            base.PhysicUpdate();
            enemy.PhysicUpdateAIMove();
        }
    }
}