using BTTree;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class AIStateManager : SingletonAutoMono<AIStateManager>{
        /// <summary>
        /// ConcurrentDictionary 一种安全的异步方法
        /// </summary>
        private ConcurrentDictionary<BaseElementMono, IState> dicCurrentState = new();
        private ConcurrentDictionary<BaseElementMono,Dictionary<System.Type, IState>> stateTable = new(); //<类型类,接口>
        private ConcurrentDictionary<BaseElementMono, IState>  dicCurrentArmState = new ();
        private ConcurrentDictionary<BaseElementMono,Dictionary<System.Type, IState>> stateArmTable = new(); //上半身状态
        
        public void AddAIState(BaseElementMono enemy){
            if (stateTable.ContainsKey(enemy)) return;
            Init(enemy,new AIIdleState(enemy));
            Init(enemy,new AIMoveState(enemy));
            Init(enemy,new AIFollowState(enemy));
            Init(enemy,new AIReactState(enemy));
            Init(enemy,new AIDeathState(enemy));
            //如果该键不存在，则使用指定函数将键/值对添加   如果该键已存在，则使用该函数更新
            // dicCurrentState.AddOrUpdate(enemy,stateTable[enemy][typeof(AIFollowState)], (a,b)=>b);
            //尝试添加
            dicCurrentState.TryAdd(enemy,stateTable[enemy][typeof(AIIdleState)]);

            InitArm(enemy, new AIAttackState(enemy));
            //添加一个空,防止切换不生效
            dicCurrentArmState.TryAdd(enemy, null);
        }

        public void RemoveAIState(BaseElementMono enemy){
            if (dicCurrentState.ContainsKey(enemy))
                // dicCurrentState.Remove(enemy,out IState s);
                //尝试删除
                dicCurrentState.TryRemove(enemy,out IState s);
        }

        private void Init<T>(BaseElementMono enemy, T state) where T : CommonEnemyState{
            if (stateTable.ContainsKey(enemy)){
                stateTable[enemy].Add(typeof(T), state);
            }
            else{
                Dictionary<System.Type, IState> s = new();
                s.Add(typeof(T), state);
                // stateTable.AddOrUpdate(enemy,s,(a,b)=>b);
                stateTable.TryAdd(enemy,s);
            }
        }
        private void InitArm<T>(BaseElementMono enemy, T state) where T : CommonEnemyState{
            if (stateArmTable.ContainsKey(enemy)){
                stateArmTable[enemy].Add(typeof(T), state);
            }
            else{
                Dictionary<System.Type, IState> s = new();
                s.Add(typeof(T), state);
                // stateTable.AddOrUpdate(enemy,s,(a,b)=>b);
                stateArmTable.TryAdd(enemy,s);
            }
        }

        #region 状态切换

        private void OnDisable(){
            stateTable.Clear();
            stateArmTable.Clear();
        }
        
        private void Update(){
            foreach (var item in dicCurrentState){
                item.Value.LogicUpdate(); //逻辑状态更新
            }
            foreach (var item in dicCurrentArmState){
                item.Value?.LogicUpdate(); //上半身逻辑状态更新
            }
        }
        private void FixedUpdate(){
            foreach (var item in dicCurrentState){
                item.Value.PhysicUpdate(); //逻辑状态更新
            }
            foreach (var item in dicCurrentArmState){
                item.Value?.PhysicUpdate(); //上半身逻辑状态更新
            }
        }

        #region 上半身

        private void SwitchOn(BaseElementMono enemy,IState newState){
            //当前状态的启动
            if (dicCurrentState.ContainsKey(enemy)){
                dicCurrentState[enemy] = newState; //设置为新状态
                dicCurrentState[enemy].Enter(); //调用enter来启用
            }
        }

        public void SwitchState(BaseElementMono enemy,IState newState){
            //共有状态,切换当前方法
            if (dicCurrentState.ContainsKey(enemy)){
                dicCurrentState[enemy].Exit(); //退出当前状态
                SwitchOn(enemy,newState); //掐换为新状态
            }
        }

        public void SwitchState(BaseElementMono enemy,System.Type newStateType){
            if (!enemy.CanSwitchState && !enemy.CheckIgnore(newStateType)) return;
            SwitchState(enemy,stateTable[enemy][newStateType]);
        }

        #endregion
        
        #region 下半身

        private void ArmSwitchOn(BaseElementMono enemy,IState newState){
            //当前状态的启动
            if (dicCurrentArmState.ContainsKey(enemy)){
                dicCurrentArmState[enemy] = newState; //设置为新状态
                dicCurrentArmState[enemy]?.Enter(); //调用enter来启用
            }
        }

        public void ArmSwitchState(BaseElementMono enemy,IState newState){
            //共有状态,切换当前方法
            if (dicCurrentArmState.ContainsKey(enemy)){
                dicCurrentArmState[enemy]?.Exit(); //退出当前状态
                ArmSwitchOn(enemy,newState); //掐换为新状态
            }
        }

        public void ArmSwitchState(BaseElementMono enemy,System.Type newStateType){
            if (!enemy.CanSwitchArmState && !enemy.CheckArmIgnore(newStateType)) return;
            ArmSwitchState(enemy,stateArmTable[enemy][newStateType]);
        }

        public void ClearCurrentArmState(BaseElementMono enemy){
            dicCurrentArmState[enemy]?.Exit(); //退出当前状态
            dicCurrentArmState[enemy] = null;
        }

        #endregion
        

        #endregion
        
    }

}