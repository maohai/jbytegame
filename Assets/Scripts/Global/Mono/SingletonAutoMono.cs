using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 继承MonoBehaviour的单例模式基类
/// </summary>
public class SingletonAutoMono<T>: MonoBehaviour where T : MonoBehaviour
{
    private static T ins;
    public static bool InsIsNull => ins == null;
    public static T Ins{
        get{
            if(ins == null)
            {
                GameObject obj = new GameObject();
                obj.name=typeof(T).ToString();
                DontDestroyOnLoad(obj);
                ins=obj.AddComponent<T>();
            }
            return ins;
        }
    }
    protected virtual void Awake()
    {
        ins = this as T;
    }

    public virtual void Init(){
        
    }
}
