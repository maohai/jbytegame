using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomExpand
{
    #region string

    public static string GetNameByPath(this string path) {
        var arr = path.Split('/');
        return arr[arr.Length - 1];
    }
    public static int ToInt(this string str) {
        return int.Parse(str);
    }

    public static string Replace(this string str, string replacement) {
        if (!str.Contains(replacement)) Debug.LogError("没有包含此字符串");
        return str.Replace(replacement, "");
    }
    #endregion
}
