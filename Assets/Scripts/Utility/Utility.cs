using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MouseLeftClickDownMessage : Singleton<MouseLeftClickDownMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}
public class MouseLeftClickUpMessage : Singleton<MouseLeftClickUpMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}
public class MouseLeftContinueClickMessage : Singleton<MouseLeftContinueClickMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}
public class MouseRightClickDownMessage : Singleton<MouseRightClickDownMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}
public class MouseRightClickUpMessage : Singleton<MouseRightClickUpMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}
public class MouseContinueDetectionMessage : Singleton<MouseContinueDetectionMessage>, IEventMessage {
    public void SendMessage(Action cb = null) {
        UniEvent.SendMessage(Ins);
    }
}

namespace Game {
    public class Utility : SingletonAutoMono<Utility> {
        public static void NumClam(ref int num, int value, int comValue, bool isAdd = true) {
            if (isAdd) {
                var temp =  num + value;
                if (temp >= comValue)
                    num = comValue;
                else num = temp;
            } else {
                var temp =  num - value;
                if (temp <= comValue)
                    num = comValue;
                else num = temp;
            }
        }

        public static void VariationAuto(ref int num, int value) {
            NumClam(ref num, value, value > 0 ? Int32.MaxValue : 0, value > 0);
        }
        public int GetEnumNum(Type type) {
            int num = 0;
            foreach (var e in Enum.GetValues(type))
                ++num;
            return num;
        }
        public void BindActionByEnum(Type type, Action<Enum> cb) {
            foreach (Enum e in Enum.GetValues(type))
                cb?.Invoke(e);
        }

        public void Delay(Action action, float delay) {
            StartCoroutine(Cor());
            IEnumerator Cor() {
                yield return new WaitForSeconds(delay);
                action?.Invoke();
            }
        }

        #region 协程相关
        /// <summary>
        /// 贝塞尔时间公式（二阶、三阶）
        /// </summary>
        /// <param name="t">时间参数，范围0~1</param>
        /// <returns></returns>
        public Vector3 Formula(Transform[] points, float t) {
            switch (points.Length) {
                case 3: return QuardaticBezier(points, t);
                case 4: return CubicBezier(points, t);
            }
            return Vector3.zero;
        }
        /// <summary>
        /// 一阶贝塞尔
        /// </summary>
        /// <param name="t">时间参数，范围0~1</param>
        /// <returns></returns>
        public Vector3 LineBezier(Transform[] points, float t) {
            Vector3 a = points[0].position;
            Vector3 b = points[1].position;
            return a + (b - a) * t;
        }
        /// <summary>
        /// 二阶贝塞尔
        /// </summary>
        /// <param name="t">时间参数，范围0~1</param>
        /// <returns></returns>
        private Vector3 QuardaticBezier(Transform[] points, float t) {
            Vector3 a = points[0].position;
            Vector3 b = points[1].position;
            Vector3 c = points[2].position;

            Vector3 aa = a + (b - a) * t;
            Vector3 bb = b + (c - b) * t;
            return aa + (bb - aa) * t;
        }
        /// <summary>
        /// 三阶贝塞尔
        /// </summary>
        /// <param name="t">时间参数，范围0~1</param>
        /// <returns></returns>
        private Vector3 CubicBezier(Transform[] points, float t) {
            Vector3 a = points[0].position;
            Vector3 b = points[1].position;
            Vector3 c = points[2].position;
            Vector3 d = points[3].position;

            Vector3 aa = a + (b - a) * t;
            Vector3 bb = b + (c - b) * t;
            Vector3 cc = c + (d - c) * t;

            Vector3 aaa = aa + (bb - aa) * t;
            Vector3 bbb = bb + (cc - bb) * t;
            return aaa + (bbb - aaa) * t;
        }
        ///<summary>
        ///根据T值，计算贝塞尔曲线上面相对应的点
        ///</summary>
        ///<param name="t">T值</param>
        ///<param name="p0">起始点</param>
        ///<param name="p1">控制点</param>
        ///<param name="p2">目标点</param>
        ///<returns>根据T值计算出来的贝赛尔曲线点</returns>
        private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2) {
            float u=1-t;
            float tt=t*t;
            float uu=u*u;

            Vector3 p=uu*p0;
            p += 2 * u * t * p1;
            p += tt * p2;
            return p;
        }
        /// <summary>
        /// 获取路径点
        /// </summary>
        /// <param name="startPoint">起始点</param>
        /// <param name="controlPoint">控制点</param>
        /// <param name="endPoint">终点</param>
        /// <param name="segmentNum">数量</param>
        /// <returns></returns>
        public Vector3[] GetBeizerList(Vector3 startPoint, Vector3 controlPoint, Vector3 endPoint, int segmentNum) {
            Vector3[] path = new Vector3[segmentNum];
            for (int i = 1; i <= segmentNum; i++) {
                float t = i / (float) segmentNum;
                Vector3 pixel = CalculateCubicBezierPoint(t, startPoint,
                controlPoint, endPoint);
                path[i - 1] = pixel;
                // Debug.Log(path[i - 1]);
            }
            return path;
        }
        /// <summary>
        /// 获取当前贝塞尔路径的位置
        /// 需要注意的是,index是currentTime和runTime的插值来算
        /// </summary>
        /// <param name="pointNum">路径点</param>
        /// <param name="runTime">贝塞尔运动时间</param>
        /// <param name="index">输出当前点</param>
        /// <param name="currentTime">当前时间,不小于总时间</param>
        public void GetBeizerIndex(int pointNum, float runTime, ref int index, ref float currentTime) {
            currentTime += Time.deltaTime;
            var ct = Mathf.Lerp(0, runTime, currentTime/runTime);
            if (ct > runTime / pointNum * index) {
                ++index;
            }
        }


        #endregion

        #region 动画相关

        /// <summary>
        /// 获取animation的时间
        /// </summary>
        /// <param name="animation"></param>
        /// <param name="animation_Name">动画的名字</param>
        /// <returns></returns>
        public float GetAnimationTimer(Animation animation, string aniName) => animation.GetClip(aniName).length;


        /// <summary>
        /// 获取animator的时间，注意是Animator!!!!
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="aniName">动画的名字</param>
        /// <returns></returns>
        public float GetAnimatorTimer(Animator animator, string aniName) {
            float length = 0;
            AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
            foreach (AnimationClip clip in clips) {
                if (clip.name.Equals(aniName)) {
                    length = clip.length;
                    break;
                }
            }

            return length;
        }

        /// <summary>
        /// slider动画,用于血量,蓝量插值
        /// </summary>
        /// <param name="slider"></param>
        /// <param name="startValue"></param>
        /// <param name="targetValue"></param>
        /// <param name="time"></param>
        /// <param name="cb"></param>
        /// <returns></returns>
        public IEnumerator SliderValueVariationCoroutine(
            Slider slider,
            float startValue,
            float targetValue,
            float time = 2f,
            Action cb = null) {
            float currentTime = 0;
            float currentValue = startValue;
            while (currentValue != targetValue) {
                currentTime += 0.02f;
                currentValue = Mathf.Lerp(startValue, targetValue, currentTime / time);
                slider.value = currentValue;
                yield return new WaitForSeconds(0.02f);
            }

            cb?.Invoke();
        }

        #endregion

        #region 鼠标射线检测

        private Ray ray;//摄像检测
        private RaycastHit hit;//碰撞信息类
        public T MouseFun<T>(Action cb = null, float maxDistance = 500, int ignore = 1) where T : class {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);//鼠标点击发射射线
            bool isHit = Physics.Raycast(ray, out hit,maxDistance,ignore);

            if (isHit) {
                isHit = hit.collider.TryGetComponent(out T component);
                if (isHit) {
                    cb?.Invoke();
                    return component;
                }
                // RaycastHit[] hits = Physics.RaycastAll(ray);
                // RaycastHit[] hits1 = Physics.RaycastAll(ray,100);//距离100以内
                // RaycastHit[] hits2 = Physics.RaycastAll(ray,100,1<<10);//检测第十层的物体,距离100以内
            }
            return null;
        }

        public RaycastHit MouseFun(Action cb = null, float maxDistance = 500, int ignore = 1) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);//鼠标点击发射射线
            var hitFlag = Physics.Raycast(ray, out RaycastHit hit,maxDistance,ignore);
            if (hitFlag) {
                //Debug.Log("击中了");
                cb?.Invoke();
            }
            return hit;
        }
        /// <summary>
        /// 鼠标左键点击事件
        /// </summary>
        public T LeftMouseClick<T>(Action cb = null, float maxDistance = 500, int ignore = 1) where T : class {
            if (PlayerInput.Ins.LeftMouseClick) {
                return MouseFun<T>(cb, maxDistance, ignore);
            }
            return null;//不包含这个类型
        }

        private float moveOffset;
        private Vector2 movePrePos;//鼠标上一次位置
        public (RaycastHit hit, Vector3 mousePos) MouseMove(Action cb = null, float maxDistance = 500, int ignore = 1) {
            moveOffset = Vector2.Distance(Input.mousePosition, movePrePos);
            if (moveOffset > float.MinValue) {
                return (MouseFun(cb, maxDistance, ignore), Input.mousePosition);
            }
            return (new RaycastHit(), movePrePos);
        }

        #endregion

        #region 怪物刷新逻辑

        /*
         * 时间间隔刷新：根据一定的时间间隔来刷新怪物，例如每隔一定时间刷新一波怪物。
         * 玩家位置刷新：当玩家接近或进入特定区域时，刷新怪物，以增加游戏的挑战性
         * 特定事件触发：当玩家完成特定任务、触发特定事件或达到一定条件时，刷新怪物。
         * 随机刷新：在游戏地图的特定位置随机刷新怪物，增加游戏的变化性。
         * 波次刷新：按照波次的方式刷新怪物，每一波怪物难度逐渐增加。
         * 固定位置刷新：在游戏地图的固定位置刷新怪物，例如在特定的怪物生成点.
         * 玩家等级刷新：根据玩家的等级或进度来刷新不同难度的怪物，保持游戏的挑战性。
         * 限定条件刷新：根据特定条件（例如天气、时间等）来刷新怪物，增加游戏的变化性和挑战性。
         * 玩家行为刷新：根据玩家的行为，如攻击、移动或使用技能等，来触发怪物的刷新。
         * 剧情触发刷新：在游戏剧情关键点或特定剧情事件发生时，刷新怪物以推动剧情发展。
         * 资源或物品触发刷新：当玩家获得特定资源或物品时，刷新怪物以增加游戏挑战性或奖励。
         * 生存模式刷新：在游戏的生存模式中，随着时间的推移或特定条件的达成，刷新更强大的怪物。
         * 环境因素刷新：根据游戏环境的变化，如天气、地形等，刷新怪物以增加游戏的动态性。
         * 难度选择刷新：根据玩家选择的难度级别来调整怪物的刷新数量、类型和难度。
         * 连续击败刷新：当玩家连续击败一定数量的怪物时，触发更强大的怪物刷新。
         * 玩家状态刷新：根据玩家的状态，如生命值、饥饿度等，刷新怪物以增加游戏的策略性和挑战性。
         */


        /// <summary>
        /// 一个怪物的刷新
        /// </summary>
        /// <returns></returns>
        private const float OneMonsterRefreshTime = 0.05f;
        private WaitForSeconds waitOneMonsterRefreshTime = new WaitForSeconds(OneMonsterRefreshTime);
        // public IEnumerator OneMonsterRefresh(string objName,Vector3 pos = default){
        //     PoolMgr.Ins.GetObj(objName, obj => { obj.transform.position = pos; });
        //     yield return waitOneMonsterRefreshTime;
        // }

        /// <summary>
        /// 时间间隔刷新：根据一定的时间间隔来刷新怪物，例如每隔一定时间刷新一波怪物
        /// 相隔一定时间刷新怪物
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="time">相隔时间</param>
        /// <param name="num">人数</param>
        public IEnumerator MonsterRefreshByTimes(string objName, Vector3 pos, float time, int num = 1) {
            while (true) {
                while (--num > 0) {
                    var obj = PoolManager.Ins.Release(objName);
                    yield return waitOneMonsterRefreshTime;
                }
                yield return new WaitForSeconds(time);
            }
        }

        /// <summary>
        /// 简单刷新怪物
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="pos"></param>
        /// <param name="num">人数</param>
        /// <returns></returns>
        public IEnumerator MonsterRefreshBySimple(string objName, Vector3 pos, int num = 1) {
            while (--num > 0) {
                var obj = PoolManager.Ins.Release(objName);
                obj.transform.position = pos;
                yield return waitOneMonsterRefreshTime;
            }
        }

        public IEnumerator MonsterRefreshByPosRandom(Vector3 pos, float circle, string objName, Action<GameObject> cb = null, int num = 1) {
            while (num-- > 0) {
                Vector3 xy = new Vector3(Random.Range(-circle, circle),0, Random.Range(-circle, circle));
                PoolManager.Ins.Release(objName, pos + xy, cb);
                yield return waitOneMonsterRefreshTime;
            }
        }
        #endregion

        #region 敌人简单AI移动,四处走动

        /// <summary>
        /// 默认周围半径一米找个地点
        /// </summary>
        /// <param name="circle"></param>
        /// <returns></returns>
        public Vector3 AIMove(Vector3 aiPos, float circle = 1) {
            var nextPos = new Vector3(
            aiPos.x + Random.Range(-circle, circle),
            aiPos.y,
            aiPos.z + Random.Range(-circle, circle)
            );

            return nextPos;
        }

        #endregion

        #region 游戏对象显示与隐藏

        public void ObjSetActive(GameObject obj, bool a) {
            if (obj == null) return;
            if (obj.activeSelf && !a) {
                obj.SetActive(a);
                return;
            }

            if (!obj.activeSelf && a) {
                obj.SetActive(a);
            }
        }

        #endregion

        #region 世界坐标转屏幕
        /// <summary>
        /// 获取屏幕中心点
        /// </summary>
        /// <returns></returns>
        public Vector2 GetScreenCenter() {
            float centerX = Screen.width / 2;
            float centerY = Screen.height / 2;
            return new Vector2(centerX, centerY);
        }

        /// <summary>
        /// 获取当前鼠标位置对于屏幕中心的向量
        /// </summary>
        /// <returns></returns>
        public Vector2 GetDirByMainCameraMousePoint() {
            var mousePos = Input.mousePosition;
            Vector3 center = GetScreenCenter();
            return (mousePos - center).normalized;
        }

        /// <summary>
        /// 获取8个方向 顺时针
        /// ↖0 ↑1 ↗2
        /// ←7     →3
        /// ↙6 ↓5 ↘4
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Vector3 Get8Dir(int i) {
            i = i % 8;
            switch (i) {
                case 0: return new Vector3(-1, 0, 1).normalized;
                case 4: return new Vector3(1, 0, -1).normalized;
                case 1: return new Vector3(0, 0, 1).normalized;
                case 5: return new Vector3(0, 0, -1).normalized;
                case 2: return new Vector3(1, 0, 1).normalized;
                case 6: return new Vector3(-1, 0, -1).normalized;
                case 3: return new Vector3(1, 0, 0).normalized;
                case 7: return new Vector3(-1, 0, 0).normalized;
                default:
                    break;
            }
            return new Vector3(0, 0, 1).normalized;
        }

        public Vector2 GetUIMousePosition() {
            return UIManager.Ins.UICamera.ScreenToWorldPoint(Input.mousePosition);
        }
        /// <summary>
        /// 将透视3d物体坐标转为正交屏幕坐标
        /// 注意的是 需要赋值给localPosition自身坐标
        /// </summary>
        /// <param name="pos">世界坐标位置</param>
        /// <returns></returns>
        public Vector2 World2Screen(Vector3 pos) {
            //首先获取3D物体的屏幕空间坐标
            Vector3 targetPos = Camera.main.WorldToScreenPoint(pos);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                UIManager.Ins.TransCanvas,
                targetPos,
                UIManager.Ins.UICamera,
                out Vector2 canvasPoint);

            return canvasPoint;
        }
        /// <summary>
        /// 人物旋转时,主角上下左右移动不变,按照当前方向来转换动画设置的vh值
        /// </summary>
        /// <param name="v">动画设置的vertival</param>
        /// <param name="h">动画设置的horzontal</param>
        /// <param name="x">屏幕当前点对于中心点的归一x值</param>
        /// <param name="y">屏幕当前点对于中心点的归一y值</param>
        /// <returns></returns>
        public void TransformDir2Anim(ref float v, ref float h, float x, float y) {
            //取中心点的时候对角0.87 0.49分上下左右
            //上 x(-0.87,0.87) y(0.49,1)
            //下 x(-0.87,0.87) y(-1,-0.49)
            //左 x(-0.87,-1) y(-0.49,0.49)
            //右 x(-0.87,-1) y(0.49,1)
            //0.87 0.49
            //0,0,1 (0.5,0) (-0.5,0) (0,-0.5) (0,0.5)
            //0,0,-1 相反即可
            //1,0,0 (0,-0.5)->(0.5,0) (0,0.5)->(-0.5,0) (-0.5 0)->(0,-0.5) (0.5,0)->(0,0.5)
            //-1,0,0 相反 (0,0.5) (0,-0.5) (0.5,0) (-0.5,0)
            if (x >= -0.87 && x <= 0.87 && y >= 0.49) {
                return;
                //正方向
            } else if (x >= -0.87 && x <= 0.87 && y < -0.49) {
                //负方向 需要改变
                v = -v;
                h = -h;
            } else if (x < -0.87 && y > -0.49 && y < 0.49) {
                var temp = v;
                v = -h;
                h = temp;
            } else {
                var temp = v;
                v = h;
                h = -temp;
            }
        }

        #endregion

        #region 碰撞相关

        public void AddRigibody(GameObject obj, bool freezePos = false, bool freezeRotate = false) {
            var r = obj.AddComponent<Rigidbody>();
            if (freezePos && !freezeRotate) {
                r.constraints = RigidbodyConstraints.FreezePosition;
            } else if (!freezePos && freezeRotate) {
                r.constraints = RigidbodyConstraints.FreezeRotation;
            } else {
                r.constraints = RigidbodyConstraints.FreezeAll;
            }
            //冻结rotation
            //r.constraints = RigidbodyConstraints.FreezeRotation;
            //冻结rotation的x轴方向的旋转
            //r.constraints = RigidbodyConstraints.FreezeRotationX;
            //冻结rotation的y轴方向的旋转
            //r.constraints = RigidbodyConstraints.FreezeRotationY;
            //冻结rotation的z轴方向的旋转
            //r.constraints = RigidbodyConstraints.FreezeRotationZ;
            //冻结position
            //r.constraints = RigidbodyConstraints.FreezePosition;
            //冻结position的x轴方向的移动
            //r.constraints = RigidbodyConstraints.FreezePositionX;
            //冻结position的y轴方向的移动
            //r.constraints = RigidbodyConstraints.FreezePositionY;
            //冻结position的z轴方向的移动
            //r.constraints = RigidbodyConstraints.FreezePositionZ;
            //冻结position和rotation
            //r.constraints = RigidbodyConstraints.FreezeAll;
        }
        public void RemoveRigibody(GameObject obj) {
            if (obj == null) return;
            var r = obj.GetComponentInChildren<Rigidbody>();
            Destroy(r);
        }

        #endregion
    }
}