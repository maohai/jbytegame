Shader "Custom/Build/DeferredDecal"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags {"Queue"="Transparent+100"}
		Pass
		{
			Fog { Mode Off } // no fog in g-buffers pass
			ZWrite Off
			//Blend SrcAlpha OneMinusSrcAlpha
			Blend One One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
 
			struct appdata
			{
				float4 vertex : POSITION;
			};
 
			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 screenPos : TEXCOORD1;
			};
 
			sampler2D _MainTex;
			sampler2D _BumpMap;
			sampler2D_float _CameraDepthTexture;
			sampler2D _NormalCopyRT;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.screenPos = ComputeScreenPos(o.pos);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//齐次除法，计算ndc
				float4 divW = i.screenPos / i.screenPos.w;
				float4 ndcPos = divW * 2 - 1;
				//将屏幕像素对应在摄像机远平面的点转换到剪裁空间，也是相机(0,0,0)指向该点的向量
				float far = _ProjectionParams.z;
				float3 farClipVec = float3(ndcPos.xy, 1) * far;
				//通过逆投影矩阵将向量转换到观察空间
				float3 viewVec = mul(unity_CameraInvProjection, farClipVec.xyzz).xyz;
				//将向量乘以线性深度值，得到在深度缓冲中储存的值在观察空间的位置
				float2 screenUV = divW.xy;
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, screenUV);
				float3 viewPos = viewVec * Linear01Depth(depth);
				//观察空间变换到世界空间
				float4 worldPos = mul(UNITY_MATRIX_I_V, float4(viewPos, 1.0));

				float4 objectPos = mul(unity_WorldToObject, worldPos);
				clip(float3(0.5, 0.5, 0.5) - abs(objectPos));
				float2 uv = objectPos.xz + 0.5;
				fixed4 finalColor = tex2D(_MainTex, uv);
				return finalColor;
			}
			ENDCG
		}
	}
}