Shader "Custom/Build/BuildConstructionCondition"
{
    		Properties {
		_Color("Color",Color) = (1,1,1,1)
		_MainTex("MainTex",2D) = "white"{}
		_AlphaScale("Alpha",range(0,1)) = 0.8
	}

	SubShader {
		Tags{
			"Queue" = "Transparent"  //渲染队列
			"IgnoreProject" = "true" //使得该subShader不受projector影响,常用于半透明物体
			"RenderType" = "透明"     //对这色漆进行分类 
			}
		Pass {
			ZWrite On
			ColorMask 0
		}
		Pass {
			Tags { "LightMode"="ForwardBase" }//向前渲染,该pass会计算环境光/平行光/逐顶点/SH光源/Lightmaps

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha  //混合 源a 1-源a
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "Lighting.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _AlphaScale;
			
			struct a2v {
				float4 vertex : POSITION;//模型空间的顶点位置
				float3 normal : NORMAL;//顶点法线
				float4 texcoord : TEXCOORD0;//纹理坐标

			};
			
			struct v2f {
				float4 pos : SV_POSITION;//裁剪空间的顶点坐标
				float3 worldNormal : TEXCOORD0;//世界法线
				float3 worldPos : TEXCOORD1;//世界顶点
				float2 uv : TEXCOORD2;//uv纹理
			};
			
			v2f vert(a2v v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);//模型空间转为裁剪空间坐标
				o.worldNormal = UnityObjectToWorldNormal(v.normal);//模型法线转为世界法线
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;//世界坐标
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);//uv纹理
				return o;
			}
			
			fixed4 frag(v2f i) : SV_Target {
				fixed3 worldNormal = normalize(i.worldNormal);//取模
				fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));//根据给定的对象空间顶点位置计算朝向光源的世界空间方向（未标准化）
				fixed4 texColor = tex2D(_MainTex, i.uv);//采集纹理
				
				fixed3 albedo = texColor.rgb * _Color.rgb;//漫反射系数
				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;//环境光
				fixed3 diffuse = _LightColor0.rgb * albedo * max(0, dot(worldNormal, worldLightDir));//漫反射
				
				return fixed4(ambient + diffuse, texColor.a * _AlphaScale);
			}
			
			ENDCG
		}
	} 
FallBack "Transparent/VertexLit"

}
