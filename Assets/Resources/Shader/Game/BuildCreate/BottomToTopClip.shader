Shader "Custom/Build/BottomToTopClip"
{
   Properties
    {
        _MainTex ("Texture", 2D) = "white" // 纹理
        _ClipHeight ("Clip Height", Float) = 0.0 // 裁剪高度
    }
    SubShader
    {
        Tags { "RenderType"="AlphaText" }
        LOD 200

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
                float3 localPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float _ClipHeight;
            
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz; // 获取顶点的世界坐标
                o.localPos = v.vertex.xyz;//获取顶点的局部坐标
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // 如果顶点的世界坐标Y值小于裁剪高度，则裁剪该片段
                if ((i.localPos.y+0.004) * 100 < _ClipHeight)
                {
                    discard;
                }
                return tex2D(_MainTex, i.uv); // 返回纹理颜色
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
