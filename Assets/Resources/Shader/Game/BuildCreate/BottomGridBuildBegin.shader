Shader "Custom/Build/BottomGridBuildBegin"
{
   	Properties
	{
		_Color("Color",Color) = (1,1,1,1)
		_Width("Width",float) = 0.1
		_Min("Min",float) = 0.2
		_Max("Max",float) = 0.4
		_ScaleOffset("ScaleOffset",vector) = (20,20,20,20)
	}
	SubShader
	{
		Tags {
            "Queue" = "Transparent"
            "IgnoreProject" = "true" 
        }
		Pass {
			ZWrite On
			ColorMask 0
        }
		Pass
		{
			Tags { "LightMode"="ForwardBase" }
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			//敲代码的时候要注意：“CGPROGRAM”和“#pragma...”中的拼写不同,真不知道“pragma”是什么单词
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			float4 _Color;
			float _Width;
			float _Min;
			float _Max;
			float4 _ScaleOffset;
			//添加一个计算方法
			float mod(float a,float b)
			{
				//floor(x)方法是Cg语言内置的方法，返回小于x的最大的整数
				return a - b*floor(a / b);
			}
			struct appdata
			{
				float4 vertex:POSITION;
				float2 uv:TEXCOORD0;
			};
			struct v2f
			{
				float2 uv:TEXCOORD0;
				float4 vertex:SV_POSITION;
			};
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float emptyWidth = 1 - _Width;
				float2 emptySize = float2(emptyWidth, emptyWidth);
				//frac用于获取浮点数的小数部分的函数 中间乘以一个很大的无规律的数再加上偏移量用于调整随机数的范围
				float2 oneEmpth = abs(frac(i.uv * _ScaleOffset.xy + _ScaleOffset.zw) * 2.0 + -1.0) - emptySize;
				//fwidth领域像素的近似导数值
				float2 one = 1.0 - oneEmpth / fwidth(oneEmpth);
				float4 res = 1 - saturate(min(one.x, one.y)).xxxx;
				float len = length(i.uv - float2(0.5,0.5));
				res *= step(len, 0.5);
				res *= smoothstep( 1-len, _Min, _Max);

				return res * _Color;
			}
			ENDCG
		}
	}
}
