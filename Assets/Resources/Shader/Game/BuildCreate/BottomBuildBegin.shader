Shader "Custom/Build/BottomBuildBegin"
{
   Properties
    {
        _Color("Trans Color",Color) = (0,0,0,0)
    }
    SubShader
    {
        Tags {
            "Queue" = "Transparent"
            "IgnoreProject" = "true" 
        }
        
        Pass {
			ZWrite On
			ColorMask 0
        }
        Pass
        {
            Tags { "LightMode"="ForwardBase" }
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "Lighting.cginc"
            
            fixed4 _Color;
            
            struct a2v
            {
                float4 vertex : POSITION;
                float4 texcoord : TEXCOORD0;
            };
            struct v2f
            {
                float4 pos : SV_POSITION;
            };
            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(v2f i) : SV_TARGET{
                return fixed4(_Color.xyz,0.5f);
            }
           
            ENDCG
        }
    }
    FallBack "Diffuse"
}
