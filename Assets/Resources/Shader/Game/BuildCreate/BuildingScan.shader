Shader "Custom/Build/BuildingScan"
{
   Properties
    {
        _MainTex("Main Tex", 2D) = "white"{}
        _Color("Trans Color",Color) = (0,0,0,0)
        _ClipHeight("Clip",Range(0,1)) = 0
    }
    SubShader
    {
        Tags {
            "Queue" = "Transparent"
            "IgnoreProject" = "true" 
        }
        
        Pass {
			ZWrite On
			ColorMask 0
        }
        Pass
        {
            Tags { "LightMode"="ForwardBase" }
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "Lighting.cginc"
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            fixed _ClipHeight;
            fixed _Alpha;
            
            struct a2v
            {
                float4 vertex : POSITION;
                float4 texcoord : TEXCOORD0;
            };
            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 localPos : TEXCOORD1;
            };
            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.localPos = v.vertex.xyz;//局部坐标
                return o;
            }
            fixed4 frag(v2f i) : SV_TARGET{
                fixed4 col = tex2D(_MainTex, i.uv);
                if(i.localPos.y + 0.5f > _ClipHeight)
                {
                    return _Color;
                }else
                {
                    return col;
                }
            }
           
            ENDCG
        }
    }
    FallBack "Diffuse"
}
