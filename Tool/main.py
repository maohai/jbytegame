import os
try:
    from openpyxl import load_workbook
except:
    print("没有")
    os.system("pip install openpyxl")
    from openpyxl import load_workbook

import sys

path="Data.json"

dirPath=""
isCreateCs=True
# print(sys.argv)
if len(sys.argv)==4:
    dirPath=sys.argv[1]
    path=os.path.join(sys.argv[2],path)
    if sys.argv[3]!="1":
        isCreateCs=False


wb=load_workbook("./Data.xlsx")
allSheet = wb.sheetnames
data={}

csStype="""
using Newtonsoft.Json.Linq;

namespace Game
{
    SCRIPT
}
"""

cs="""
    public class NMAE
    {
ATTRIBUTES
        public NAMEFUN(JToken data)
        {
INIT
        }
    }
"""

LString='''
    public class Lstring
    {
    
LSTRING

    }
'''
csData={}


serverCs=""

def createData(sheetName):
    global serverCs
    t=wb[sheetName]
    if t.max_row<=2:
        return
    data[sheetName]=[]
    #begin 本地化
    ls=""
    for y in range(2,t.max_row+1):
        if y==3:#第三行是数据类型
            continue
        s=[]
        for x in range(1,t.max_column+1):
            s.append(t.cell(y,x).value)
        data[sheetName].append(s)

        if sheetName=="Localization" and y>3:#本地化处理
            l="\t\t/// <summary>\n\t\t/// {0}\n\t\t/// </summary>\n\t\tpublic const string {1}=\"{1}\";".format(str(t.cell(y,2).value),str(t.cell(y,1).value))
            if y<t.max_row:
                l+="\n"
            ls+=l


    if ls!="":
        ls = LString.replace("LSTRING",ls)
        p=os.path.join(dirPath,"Lstring.cs")
        with open(p,"w",encoding="utf-8") as file:
            file.write(ls)
        print("生成Lstring")
    #end 本地化
    #begin 脚本
    if isCreateCs:
        c=cs
        name=sheetName+"Data"
        attributes=""
        initStr=""
        for x in range(1,t.max_column+1):
            attrName=str(t.cell(2,x).value)
            attrType=str(t.cell(3,x).value)
            attrDsc=str(t.cell(1,x).value)

            attr1="\t\t/// <summary>\n\t\t/// {0}\n\t\t/// </summary>\n".format(attrDsc)
            attr2="\t\tpublic {0} {1}".format(attrType,attrName)+"{ get; }\n"

            init="\t\t\t{0} = data[\"{0}\"].ToObject<{1}>();".format(attrName,attrType)
            #数组处理
            if '[' in attrType:
                init="\t\t\t{0} = data[\"{0}\"].ToCustomArray<{1}>();".format(attrName,attrType.split("[")[0])

            if x<t.max_column:
                init+="\n"

            attributes+=attr1+attr2
            initStr+=init
        
        c = c.replace("NMAE",name).replace("NAMEFUN",name).replace("ATTRIBUTES",attributes).replace("INIT",initStr)
        # p=os.path.join(dirPath,(name+".cs"))
        serverCs+=c+"\n"
        # with open(p,"w",encoding="utf-8") as file:
        #     serverCs+=c+"\n"
        #     file.write(c)
    #end 脚本
            
    print(n+" 完成")



for n in allSheet:
    createData(n)

result = csStype.replace("SCRIPT",serverCs)

# print(result)

#输出脚本
current_dir = os.path.dirname(os.path.abspath(__file__))
output_folder = os.path.abspath(os.path.join(current_dir, os.pardir))

csPath = output_folder+"\\Assets\\Scripts\\Game\\Data\\"
p=os.path.join(csPath,("DataJson.cs"))
with open(p,"w",encoding="utf-8") as file:
    file.write(result)

print("输出路径"+p) 



#输出json文件
# with open(path,'w',encoding="utf-8") as file:
#     s = str(data).replace('\'','\"')
#     file.write(s)

print("输出路径"+path) 
#输出json文件
jsonPath = output_folder+"\\Assets\\Resources\\Data"
p=os.path.join(jsonPath,("Data.json"))
with open(p,'w',encoding="utf-8") as file:
    s = str(data).replace('\'','\"')
    file.write(s)

print("输出路径"+p) 

